// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    firebase: {
        projectId: 'giakki-125c7',
        appId: '1:248403296808:web:634cb5a2a461d84a282b1d',
        storageBucket: 'giakki-125c7.appspot.com',
        locationId: 'europe-west',
        apiKey: 'AIzaSyDknJYHSmkCg39sFBh7Fn6Q2rnxcNF3zoA',
        authDomain: 'giakki-125c7.firebaseapp.com',
        messagingSenderId: '248403296808',
        measurementId: 'G-CN8QEQ4M56',
    },
    useEmulators: false,
    production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
