import { Card } from '@shared/card-api/cards.model';

export function mkCard(card?: Partial<Card>): Card {
    return {
        id: crypto.randomUUID(),
        scryfall_uri: crypto.randomUUID(),
        image_uris: {
            art_crop: crypto.randomUUID(),
            normal: crypto.randomUUID(),
            ...card?.image_uris,
        },
        name: crypto.randomUUID(),
        type_line: '',
        colors: undefined,
        card_faces: undefined,
        ...card,
    };
}
