import { ApiList, Card, Collection } from '@shared/card-api/cards.model';

import { mkCard } from './card.mock';

export function mkApiCollection(list?: Partial<ApiList<Card>>): ApiList<Card> {
    return {
        data: [],
        not_found: [],
        ...list,
    };
}

export function mkApiCollectionAllFound(list?: Partial<ApiList<Card>>): ApiList<Card> {
    return {
        data: [mkCard(), mkCard(), mkCard()],
        not_found: [],
        ...list,
    };
}

export function mkCollection(collection?: Partial<Collection>): Collection {
    return {
        cards: [],
        notFound: [],
        ...collection,
    };
}
