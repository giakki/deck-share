import { Params } from '@angular/router';
import { Observable, of } from 'rxjs';

export class MockActivatedRoute {
    queryParams: Observable<Params> = of({
        deck: '123',
        sorting: 'CMC',
    });
}

export class MockRouter {
    navigate = (..._: unknown[]): void => {
        /**/
    };
}
