import { Injectable } from '@angular/core';
import { FirebaseApp, provideFirebaseApp } from '@angular/fire/app';
import { provideAuth } from '@angular/fire/auth';
import { provideFirestore } from '@angular/fire/firestore';
import type { FirebaseApp as IFirebaseApp } from 'firebase/app';
import type { Auth as IFirebaseAuth, Unsubscribe } from 'firebase/auth';
import type { Firestore as FirebaseFirestore } from 'firebase/firestore';

@Injectable({
    providedIn: 'root',
})
export class MockFirebaseApp implements IFirebaseApp {
    name = 'mock';
    options = {};
    automaticDataCollectionEnabled = false;
}

@Injectable({
    providedIn: 'root',
})
export class MockFirebaseAuth implements IFirebaseAuth {
    app = new MockFirebaseApp();
    name = 'mock auth';
    config = {
        apiKey: '',
        apiHost: '',
        apiScheme: '',
        tokenApiHost: '',
        sdkClientVersion: '',
    };
    languageCode = 'undefined';
    tenantId = '';
    settings = {
        appVerificationDisabledForTesting: true,
    };
    currentUser = null;
    emulatorConfig = null;

    _authWindow = {
        close: jasmine.createSpy(),
        closed: false,
    };
    _eventManager = {
        registerConsumer: jasmine.createSpy(),
        unregisterConsumer: jasmine.createSpy(),
    };
    _popupRedirectResolver = {
        _initialize: jasmine.createSpy().and.returnValue(this._eventManager),
        _isIframeWebStorageSupported: jasmine.createSpy(),
        _openPopup: jasmine.createSpy().and.returnValue(this._authWindow),
        _originValidation: jasmine.createSpy().and.resolveTo(),
    };
    /* istanbul ignore next */
    authStateReady(): Promise<void> {
        throw new Error('Method not implemented.');
    }
    /* istanbul ignore next */
    setPersistence(): Promise<void> {
        throw new Error('Method not implemented.');
    }
    /* istanbul ignore next */
    onAuthStateChanged(): Unsubscribe {
        throw new Error('Method not implemented.');
    }
    /* istanbul ignore next */
    beforeAuthStateChanged(): Unsubscribe {
        throw new Error('Method not implemented.');
    }
    /* istanbul ignore next */
    onIdTokenChanged(): Unsubscribe {
        throw new Error('Method not implemented.');
    }
    /* istanbul ignore next */
    updateCurrentUser(): Promise<void> {
        throw new Error('Method not implemented.');
    }
    /* istanbul ignore next */
    useDeviceLanguage(): void {
        throw new Error('Method not implemented.');
    }
    /* istanbul ignore next */
    signOut(): Promise<void> {
        throw new Error('Method not implemented.');
    }

    /* istanbul ignore next */
    loginWithSuccess() {
        this._authWindow.closed = true;
        this._eventManager.registerConsumer.calls.mostRecent().args[0].onAuthEvent({ type: 'signInViaPopup' });
    }
}

@Injectable({
    providedIn: 'root',
})
export class MockFirestore implements FirebaseFirestore {
    type = 'firestore' as const;

    _app = new MockFirebaseApp();

    /* istanbul ignore next */
    get app(): FirebaseApp {
        return this._app;
    }

    /* istanbul ignore next */
    toJSON(): object {
        throw new Error('Method not implemented.');
    }
}

export function provideMockFirebaseApp() {
    return provideFirebaseApp((injector) => injector.get(MockFirebaseApp));
}

export function provideMockFirebaseAuth() {
    return provideAuth((injector) => injector.get(MockFirebaseAuth));
}

export function provideMockFirestore() {
    return provideFirestore((injector) => injector.get(MockFirestore));
}
