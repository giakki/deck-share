import { Deck, DeckEntry } from '@analyzer';

export function mkDeckEntry(deckEntry?: Partial<DeckEntry>): DeckEntry {
    return {
        amount: Math.ceil(Math.random() * 10),
        id: crypto.randomUUID(),
        name: crypto.randomUUID(),
        ...deckEntry,
    };
}

export function mkDeck(deck?: Partial<Deck>): Deck {
    return {
        id: crypto.randomUUID(),
        cards: [mkDeckEntry(), mkDeckEntry(), mkDeckEntry()],
        name: crypto.randomUUID(),
        decklist: crypto.randomUUID(),
        ...deck,
    };
}
