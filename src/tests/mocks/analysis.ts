import { AnalysisResults, AnalysisResultsByCluster, AnalysisResultsByFrequency, RawAnalysisResults } from '@analyzer';
import { CardColor, CardType } from '@shared/card-api/cards.model';
import { PieChartData } from '@shared/charts';
import times from 'ramda/es/times';

import { mkCard } from './card.mock';

export function mkAnalysisResultsByColor(): PieChartData<CardColor, number> {
    return {
        data: [0, 0, 0, 0, 0],
        labels: ['white', 'blue', 'black', 'red', 'green'],
    };
}

export function mkAnalysisResultsByCluster(numResults: number): AnalysisResultsByCluster {
    return times(() => times(() => Math.random(), numResults), numResults);
}

export function mkAnalysisResultsByFrequency(numResults: number): AnalysisResultsByFrequency {
    return times(() => ({ card: mkCard(), amount: Math.random() }), numResults);
}

export function mkAnalysisResultsByType(): PieChartData<CardType, number> {
    return {
        data: [0, 0, 0, 0, 0, 0, 0],
        labels: ['land', 'creature', 'artifact', 'enchantment', 'planeswalker', 'instant', 'sorcery'],
    };
}

export function mkRawAnalysisResults(results?: Partial<RawAnalysisResults>): RawAnalysisResults {
    return {
        byCluster: [],
        byColor: {
            black: 0,
            blue: 0,
            green: 0,
            red: 0,
            white: 0,
        },
        byFrequency: [],
        byType: {
            artifact: 0,
            creature: 0,
            enchantment: 0,
            instant: 0,
            land: 0,
            planeswalker: 0,
            sorcery: 0,
        },
        notFound: [],
        ...results,
    };
}

export function mkAnalysisResults(results?: Partial<AnalysisResults>): AnalysisResults {
    return {
        byCluster: [],
        byColor: mkAnalysisResultsByColor(),
        byFrequency: [],
        byType: mkAnalysisResultsByType(),
        notFound: [],
        ...results,
    };
}
