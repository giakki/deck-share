declare let global: unknown;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const _global: any = typeof window === 'undefined' ? global : window;
/**
 * Utility function to create a matcher result.
 * @param pass Whether the matcher passed or not.
 * @param message The message to display.
 */
function createMatcherResult(pass: boolean, message: string): jasmine.CustomMatcherResult {
    return {
        pass,
        get message(): string {
            return message;
        },
    };
}

/**
 * Extend the API to support chaining with custom matchers.
 */
export const expect: <T>(actual: unknown) => NgMatchers<T> = _global.expect;

/**
 * Jasmine matchers declarations.
 */
export interface NgMatchers<T> extends jasmine.Matchers<T> {
    toBeSortedBy(by: (a: T, b: T) => number): boolean;
}

/**
 * Custom matchers to add to jasmine.
 */
export const customMatchers: jasmine.CustomMatcherFactories = {
    /**
     * Check that an array is sorted according to a given function.
     */
    toBeSortedBy<T>(): jasmine.CustomMatcher {
        return {
            /**
             * Jasmine interface to implement.
             */
            compare(actual: T[], by: (a: T, b: T) => number): jasmine.CustomMatcherResult {
                // The given argument must be an array.
                if (!Array.isArray(actual)) {
                    return createMatcherResult(false, `Expected ${JSON.stringify(actual)} to be instanceof Array`);
                }
                return createMatcherResult(
                    actual.every((_, index, array) => index === 0 || by(array[index], array[index - 1]) >= 0),
                    `Expected ${JSON.stringify(actual)} to be sorted.`,
                );
            },
        };
    },
};
