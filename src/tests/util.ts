import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

export function setInputValue(fixture: ComponentFixture<unknown>, input: HTMLInputElement, value: string): void {
    input.value = value;
    input.dispatchEvent(new InputEvent('input'));
    fixture.detectChanges();
}

export function submit(fixture: ComponentFixture<unknown>) {
    fixture.debugElement.query(By.css('button[type="submit"]')).nativeElement?.click();
    fixture.detectChanges();
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function testDiForCoverage<F extends (...args: any[]) => any>(fn: F, ...args: Parameters<F>) {
    for (let i = 0; i < Math.pow(2, args.length); ++i) {
        const thisArgs = args.map((arg, j) => {
            if (i & (1 << j)) {
                return arg;
            }
            return;
        });

        const result = TestBed.runInInjectionContext(() => fn(...thisArgs));
        expect(result).toBeTruthy();
    }
}
