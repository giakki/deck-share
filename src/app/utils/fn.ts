export function isNotNullOrUndefined<T>(value: T | null | undefined): value is T {
    return value !== null && isNotUndefined(value);
}

export function isNotUndefined<T>(value: T | undefined): value is T {
    return value !== undefined;
}
