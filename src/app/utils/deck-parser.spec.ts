import testDeck from '../../tests/mocks/deck.mock.json';
import { parseDeck, parseDeckCard } from './deck-parser';

describe('parseDeck', () => {
    it('should match the expected result', () => {
        expect(parseDeck(testDeck)).toEqual([
            { name: 'Etherium Sculptor', amount: 1 },
            { name: 'Plains', amount: 3 },
            { name: 'Rebbec, Architect of Ascension', amount: 1 },
        ]);
    });
});

describe('parseDeckCard', () => {
    it('should ignore empty lines', () => {
        expect(parseDeckCard('')).toBeUndefined();
    });

    it('should ignore comment lines', () => {
        expect(parseDeckCard('//Deck')).toBeUndefined();
    });

    it('should ignore lines without amount', () => {
        expect(parseDeckCard('Sideboard')).toBeUndefined();
    });

    it('should parse a simple card', () => {
        expect(parseDeckCard('1 Swamp')).toEqual({ amount: 1, name: 'Swamp' });
    });

    it('should parse a card amount', () => {
        expect(parseDeckCard('4 Goblin guide')).toEqual({
            amount: 4,
            name: 'Goblin guide',
        });
    });

    it("should parse a card with set and collector's number", () => {
        expect(parseDeckCard("1 Urza's Power Plant (chr) 115c")).toEqual({
            amount: 1,
            name: "Urza's Power Plant",
        });
    });
});
