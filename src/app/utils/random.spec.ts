import { times } from 'ramda';

import { mkRandom } from './random';

describe('IRandom', () => {
    it('should produce the expected int values', () => {
        const random = mkRandom([0, 1, 2, 3]);
        const expected = [
            384, 524546, 1075056769, 2828078338, 1617561924, 3403628308, 1563858755, 3600111265, 3629013629, 1801479719,
        ];

        const actual = times(() => random.int(2 ** 32), 10);

        expect(actual).toEqual(expected);
    });

    it('should produce the expected float values', () => {
        const random = mkRandom([0, 1, 2, 3]);
        const expected = [
            0.0, 9.5367431640625e-7, 1.955509185791015625e-3, 5.12897968292236328125e-3, 5.029327869415283203125e-1,
            1.350778341293334960938e-1, 5.096452832221984863281e-1, 1.64499402046203613281e-1,
            4.562809467315673828125e-1, 2.431724071502685546875e-1,
        ];

        const actual = times(() => random.double(), 10);

        expect(actual).toEqual(expected);
    });
});
