export type RandomSeed = [number, number, number, number];

export interface IRandom {
    int(bound: number): number;
    double(): number;
    serialize(): RandomSeed;
}

export function mkRandom(seed: RandomSeed) {
    return new Xoroshiro(seed);
}

// https://prng.di.unimi.it/
class Xoroshiro implements IRandom {
    constructor(private state: RandomSeed) {}

    // xoroshiro128++
    int(bound: number): number {
        const result = (rotateLeft(this.state[0] + this.state[3], 7) + this.state[0]) >>> 0;

        const t = (this.state[1] << 9) >>> 0;

        this.state[2] = (this.state[2] ^ this.state[0]) >>> 0;
        this.state[3] = (this.state[3] ^ this.state[1]) >>> 0;
        this.state[1] = (this.state[1] ^ this.state[2]) >>> 0;
        this.state[0] = (this.state[0] ^ this.state[3]) >>> 0;

        this.state[2] = (this.state[2] ^ t) >>> 0;

        this.state[3] = rotateLeft(this.state[3], 11);

        return result % bound;
    }

    // xoroshiro128+
    double(): number {
        const result = normalize((this.state[0] + this.state[3]) >>> 0);

        const t = (this.state[1] << 9) >>> 0;

        this.state[2] = (this.state[2] ^ this.state[0]) >>> 0;
        this.state[3] = (this.state[3] ^ this.state[1]) >>> 0;
        this.state[1] = (this.state[1] ^ this.state[2]) >>> 0;
        this.state[0] = (this.state[0] ^ this.state[3]) >>> 0;

        this.state[2] = (this.state[2] ^ t) >>> 0;

        this.state[3] = rotateLeft(this.state[3], 11);

        return result;
    }

    serialize() {
        return this.state;
    }
}

function rotateLeft(x: number, k: number) {
    return (((x << k) >>> 0) | (x >>> (32 - k))) >>> 0;
}

function normalize(n: number) {
    return (n >>> 8) * 2 ** -24;
}
