import { head } from 'ramda';

import { isNotUndefined } from './fn';

const CARD_REGEX = /^(\d+)([^([]+)(?:\((.+?)\))?(.+)?$/;
export interface ParsedDecklistItem {
    amount: number;
    name: string;
}

export function parseDeck(deck: string) {
    const chunks: string[][] = [];

    let iter: string[] | undefined;
    for (const line of deck.split('\n')) {
        if (/^\s*$/.test(line)) {
            iter = [];
            chunks.push(iter);
        } else if (iter) {
            iter.push(line);
        } else {
            iter = [line];
            chunks.push(iter);
        }
    }
    return chunks
        .filter((chunk) => {
            const header = head(chunk);
            return header && !header.toLocaleLowerCase().startsWith('sideboard');
        })
        .flat()
        .map(parseDeckCard)
        .filter(isNotUndefined);
}

export function parseDeckCard(card: string): ParsedDecklistItem | undefined {
    const match = CARD_REGEX.exec(card.trim());
    if (!match) {
        return;
    }

    const [_, amount, name] = match;
    /* istanbul ignore next: guaranteed by the regex */
    if (!amount || !name) {
        return;
    }

    return {
        amount: Number.parseInt(amount, 10),
        name: name.trim(),
    };
}
