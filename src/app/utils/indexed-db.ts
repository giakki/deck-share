import { Observable } from 'rxjs';

export interface IDBSchema {
    stores: {
        name: string;
        params: IDBObjectStoreParameters;
    }[];
}

export function open$(name: string, schema: IDBSchema) {
    let db: IDBDatabase | undefined;
    return new Observable<IDBDatabase>((sub) => {
        if (db) {
            sub.next(db);
            sub.complete();
            return;
        }

        const request = indexedDB.open(name, 1);
        request.addEventListener('upgradeneeded', () => {
            for (const store of schema.stores) {
                request.result.createObjectStore(store.name, store.params);
            }
        });

        request.addEventListener('success', () => {
            db = request.result;
            db.addEventListener('versionchange', () => {
                db?.close();
            });
            sub.next(request.result);
            sub.complete();
        });
    });
}

export function deleteDatabase$(name: string) {
    return new Observable<void>((sub) => {
        const request = indexedDB.deleteDatabase(name);
        /* istanbul ignore next */
        request.addEventListener('error', () => sub.error(request.error ?? new Error('Unknown Error.')));
        request.addEventListener('success', () => {
            sub.next();
            sub.complete();
        });
    });
}

export function transaction$(db: IDBDatabase, storeNames: string | string[], mode?: IDBTransactionMode) {
    return new Observable<IDBTransaction>((sub) => {
        const tx = db.transaction(storeNames, mode);
        tx.addEventListener('complete', () => sub.complete());
        /* istanbul ignore next */
        tx.addEventListener('error', () => sub.error(tx.error ?? new Error('Unknown Error.')));
        sub.next(tx);
    });
}

export function get$<T>(store: IDBObjectStore, key: IDBValidKey) {
    return new Observable<T>((sub) => {
        const request = store.get(key);
        /* istanbul ignore next */
        request.addEventListener('error', () => sub.error(request.error ?? new Error('Unknown Error.')));
        request.addEventListener('success', () => {
            sub.next(request.result);
            sub.complete();
        });
    });
}

export function getMany$<T>(tx: IDBTransaction, store: IDBObjectStore, keys: IDBValidKey[]) {
    return new Observable<T | undefined>((sub) => {
        tx.addEventListener('complete', () => sub.complete());
        for (const key of keys) {
            const request = store.get(key);
            /* istanbul ignore next */
            request.addEventListener('error', () => sub.error(request.error ?? new Error('Unknown Error.')));
            request.addEventListener('success', () => {
                sub.next(request.result);
            });
        }
    });
}

export function getAll$<T>(store: IDBObjectStore) {
    return new Observable<T[]>((sub) => {
        const request = store.getAll();
        /* istanbul ignore next */
        request.addEventListener('error', () => sub.error(request.error ?? new Error('Unknown Error.')));
        request.addEventListener('success', () => {
            sub.next(request.result);
            sub.complete();
        });
    });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function put$(store: IDBObjectStore, data: any, key?: IDBValidKey) {
    return new Observable<IDBValidKey>((sub) => {
        const request = store.put(data, key);
        /* istanbul ignore next */
        request.addEventListener('error', () => sub.error(request.error ?? new Error('Unknown Error.')));
        request.addEventListener('success', () => {
            sub.next(request.result);
            sub.complete();
        });
    });
}
