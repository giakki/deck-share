/* eslint-disable @typescript-eslint/ban-ts-comment */
import { isNotNullOrUndefined, isNotUndefined } from './fn';

describe('isNotNullUndefined', () => {
    it('should act as a type guard', () => {
        const v1: number | null | undefined = undefined;
        // @ts-ignore
        let _: number;
        // @ts-ignore
        let __: null | undefined;

        if (isNotNullOrUndefined<number>(v1)) {
            _ = v1;
        } else {
            __ = v1;
        }

        expect(true).toBeTrue();
    });

    it('should return false for non-null-or-undefined values', () => {
        const values = [0, false, ''];
        for (const value of values) {
            expect(isNotNullOrUndefined(value)).toBeTrue();
        }
    });

    it('should return true for null values', () => {
        expect(isNotNullOrUndefined(null)).toBeFalse();
    });

    it('should return true for undefined values', () => {
        expect(isNotNullOrUndefined(undefined)).toBeFalse();
    });
});

describe('isNotUndefined', () => {
    it('should act as a type guard', () => {
        const v1: number | undefined = undefined;
        // @ts-ignore
        let _: number;
        // @ts-ignore
        let __: undefined;

        if (isNotUndefined<number>(v1)) {
            _ = v1;
        } else {
            __ = v1;
        }

        expect(true).toBeTrue();
    });

    it('should return false for non-undefined values', () => {
        const values = [0, false, '', null];
        for (const value of values) {
            expect(isNotUndefined(value)).toBeTrue();
        }
    });

    it('should return true for undefined values', () => {
        expect(isNotUndefined(undefined)).toBeFalse();
    });
});
