import { head } from 'ramda';
import { firstValueFrom, forkJoin, isEmpty, map, switchMap, toArray } from 'rxjs';

import { deleteDatabase$, get$, getAll$, getMany$, open$, put$, transaction$ } from './indexed-db';

describe('IndexedDB', () => {
    const TEST_DB = '__test__';
    let db: IDBDatabase | undefined;

    beforeEach(
        () =>
            new Promise((resolve, reject) => {
                db?.close();
                const request = indexedDB.deleteDatabase(TEST_DB);
                request.addEventListener('error', () => reject(request.error));
                request.addEventListener('success', resolve);
            }),
    );

    describe('open$', () => {
        it('should produce only one value and then complete', async () => {
            db = head(await firstValueFrom(open$(TEST_DB, { stores: [] }).pipe(toArray())));

            expect(db).toEqual(jasmine.any(Object));
        });

        it('should produce an existing db, id if was already present', async () => {
            const db$ = open$(TEST_DB, { stores: [] });

            db = head(await firstValueFrom(db$.pipe(toArray())));
            const db2 = await firstValueFrom(db$.pipe(toArray()));

            expect(db).toBe(db2[0]);
        });

        it('should create the requested object stores', async () => {
            const storeName = '__store__';
            db = head(
                await firstValueFrom(open$(TEST_DB, { stores: [{ name: storeName, params: {} }] }).pipe(toArray())),
            );

            expect(db?.objectStoreNames.contains(storeName)).toBeTrue();
        });
    });

    describe('deleteDatabase$', () => {
        it('should produce only one value and then complete', async () => {
            const value = await firstValueFrom(deleteDatabase$(TEST_DB).pipe(toArray()));

            expect(value).toEqual([undefined]);
        });

        it('should delete the database', async () => {
            const storeName = '__store__';
            db = await firstValueFrom(open$(TEST_DB, { stores: [{ name: storeName, params: {} }] }));
            const txInsert = await firstValueFrom(transaction$(db, storeName, 'readwrite'));
            const key = await firstValueFrom(put$(txInsert.objectStore(storeName), '__value__', 1));

            await firstValueFrom(deleteDatabase$(TEST_DB));

            db = await firstValueFrom(open$(TEST_DB, { stores: [{ name: storeName, params: {} }] }));
            const txGet = await firstValueFrom(transaction$(db, storeName));
            const value = await firstValueFrom(get$(txGet.objectStore(storeName), key));
            expect(value).toBeUndefined();
        });

        it('should create the requested object stores', async () => {
            const storeName = '__store__';
            db = head(
                await firstValueFrom(open$(TEST_DB, { stores: [{ name: storeName, params: {} }] }).pipe(toArray())),
            );

            expect(db?.objectStoreNames.contains(storeName)).toBeTrue();
        });
    });

    describe('transaction$', () => {
        const STORE_NAME = '__store__';

        beforeEach(async () => {
            db = await firstValueFrom(open$(TEST_DB, { stores: [{ name: STORE_NAME, params: {} }] }));
        });

        it('should produce only one value and then complete', async () => {
            const tx = await firstValueFrom(transaction$(db!, STORE_NAME, 'readwrite').pipe(toArray()));

            expect(tx).toEqual([jasmine.any(Object)]);
        });

        it('should handle errors', async () => {
            try {
                await firstValueFrom(transaction$(db!, '__error__', 'readwrite').pipe(toArray()));
                fail();
            } catch {
                expect(true).toBe(true);
            }
        });
    });

    describe('get$', () => {
        const STORE_NAME = '__store__';
        const TEST_VALUE = '__value__';

        beforeEach(async () => {
            db = await firstValueFrom(open$(TEST_DB, { stores: [{ name: STORE_NAME, params: {} }] }));

            await firstValueFrom(
                transaction$(db, STORE_NAME, 'readwrite').pipe(
                    switchMap((tx) => put$(tx.objectStore(STORE_NAME), TEST_VALUE, 1)),
                    isEmpty(),
                ),
            );
        });

        it('should produce only one value and then complete', async () => {
            const item = await firstValueFrom(
                transaction$(db!, STORE_NAME).pipe(
                    map((tx) => tx.objectStore(STORE_NAME)),
                    switchMap((store) => get$(store, 1)),
                    toArray(),
                ),
            );

            expect(item).toEqual([TEST_VALUE]);
        });

        it('should handle errors', async () => {
            try {
                await firstValueFrom(
                    transaction$(db!, STORE_NAME).pipe(
                        map((tx) => tx.objectStore(STORE_NAME)),
                        switchMap((store) => get$(store, undefined as any)),
                    ),
                );
                fail();
            } catch {
                expect(true).toBe(true);
            }
        });
    });

    describe('getMany$', () => {
        const STORE_NAME = '__store__';
        const TEST_VALUE_1 = '__value1__';
        const TEST_VALUE_2 = '__value2__';

        beforeEach(async () => {
            db = await firstValueFrom(open$(TEST_DB, { stores: [{ name: STORE_NAME, params: {} }] }));

            await firstValueFrom(
                transaction$(db, STORE_NAME, 'readwrite').pipe(
                    switchMap((tx) => {
                        const store = tx.objectStore(STORE_NAME);
                        return forkJoin([
                            put$(store, TEST_VALUE_1, 1).pipe(isEmpty()),
                            put$(store, TEST_VALUE_2, 2).pipe(isEmpty()),
                        ]);
                    }),
                ),
            );
        });

        it('should produce only one value and then complete', async () => {
            const item = await firstValueFrom(
                transaction$(db!, STORE_NAME).pipe(
                    switchMap((tx) => {
                        const store = tx.objectStore(STORE_NAME);
                        return getMany$(tx, store, [1, 2]);
                    }),
                    toArray(),
                ),
            );

            expect(item).toEqual([TEST_VALUE_1, TEST_VALUE_2]);
        });

        it('should handle errors', async () => {
            try {
                await firstValueFrom(
                    transaction$(db!, STORE_NAME).pipe(
                        switchMap((tx) => {
                            const store = tx.objectStore(STORE_NAME);
                            return getMany$(tx, store, undefined as any);
                        }),
                    ),
                );
                fail();
            } catch {
                expect(true).toBe(true);
            }
        });
    });

    describe('getAll$', () => {
        const STORE_NAME = '__store__';
        const TEST_VALUE_1 = '__value1__';
        const TEST_VALUE_2 = '__value2__';

        beforeEach(async () => {
            db = await firstValueFrom(open$(TEST_DB, { stores: [{ name: STORE_NAME, params: {} }] }));

            await firstValueFrom(
                transaction$(db, STORE_NAME, 'readwrite').pipe(
                    switchMap((tx) => {
                        const store = tx.objectStore(STORE_NAME);
                        return forkJoin([
                            put$(store, TEST_VALUE_1, 1).pipe(isEmpty()),
                            put$(store, TEST_VALUE_2, 2).pipe(isEmpty()),
                        ]);
                    }),
                ),
            );
        });

        it('should produce only one value and then complete', async () => {
            const item = await firstValueFrom(
                transaction$(db!, STORE_NAME).pipe(
                    switchMap((tx) => {
                        const store = tx.objectStore(STORE_NAME);
                        return getAll$(store);
                    }),
                    toArray(),
                ),
            );

            expect(item).toEqual([[TEST_VALUE_1, TEST_VALUE_2]]);
        });
    });

    describe('put$', () => {
        const STORE_NAME = '__store__';
        const TEST_VALUE_1 = '__value1__';

        beforeEach(async () => {
            db = await firstValueFrom(open$(TEST_DB, { stores: [{ name: STORE_NAME, params: {} }] }));
        });

        it('should produce only one value and then complete', async () => {
            const item = await firstValueFrom(
                transaction$(db!, STORE_NAME, 'readwrite').pipe(
                    switchMap((tx) => {
                        const store = tx.objectStore(STORE_NAME);
                        return put$(store, TEST_VALUE_1, 1);
                    }),
                    toArray(),
                ),
            );

            expect(item as any).toEqual([1]);
        });

        it('should handle errors', async () => {
            try {
                await firstValueFrom(
                    transaction$(db!, STORE_NAME).pipe(
                        switchMap((tx) => {
                            const store = tx.objectStore(STORE_NAME);
                            return put$(store, undefined as any);
                        }),
                    ),
                );
                fail();
            } catch {
                expect(true).toBe(true);
            }
        });
    });
});
