import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { mkCard } from '@mocks/card.mock';
import { mkCollection } from '@mocks/collection.mock';
import { mkDeck, mkDeckEntry } from '@mocks/deck.mock';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { CardsService } from '../shared/card-api/cards.service';
import { AnalysisSettings, deckEntryFromCard } from './analyzer.model';
import { AnalyzerService } from './analyzer.service';

const ANALYSIS_SETTINGS: AnalysisSettings = {
    byCluster: {
        numClusters: 0,
        numIterations: 0,
        randomSeed: [0, 0, 0, 0],
    },
};

describe('AnalyzerService', () => {
    let service: AnalyzerService;
    let testScheduler: TestScheduler;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [provideMockStore(), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()],
        });
        service = TestBed.inject(AnalyzerService);

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    describe('constructDeckFromList', () => {
        it('should load cards from the collection', () => {
            const card = mkCard();
            const spy = spyOn(TestBed.inject(CardsService), 'collection').and.returnValue(
                of({ cards: [card], notFound: [] }),
            );

            testScheduler.run(() => {
                service.getCardsFromDecklist(`1 ${card.name}`).subscribe();

                expect(spy).toHaveBeenCalledOnceWith([{ name: card.name }]);
            });
        });

        it('should throw if some cards could not be found', () => {
            const card = mkCard();
            spyOn(TestBed.inject(CardsService), 'collection').and.returnValue(
                of({ cards: [], notFound: [{ name: card.name }] }),
            );

            testScheduler.run(({ expectObservable }) => {
                const obs = service.getCardsFromDecklist(`1 ${card.name}`);

                expectObservable(obs).toBe('#', {}, jasmine.any(Error));
            });
        });

        it('should throw in exceptional cases', () => {
            const card = mkCard();
            spyOn(TestBed.inject(CardsService), 'collection').and.returnValue(of({ cards: [], notFound: [] }));

            testScheduler.run(({ expectObservable }) => {
                const obs = service.getCardsFromDecklist(`1 ${card.name}`);

                expectObservable(obs).toBe('#', {}, jasmine.any(Error));
            });
        });

        it('should produce the expected result', () => {
            const cards = [mkCard(), mkCard(), mkCard()];
            const deckText = `1 ${cards[0].name}\n2 ${cards[1].name}`;

            testScheduler.run(({ expectObservable, hot }) => {
                const collection$ = hot('-a|', { a: { cards, notFound: [] } });
                spyOn(TestBed.inject(CardsService), 'collection').and.returnValue(collection$);

                const obs = service.getCardsFromDecklist(deckText);

                expectObservable(obs).toBe('-a|', {
                    a: [
                        { id: cards[0].id, name: cards[0].name, amount: 1 },
                        { id: cards[1].id, name: cards[1].name, amount: 2 },
                    ],
                });
            });
        });

        it('should handle double-face cards', () => {
            const cards = [
                mkCard({ card_faces: [{ colors: [], name: crypto.randomUUID(), type_line: crypto.randomUUID() }] }),
            ];
            const deckText = `1 ${cards[0].card_faces?.[0].name}`;

            testScheduler.run(({ expectObservable, hot }) => {
                const collection$ = hot('-a|', { a: { cards, notFound: [] } });
                spyOn(TestBed.inject(CardsService), 'collection').and.returnValue(collection$);

                const obs = service.getCardsFromDecklist(deckText);

                expectObservable(obs).toBe('-a|', {
                    a: [{ id: cards[0].id, name: cards[0].name, amount: 1 }],
                });
            });
        });
    });

    describe('run', () => {
        it('should be created', () => {
            expect(service).toBeTruthy();
        });

        it('should ask for the collection to the api', () => {
            const decks = [mkDeck(), mkDeck()];
            const spy = spyOn(TestBed.inject(CardsService), 'collection').and.callFake((ids) =>
                of({
                    cards: ids.map(({ name }) => mkCard({ name })),
                    notFound: [],
                }),
            );
            testScheduler.run(() => {
                service.run(decks, ANALYSIS_SETTINGS).subscribe();

                expect(spy).toHaveBeenCalledOnceWith(
                    decks.flatMap((deck) => deck.cards.map((card) => ({ name: card.name }))),
                );
            });
        });

        it('should report cards by color', () => {
            const cards = [
                mkCard({ colors: ['W'] }),
                mkCard({ colors: ['U'] }),
                mkCard({ colors: ['B'] }),
                mkCard({ colors: ['R'] }),
                mkCard({ colors: ['G'] }),
            ];
            const decks = [
                mkDeck({ cards: [deckEntryFromCard(cards[0], 1), deckEntryFromCard(cards[1], 2)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[0], 1), deckEntryFromCard(cards[2], 5)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[1], 2), deckEntryFromCard(cards[3], 12)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[2], 3), deckEntryFromCard(cards[4], 16)] }),
            ];

            testScheduler.run(({ cold, expectObservable }) => {
                spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                    cold('-a', {
                        a: mkCollection({ cards }),
                    }),
                );
                expectObservable(service.run(decks, ANALYSIS_SETTINGS)).toBe('-a', {
                    a: {
                        results: {
                            byCluster: [],
                            byColor: {
                                white: 0.5,
                                blue: 1,
                                black: 2,
                                red: 3,
                                green: 4,
                            },
                            byFrequency: jasmine.any(Object),
                            byType: jasmine.any(Object),
                            notFound: [],
                        },
                        newSettings: ANALYSIS_SETTINGS,
                    },
                });
            });
        });

        it('should report card frequencies', () => {
            const decks = [
                mkDeck({ cards: [mkDeckEntry({ name: '_1_', amount: 3 }), mkDeckEntry({ amount: 4 })] }),
                mkDeck({ cards: [mkDeckEntry({ name: '_1_', amount: 2 })] }),
            ];
            const cards = decks.flatMap((deck) => deck.cards.map((card) => mkCard({ name: card.name })));

            testScheduler.run(({ cold, expectObservable }) => {
                spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                    cold('-a', {
                        a: {
                            cards,
                            notFound: [],
                        },
                    }),
                );
                expectObservable(service.run(decks, ANALYSIS_SETTINGS)).toBe('-a', {
                    a: {
                        results: {
                            byCluster: [],
                            byColor: jasmine.any(Object),
                            byFrequency: [
                                { card: cards[0], amount: 5 },
                                { card: cards[1], amount: 4 },
                            ],
                            byType: jasmine.any(Object),
                            notFound: jasmine.any(Array),
                        },
                        newSettings: ANALYSIS_SETTINGS,
                    },
                });
            });
        });

        it('should report cards by type', () => {
            const cards = [
                mkCard({ type_line: 'Basic Land — Island' }),
                mkCard({ type_line: 'Enchantment Land' }),
                mkCard({ type_line: 'Artifact Creature — Thopter' }),
                mkCard({ type_line: 'Tribal Sorcery — Eldrazi' }),
                mkCard({ type_line: 'Instant' }),
                mkCard({ type_line: 'Legendary Planeswalker — Bolas' }),
                mkCard({ type_line: 'Creature — Goblin' }),
            ];
            const decks = [
                mkDeck({ cards: [deckEntryFromCard(cards[0], 48), deckEntryFromCard(cards[4], 1)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[0], 16), deckEntryFromCard(cards[4], 3)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[1], 24), deckEntryFromCard(cards[5], 3)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[1], 8), deckEntryFromCard(cards[5], 5)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[2], 32), deckEntryFromCard(cards[6], 12)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[2], 32), deckEntryFromCard(cards[6], 20)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[3], 1), deckEntryFromCard(cards[0], 8)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[3], 1), deckEntryFromCard(cards[0], 24)] }),
            ];

            testScheduler.run(({ cold, expectObservable }) => {
                spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                    cold('-a', {
                        a: mkCollection({ cards }),
                    }),
                );
                expectObservable(service.run(decks, ANALYSIS_SETTINGS)).toBe('-a', {
                    a: {
                        results: {
                            byCluster: [],
                            byColor: jasmine.any(Object),
                            byFrequency: jasmine.any(Object),
                            byType: {
                                land: 16,
                                creature: 12,
                                artifact: 8,
                                enchantment: 4,
                                planeswalker: 1,
                                instant: 0.5,
                                sorcery: 0.25,
                            },
                            notFound: [],
                        },
                        newSettings: ANALYSIS_SETTINGS,
                    },
                });
            });
        });

        it('should report missing cards', () => {
            const decks = [
                mkDeck({ cards: [mkDeckEntry({ name: '_1_', amount: 3 }), mkDeckEntry({ name: '_2_', amount: 4 })] }),
                mkDeck({ cards: [mkDeckEntry({ name: '_1_', amount: 2 })] }),
            ];
            const foundCard = mkCard({ name: decks[0].cards[0].name });

            testScheduler.run(({ cold, expectObservable }) => {
                spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                    cold('-a', {
                        a: {
                            cards: [foundCard],
                            notFound: [{ name: '_2_' }],
                        },
                    }),
                );
                expectObservable(service.run(decks, ANALYSIS_SETTINGS)).toBe('-a', {
                    a: {
                        results: {
                            byCluster: [],
                            byColor: jasmine.any(Object),
                            byFrequency: [{ card: foundCard, amount: 5 }],
                            byType: jasmine.any(Object),
                            notFound: ['_2_'],
                        },
                        newSettings: ANALYSIS_SETTINGS,
                    },
                });
            });
        });

        it('should run a full analysis', () => {
            const cards = [
                mkCard({ colors: [], type_line: 'Basic Land — Island' }),
                mkCard({ colors: [], type_line: 'Enchantment Land' }),
                mkCard({ colors: [], type_line: 'Artifact Creature — Thopter' }),
                mkCard({ colors: ['G'], type_line: 'Tribal Sorcery — Elf' }),
                mkCard({ colors: ['W', 'B'], type_line: 'Instant' }),
                mkCard({ colors: ['U', 'B', 'R'], type_line: 'Legendary Planeswalker — Bolas' }),
                mkCard({
                    card_faces: [
                        { colors: ['R'], type_line: 'Creature — Werefolf Horror' },
                        { colors: ['G'], type_line: 'Enchantment' },
                    ],
                    type_line: 'Creature — Werefolf Horror',
                }),
            ];
            const decks = [
                mkDeck({ cards: [deckEntryFromCard(cards[0], 48), deckEntryFromCard(cards[4], 1)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[0], 16), deckEntryFromCard(cards[4], 3)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[1], 24), deckEntryFromCard(cards[5], 3)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[1], 8), deckEntryFromCard(cards[5], 5)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[2], 32), deckEntryFromCard(cards[6], 12)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[2], 32), deckEntryFromCard(cards[6], 20)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[3], 1), deckEntryFromCard(cards[0], 8)] }),
                mkDeck({ cards: [deckEntryFromCard(cards[3], 1), deckEntryFromCard(cards[0], 24)] }),
            ];

            testScheduler.run(({ cold, expectObservable }) => {
                spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                    cold('-a', {
                        a: mkCollection({ cards }),
                    }),
                );
                expectObservable(service.run(decks, ANALYSIS_SETTINGS)).toBe('-a', {
                    a: {
                        results: {
                            byCluster: [],
                            byColor: {
                                white: 0.5,
                                blue: 1,
                                black: 1.5,
                                red: 5,
                                green: 4.25,
                            },
                            byFrequency: [
                                { card: cards[2], amount: 64 },
                                { card: cards[1], amount: 32 },
                                { card: cards[6], amount: 32 },
                                { card: cards[5], amount: 8 },
                                { card: cards[4], amount: 4 },
                                { card: cards[3], amount: 2 },
                            ],
                            byType: {
                                land: 16,
                                creature: 12,
                                artifact: 8,
                                enchantment: 8,
                                planeswalker: 1,
                                instant: 0.5,
                                sorcery: 0.25,
                            },
                            notFound: [],
                        },
                        newSettings: ANALYSIS_SETTINGS,
                    },
                });
            });
        });
    });
});
