import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { Component, ElementRef, Inject, inject, OnInit, TemplateRef, viewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { PartialBy } from '@utils/types';

import { Deck } from '../analyzer.model';
import { DeckModalValidator, ERROR_EMPTY_DECK, ERROR_INVALID_DECK } from './deck-modal.validator';

export interface AddDeckModalInput {
    deck: Deck | undefined;
}

export type AddDeckModalOuput = PartialBy<Deck, 'name'>;

@Component({
    templateUrl: './deck-modal.component.html',
    styleUrl: './deck-modal.component.scss',
    providers: [DeckModalValidator],
    standalone: true,
    imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatProgressSpinnerModule,
    ],
})
export class AddDeckModalComponent implements OnInit {
    invalidCardsTooltip = viewChild<TemplateRef<unknown>>('invalidCardsTooltip');

    invalidCardsTooltipOrigin = viewChild('invalidCardsTooltipOrigin', { read: ElementRef });

    form = inject(FormBuilder).group({
        name: [''],
        decklist: ['', Validators.required, this.deckModalValidator.validate.bind(this.deckModalValidator)],
    });

    private overlayRef?: OverlayRef;

    constructor(
        @Inject(MAT_DIALOG_DATA) private dialogData: { deck: Deck },
        public dialogRef: MatDialogRef<AddDeckModalComponent, AddDeckModalOuput>,
        private deckModalValidator: DeckModalValidator,
        private overlay: Overlay,
        private vcr: ViewContainerRef,
    ) {}

    get errorText() {
        const decklistFieldErrors = this.form.controls.decklist?.errors ?? {};
        if (decklistFieldErrors['required']) {
            return ERROR_EMPTY_DECK;
        }
        const decklistValidationErrors = decklistFieldErrors['decklist'];
        if (typeof decklistValidationErrors === 'string') {
            return decklistValidationErrors;
        }
        if (Array.isArray(decklistValidationErrors)) {
            return ERROR_INVALID_DECK;
        }
        return null;
    }

    get invalidCards() {
        return this.form.controls.decklist?.errors?.['decklist'] ?? [];
    }

    closeInvalidCardsTooltip() {
        if (this.overlayRef) {
            this.overlayRef.dispose();
            this.overlayRef = undefined;
        }
    }

    ngOnInit() {
        this.form.setValue({
            name: this.dialogData?.deck?.name ?? '',
            decklist: this.dialogData?.deck?.decklist ?? '',
        });
    }

    onSubmit() {
        this.form.markAsDirty();

        if (!this.form.valid) {
            return;
        }

        return this.dialogRef.close({
            ...this.dialogData?.deck,
            id: this.dialogData?.deck?.id ?? crypto.randomUUID(),
            name:
                this.form.value.name ??
                /* istanbul ignore next */
                undefined,
            cards:
                this.deckModalValidator.cachedCards ??
                /* istanbul ignore next */
                [],
            decklist:
                this.form.value.decklist ??
                /* istanbul ignore next */
                '',
        });
    }

    openInvalidCardsTooltip() {
        const invalidCardsTooltipOrigin = this.invalidCardsTooltipOrigin();
        const invalidCardsTooltip = this.invalidCardsTooltip();
        if (!invalidCardsTooltipOrigin || !invalidCardsTooltip) {
            return;
        }
        this.closeInvalidCardsTooltip();
        this.overlayRef = this.overlay.create({
            hasBackdrop: false,
            positionStrategy: this.overlay
                .position()
                .flexibleConnectedTo(invalidCardsTooltipOrigin)
                .withPositions([
                    {
                        originX: 'end',
                        originY: 'center',
                        overlayX: 'start',
                        overlayY: 'center',
                        offsetX: 8,
                    },
                    {
                        originX: 'start',
                        originY: 'center',
                        overlayX: 'end',
                        overlayY: 'center',
                        offsetX: -8,
                    },
                ]),
        });
        const portal = new TemplatePortal(invalidCardsTooltip, this.vcr, { invalidCards: this.invalidCards });
        this.overlayRef.attach(portal);
    }
}
