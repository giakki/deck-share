import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { IncompleteCollectionError } from '@shared/card-api/cards.model';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { DeckEntry } from '../analyzer.model';
import { AnalyzerService } from '../analyzer.service';

export const ERROR_EMPTY_DECK = 'Insert at least one card';
export const ERROR_INVALID_DECK = 'Decklist contains invalid cards';

@Injectable()
export class DeckModalValidator implements AsyncValidator {
    cachedCards?: DeckEntry[];

    constructor(private analyzer: AnalyzerService) {}

    validate(control: AbstractControl): Observable<ValidationErrors | null> {
        const value = typeof control.value === 'string' ? control.value : '';

        return this.analyzer.getCardsFromDecklist(value.trim() || '').pipe(
            map((cards) => {
                if (cards.length === 0) {
                    return { decklist: ERROR_EMPTY_DECK };
                }
                this.cachedCards = cards;
                return null;
            }),
            catchError((err) =>
                err instanceof IncompleteCollectionError
                    ? of({ decklist: err.identifiers })
                    : of({ decklist: err.message }),
            ),
        );
    }
}
