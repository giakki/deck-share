import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { mkCard } from '@mocks/card.mock';
import { mkCollection } from '@mocks/collection.mock';
import { mkDeck, mkDeckEntry } from '@mocks/deck.mock';
import { provideMockStore } from '@ngrx/store/testing';
import { Card } from '@shared/card-api/cards.model';
import { CardsService } from '@shared/card-api/cards.service';
import * as tu from '@tests/util';
import { of } from 'rxjs';

import { AddDeckModalComponent } from './deck-modal.component';
import { ERROR_EMPTY_DECK, ERROR_INVALID_DECK } from './deck-modal.validator';

describe('DeckModalComponent', () => {
    let component: AddDeckModalComponent;
    let fixture: ComponentFixture<AddDeckModalComponent>;
    let mockModal: any;

    function getDecklistInput() {
        return fixture.debugElement.query(By.css('textarea'))?.nativeElement;
    }

    function getDeckNameInput() {
        return fixture.debugElement.query(By.css('input'))?.nativeElement;
    }

    function getValidation() {
        return fixture.debugElement.query(By.css('mat-error'));
    }

    function getHelpIcon() {
        return fixture.debugElement.query(By.css('.invalid-cards-help'));
    }

    beforeEach(async () => {
        mockModal = { close: jasmine.createSpy() };
        await TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                MatDialogModule,
                MatFormFieldModule,
                MatIconModule,
                MatInputModule,
                AddDeckModalComponent,
            ],
            providers: [
                provideMockStore(),
                {
                    provide: MatDialogRef,
                    useValue: mockModal,
                },
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: {},
                },
                provideHttpClient(withInterceptorsFromDi()),
                provideHttpClientTesting(),
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AddDeckModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('children', () => {
        it('should have an input child', () => {
            expect(getDecklistInput()).toBeTruthy();
        });

        it('should have a textarea child', () => {
            expect(getDecklistInput()).toBeTruthy();
        });
    });

    describe('validation', () => {
        it('should check for an empty deck on submit', () => {
            spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() => of(mkCollection()));

            tu.setInputValue(fixture, getDecklistInput(), '//TEST\nTEST');
            fixture.detectChanges();
            tu.submit(fixture);

            expect(getValidation().nativeElement.innerHTML).toMatch(ERROR_EMPTY_DECK);
        });

        it('should request submitted cards to the service', () => {
            const spy = spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() => of(mkCollection()));

            tu.setInputValue(fixture, getDecklistInput(), '1 test1\n2 test2');
            tu.submit(fixture);

            expect(spy).toHaveBeenCalledOnceWith([{ name: 'test1' }, { name: 'test2' }]);
        });

        it('should report if there are missing cards', () => {
            spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                of(mkCollection({ notFound: [{ name: 'test' }] })),
            );

            tu.setInputValue(fixture, getDecklistInput(), '1 test1\n2 test2');
            tu.submit(fixture);

            expect(getValidation().nativeElement.innerHTML).toMatch(ERROR_INVALID_DECK);
        });

        it('should show an icon if there are missing cards', () => {
            spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                of(mkCollection({ notFound: [{ name: 'test' }] })),
            );

            tu.setInputValue(fixture, getDecklistInput(), '1 test1\n2 test2');
            tu.submit(fixture);

            expect(getHelpIcon()).toBeTruthy();
        });

        it('should show open a tooltip while hovering over the missing cards icon', () => {
            spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                of(mkCollection({ notFound: [{ name: 'test' }] })),
            );
            tu.setInputValue(fixture, getDecklistInput(), '1 test1\n2 test2');
            tu.submit(fixture);
            const icon = getHelpIcon();
            const spyOpen = spyOn(component, 'openInvalidCardsTooltip').and.callThrough();
            const spyClose = spyOn(component, 'closeInvalidCardsTooltip').and.callThrough();
            icon.nativeElement.dispatchEvent(
                new MouseEvent('mouseenter', {
                    view: window,
                    bubbles: true,
                    cancelable: true,
                }),
            );
            icon.nativeElement.dispatchEvent(
                new MouseEvent('mouseleave', {
                    view: window,
                    bubbles: true,
                    cancelable: true,
                }),
            );

            expect(spyOpen).toHaveBeenCalledTimes(1);
            expect(spyClose).toHaveBeenCalledTimes(2);
        });

        it('should handle the exceptional case of there not being a tooltip origin', () => {
            expect(() => component.openInvalidCardsTooltip()).not.toThrow();
        });

        it('should report if the deck is valid', () => {
            spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() =>
                of(
                    mkCollection({
                        cards: [mkCard({ name: 'test' })],
                        notFound: [],
                    }),
                ),
            );

            tu.setInputValue(fixture, getDecklistInput(), '1 test');

            expect(component.errorText).toEqual(null);
            expect(getValidation()).toBeFalsy();
        });
    });

    describe('submit', () => {
        let card: Card;

        beforeEach(() => {
            card = mkCard();

            spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() => of(mkCollection({ cards: [card] })));
        });

        it('should read the deck name', () => {
            const expectedDeck = {
                id: jasmine.any(String),
                cards: [mkDeckEntry({ amount: 1, id: card.id, name: card.name })],
                name: crypto.randomUUID(),
                decklist: `1 ${card.name}`,
            };

            tu.setInputValue(fixture, getDecklistInput(), `1 ${card.name}`);
            tu.setInputValue(fixture, getDeckNameInput(), expectedDeck.name);
            fixture.detectChanges();
            tu.submit(fixture);

            expect(mockModal.close).toHaveBeenCalledOnceWith(expectedDeck);
        });

        it('should not require inputting a deck name', () => {
            const expectedDeck = {
                id: jasmine.any(String),
                cards: [mkDeckEntry({ amount: 1, id: card.id, name: card.name })],
                name: '',
                decklist: `1 ${card.name}`,
            };

            tu.setInputValue(fixture, getDecklistInput(), `1 ${card.name}`);
            tu.setInputValue(fixture, getDeckNameInput(), '');
            fixture.detectChanges();
            tu.submit(fixture);

            expect(mockModal.close).toHaveBeenCalledOnceWith(expectedDeck);
        });

        it('should read the deck from the validator', () => {
            const initialDeck = mkDeck();
            const expectedDeck = {
                ...initialDeck,
                id: jasmine.any(String),
                cards: [mkDeckEntry({ amount: 1, id: card.id, name: card.name })],
                decklist: `1 ${card.name}`,
            };

            tu.setInputValue(fixture, getDecklistInput(), `1 ${card.name}`);
            tu.setInputValue(fixture, getDeckNameInput(), expectedDeck.name);
            fixture.detectChanges();
            tu.submit(fixture);

            expect(mockModal.close).toHaveBeenCalledOnceWith(expectedDeck);
        });
    });
});
