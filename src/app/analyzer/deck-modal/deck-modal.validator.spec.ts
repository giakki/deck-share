import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { mkCard } from '@mocks/card.mock';
import { mkCollection } from '@mocks/collection.mock';
import { provideMockStore } from '@ngrx/store/testing';
import { Collection } from '@shared/card-api/cards.model';
import { CardsService } from '@shared/card-api/cards.service';
import { of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { DeckModalValidator, ERROR_EMPTY_DECK } from './deck-modal.validator';

describe('DeckModalValidator', () => {
    let validator: DeckModalValidator;
    let testScheduler: TestScheduler;
    let mockCollection: Collection;
    let collectionSpy: jasmine.Spy;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [],
            providers: [
                provideMockStore(),
                DeckModalValidator,
                provideHttpClient(withInterceptorsFromDi()),
                provideHttpClientTesting(),
            ],
        }).compileComponents();
        validator = TestBed.inject(DeckModalValidator);

        mockCollection = mkCollection();
        collectionSpy = spyOn(TestBed.inject(CardsService), 'collection').and.callFake(() => of(mockCollection));

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    it('should create', () => {
        expect(validator).toBeTruthy();
    });

    describe('basic validation', () => {
        it('should reject an empty deck', () => {
            testScheduler.run(({ expectObservable }) => {
                expectObservable(validator.validate(new FormControl())).toBe('(a|)', {
                    a: { decklist: ERROR_EMPTY_DECK },
                });
                expectObservable(validator.validate(new FormControl(''))).toBe('(a|)', {
                    a: { decklist: ERROR_EMPTY_DECK },
                });
            });
        });

        it('should reject a deck with no cards', () => {
            testScheduler.run(({ expectObservable }) => {
                expectObservable(validator.validate(new FormControl('//Comment\nSide'))).toBe('(a|)', {
                    a: { decklist: ERROR_EMPTY_DECK },
                });
            });
        });
    });

    describe('deck format', () => {
        it('should ask the api for the cards', () => {
            const expected = [{ name: 'test1' }, { name: 'test2' }];
            testScheduler.run(() => {
                validator.validate(new FormControl('1 test1\n2 test2')).subscribe();

                expect(collectionSpy).toHaveBeenCalledOnceWith(expected);
            });
        });

        it('should reject a deck with missing cards', () => {
            mockCollection = mkCollection({
                notFound: [{ name: 'test' }],
            });
            testScheduler.run(({ expectObservable }) => {
                expectObservable(validator.validate(new FormControl('__IGNORED__'))).toBe('(a|)', {
                    a: { decklist: mockCollection.notFound },
                });
            });
        });

        it('should accept a correctly formatted deck', () => {
            mockCollection = mkCollection({
                cards: [mkCard({ name: 'test1' }), mkCard({ name: 'test2' })],
            });
            testScheduler.run(({ expectObservable }) => {
                expectObservable(validator.validate(new FormControl('1 test1\n2 test2'))).toBe('(a|)', {
                    a: null,
                });
            });
        });

        it('should cache the validated deck', () => {
            const expectedText = '1 test1\n2 test2';
            mockCollection = mkCollection({
                cards: [mkCard({ name: 'test1' }), mkCard({ name: 'test2' })],
            });
            testScheduler.run(() => {
                validator.validate(new FormControl(expectedText)).subscribe();

                expect(validator.cachedCards).toEqual([
                    {
                        id: mockCollection.cards[0].id,
                        name: mockCollection.cards[0].name,
                        amount: 1,
                    },
                    {
                        id: mockCollection.cards[1].id,
                        name: mockCollection.cards[1].name,
                        amount: 2,
                    },
                ]);
            });
        });
    });
});
