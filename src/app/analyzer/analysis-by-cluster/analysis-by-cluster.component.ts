import { ChangeDetectionStrategy, Component, inject, input, OnChanges, output, SimpleChanges } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatTableModule } from '@angular/material/table';

import { AnalysisResultsByCluster, AnalysisSettingsByCluster } from '../analyzer.model';

@Component({
    selector: 'az-analysis-by-cluster',
    templateUrl: './analysis-by-cluster.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [ReactiveFormsModule, MatFormFieldModule, MatSliderModule, MatTableModule],
})
export class AnalysisByClusterComponent implements OnChanges {
    results = input<AnalysisResultsByCluster>();

    settings = input<AnalysisSettingsByCluster>();

    settingsChange = output<Partial<AnalysisSettingsByCluster>>();

    form = inject(FormBuilder).group({
        numClusters: [this.settings()?.numClusters ?? null, Validators.required],
        numIterations: [this.settings()?.numIterations ?? null, Validators.required],
    });

    ngOnChanges({ settings }: SimpleChanges) {
        if (settings) {
            this.updateForm();
        }
    }

    onSettingsChange() {
        const { numClusters, numIterations } = this.form.value;
        this.settingsChange.emit({
            numClusters: numClusters ?? undefined,
            numIterations: numIterations ?? undefined,
        });
    }

    private updateForm() {
        const settings = this.settings();
        this.form.setValue(
            {
                numClusters: settings?.numClusters ?? null,
                numIterations: settings?.numIterations ?? null,
            },
            { emitEvent: false },
        );
    }
}
