import { Component, OutputRefSubscription } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { MatTableModule } from '@angular/material/table';
import { By } from '@angular/platform-browser';
import { mkAnalysisResultsByCluster } from '@mocks/analysis';
import { ReplaySubject } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { AnalysisResultsByCluster, AnalysisSettingsByCluster } from '../analyzer.model';
import { AnalysisByClusterComponent } from './analysis-by-cluster.component';

@Component({
    template: `<az-analysis-by-cluster [results]="results" [settings]="settings"></az-analysis-by-cluster>`,
    standalone: true,
    imports: [AnalysisByClusterComponent],
})
class TestHostComponent {
    results?: AnalysisResultsByCluster;
    settings?: AnalysisSettingsByCluster;
}

describe('AnalysisByClusterComponent', () => {
    let component: AnalysisByClusterComponent;
    let hostComponent: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;
    let testScheduler: TestScheduler;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
                MatFormFieldModule,
                MatIconModule,
                MatSliderModule,
                MatTableModule,
                TestHostComponent,
            ],
        }).compileComponents();

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        hostComponent = fixture.componentInstance;
        fixture.detectChanges();
        component = fixture.debugElement.query(By.directive(AnalysisByClusterComponent)).componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('ui', () => {
        it('should have a row for each entry', () => {
            const expectedNumRows = 5;
            hostComponent.results = mkAnalysisResultsByCluster(expectedNumRows);
            fixture.detectChanges();

            const rows = fixture.debugElement.queryAll(By.css('tbody tr'));
            expect(rows).toHaveSize(expectedNumRows);
        });

        it('should show the index in the first column', () => {
            hostComponent.results = mkAnalysisResultsByCluster(6);
            fixture.detectChanges();

            const tds = fixture.debugElement.queryAll(By.css('tbody td:nth-child(1)'));

            expect(tds).toHaveSize(6);
            for (const [i, td] of tds.entries()) {
                expect(td.nativeElement.innerHTML.trim()).toEqual(i.toString());
            }
        });

        it('should show the deck numbers in the second column', () => {
            hostComponent.results = mkAnalysisResultsByCluster(7);
            fixture.detectChanges();

            const tds = fixture.debugElement.queryAll(By.css('tbody td:nth-child(2)'));

            expect(tds).toHaveSize(7);
            for (const [i, td] of tds.entries()) {
                expect(td.nativeElement.innerHTML.trim()).toEqual(hostComponent.results[i].join(', '));
            }
        });
    });

    describe('events', () => {
        it('should update the form when the settings change', fakeAsync(() => {
            hostComponent.settings = { numClusters: 10, numIterations: 100, randomSeed: [0, 0, 0, 0] };
            fixture.detectChanges();

            const numClustersInput = fixture.debugElement.query(
                By.css('input[formControlName="numClusters"]'),
            ).nativeElement;
            const numIterationsInput = fixture.debugElement.query(
                By.css('input[formControlName="numIterations"]'),
            ).nativeElement;

            expect(numClustersInput.value).toEqual(component.settings()?.numClusters.toString());
            expect(numIterationsInput.value).toEqual(component.settings()?.numIterations.toString());
        }));

        it('should emit an event when the number of clusters change', () => {
            let sub: OutputRefSubscription | undefined;

            testScheduler.run(({ expectObservable }) => {
                const obs = new ReplaySubject(2);
                sub = component.settingsChange.subscribe((v) => obs.next(v));

                const input = fixture.debugElement.query(By.css('input[formControlName="numClusters"]')).nativeElement;
                input.value = 5;
                input.dispatchEvent(new Event('input'));
                input.dispatchEvent(new Event('change'));

                expectObservable(obs).toBe('a', {
                    a: { numClusters: 5, numIterations: undefined },
                });
            });
            sub?.unsubscribe();
        });

        it('should emit an event when the number of iterations change', () => {
            testScheduler.run(({ expectObservable }) => {
                const obs = new ReplaySubject(2);
                component.settingsChange.subscribe((v) => obs.next(v));

                const input = fixture.debugElement.query(
                    By.css('input[formControlName="numIterations"]'),
                ).nativeElement;
                input.value = 1000;
                input.dispatchEvent(new Event('input'));
                input.dispatchEvent(new Event('change'));

                expectObservable(obs).toBe('a', { a: { numClusters: undefined, numIterations: 1000 } });
            });
        });
    });
});
