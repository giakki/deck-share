import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { DocumentChange } from '@angular/fire/firestore';
import { mkDeck } from '@mocks/deck.mock';
import { provideMockFirebaseApp, provideMockFirebaseAuth, provideMockFirestore } from '@mocks/firebase';
import { MemoizedSelector } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import firebase from '@shared/firebase';
import times from 'ramda/es/times';
import { ReplaySubject, tap } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { makeUser, User } from '../auth/auth.reducer';
import { selectAuthUser } from '../auth/auth.selectors';
import { analyzerActions } from './analyzer.actions';
import { AnalyzerFirestore, DeckConverter } from './analyzer.firestore';
import { Deck } from './analyzer.model';

describe('DeckConverter', () => {
    let converter: DeckConverter;

    beforeEach(() => {
        converter = new DeckConverter();
    });

    it('should not convert objects going into the store', () => {
        const expected = mkDeck();

        expect(converter.toFirestore(expected)).toBe(expected);
    });

    it('should correctly convert objects from the firestore', () => {
        const expected = mkDeck();

        const actual = converter.fromFirestore({
            data() {
                return expected;
            },
        } as any);

        expect(actual).toEqual(expected);
    });
});

describe('AnalyzerFirestore', () => {
    let service: AnalyzerFirestore;
    let mockStore: MockStore;
    let selectMockUser: MemoizedSelector<any, User | null>;
    let testScheduler: TestScheduler;

    let mockCollection: any;
    let mockDoc: any;
    let docSpy: jasmine.Spy;
    let docChangesSpy: jasmine.Spy;
    let converterSpy: jasmine.Spy;
    let collectionSpy: jasmine.Spy;
    let getDocsSpy: jasmine.Spy;
    let setDocSpy: jasmine.Spy;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockFirebaseApp(),
                provideMockFirebaseAuth(),
                provideMockFirestore(),
                provideMockStore(),
            ],
        });

        service = TestBed.inject(AnalyzerFirestore);

        mockStore = TestBed.inject(MockStore);
        selectMockUser = mockStore.overrideSelector(selectAuthUser, null);

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });

        mockCollection = crypto.randomUUID();
        mockDoc = crypto.randomUUID();

        docSpy = spyOn(firebase.firestore, 'doc').and.returnValue(mockDoc);
        docChangesSpy = jasmine.createSpy('docChanges').and.returnValue([]);
        converterSpy = jasmine.createSpy('converterSpy').and.returnValue(mockCollection);
        collectionSpy = spyOn(firebase.firestore, 'collection').and.returnValues({
            withConverter: converterSpy,
        } as any);
        getDocsSpy = spyOn(firebase.firestore, 'getDocs').and.resolveTo({
            docChanges: docChangesSpy,
        } as any);
        setDocSpy = spyOn(firebase.firestore, 'setDoc').and.resolveTo(mockDoc);
        spyOn(firebase.performance, 'traceUntilFirst').and.returnValue(tap());
    });

    afterEach(() => {
        mockStore.resetSelectors();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('decksActions$', () => {
        it('should return nothing if the user is not logged in', () => {
            testScheduler.run(({ expectObservable }) => {
                expectObservable(service.decksActions$).toBe('');
                expect(collectionSpy).not.toHaveBeenCalled();
            });
        });

        it('should call the correct methods', fakeAsync(() => {
            const mockUser = makeUser(crypto.randomUUID());
            selectMockUser.setResult(mockUser);
            mockStore.refreshState();

            service.decksActions$.subscribe();

            tick();

            expect(docChangesSpy).toHaveBeenCalledTimes(1);
            expect(converterSpy).toHaveBeenCalledOnceWith(jasmine.any(DeckConverter));
            expect(collectionSpy).toHaveBeenCalledOnceWith(
                jasmine.any(Object),
                `/apps/deck-share/analyzer/${mockUser.uid}/decks`,
            );
            expect(getDocsSpy).toHaveBeenCalledOnceWith(mockCollection);
        }));

        it('should map docChanges to actions', fakeAsync(() => {
            const replay = new ReplaySubject();
            const decks = times(() => mkDeck(), 3);
            const changes: DocumentChange<Deck>[] = [
                {
                    doc: { data: () => decks[0] } as any,
                    newIndex: 0,
                    oldIndex: 0,
                    type: 'added',
                },
                {
                    doc: { data: () => decks[1] } as any,
                    newIndex: 0,
                    oldIndex: 0,
                    type: 'modified',
                },
                {
                    doc: { data: () => decks[2] } as any,
                    newIndex: 0,
                    oldIndex: 0,
                    type: 'removed',
                },
            ];

            docChangesSpy.and.returnValues(changes);
            selectMockUser.setResult(makeUser(crypto.randomUUID()));
            mockStore.refreshState();

            service.decksActions$.subscribe(replay);

            tick();

            testScheduler.run(({ expectObservable }) => {
                expectObservable(replay).toBe('(abc)', {
                    a: analyzerActions.deckUpsert({ deck: changes[0].doc.data() }),
                    b: analyzerActions.deckUpsert({ deck: changes[1].doc.data() }),
                    c: analyzerActions.deckRemove({ deck: changes[2].doc.data() }),
                });
            });
        }));

        it('should catch firebase errors', fakeAsync(() => {
            const replay = new ReplaySubject();
            selectMockUser.setResult(makeUser(crypto.randomUUID()));
            mockStore.refreshState();
            getDocsSpy.and.rejectWith(new Error());

            service.decksActions$.subscribe();

            tick();

            testScheduler.run(({ expectObservable }) => {
                expectObservable(replay).toBe('');
            });
        }));
    });

    describe('upsertDeck', () => {
        it('should do nothing if the user is not logged in', (done) => {
            service.upsertDeck(mkDeck()).subscribe({
                next: () => fail(new Error('Unexpected value')),
                error: (err) => fail(err),
                complete: () => {
                    expect(true).toBeTrue();
                    done();
                },
            });
        });

        it('should call the correct methods', fakeAsync(() => {
            const replay = new ReplaySubject();
            const deck = mkDeck();
            selectMockUser.setResult(makeUser(crypto.randomUUID()));
            mockStore.refreshState();

            service.upsertDeck(deck).subscribe(replay);

            testScheduler.run(({ expectObservable }) => {
                expectObservable(replay).toBe('(a|)', { a: mockDoc });
                tick();
                expect(docSpy).toHaveBeenCalledOnceWith(mockCollection, deck.id);
                expect(setDocSpy).toHaveBeenCalledOnceWith(mockDoc, deck, { merge: true });
            });
        }));
    });
});
