import { mkAnalysisResults } from '@mocks/analysis';
import { mkDeck } from '@mocks/deck.mock';

import { analyzerActions } from './analyzer.actions';

describe('deckEditRequest', () => {
    it('should return an action', () => {
        expect(analyzerActions.deckEditRequest({}).type).toBe('[Analyzer] Deck Edit Request');
    });

    it('should wrap its props', () => {
        const expected = crypto.randomUUID();
        expect(analyzerActions.deckEditRequest({ id: expected }).id).toBe(expected);
    });
});

describe('deckEditCancelled', () => {
    it('should return an action', () => {
        expect(analyzerActions.deckEditCancelled().type).toBe('[Analyzer] Deck Edit Cancelled');
    });
});

describe('deckEditError', () => {
    it('should return an action', () => {
        expect(analyzerActions.deckEditError({ message: '' }).type).toBe('[Analyzer] Deck Edit Error');
    });

    it('should wrap its props', () => {
        const expected = crypto.randomUUID();
        expect(analyzerActions.deckEditError({ message: expected }).message).toBe(expected);
    });
});

describe('deckUpsert', () => {
    it('should return an action', () => {
        expect(analyzerActions.deckUpsert({ deck: mkDeck() }).type).toBe('[Analyzer] Deck Upsert');
    });

    it('should wrap its props', () => {
        const expected = mkDeck();
        expect(analyzerActions.deckUpsert({ deck: expected }).deck).toEqual(expected);
    });
});

describe('runAnalysis', () => {
    it('should return an action', () => {
        expect(analyzerActions.runAnalysis().type).toBe('[Analyzer] Run Analysis');
    });
});

describe('runAnalysisError', () => {
    it('should return an action', () => {
        expect(analyzerActions.runAnalysisError({ message: '' }).type).toBe('[Analyzer] Run Analysis Error');
    });

    it('should wrap its props', () => {
        const expected = crypto.randomUUID();
        expect(analyzerActions.runAnalysisError({ message: expected }).message).toBe(expected);
    });
});

describe('runAnalysisSuccess', () => {
    it('should return an action', () => {
        expect(analyzerActions.runAnalysisSuccess({ results: mkAnalysisResults() }).type).toBe(
            '[Analyzer] Run Analysis Success',
        );
    });

    it('should wrap its props', () => {
        const expected = mkAnalysisResults();
        expect(analyzerActions.runAnalysisSuccess({ results: expected }).results).toBe(expected);
    });
});
