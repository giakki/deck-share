import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { By } from '@angular/platform-browser';
import { mkDeckEntry } from '@mocks/deck.mock';

import { Deck } from '../analyzer.model';
import { ExampleDataState } from '../analyzer.reducer';
import { DeckListComponent } from './deck-list.component';

function mkDecks(num: number): Deck[] {
    return Array.from({ length: num })
        .fill(0)
        .map(() => ({
            id: crypto.randomUUID(),
            cards: [],
            name: crypto.randomUUID(),
            decklist: crypto.randomUUID(),
        }));
}

describe('DeckListComponent', () => {
    let component: DeckListComponent;
    let fixture: ComponentFixture<DeckListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [FormsModule, MatListModule, MatProgressSpinnerModule, DeckListComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DeckListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('children', () => {
        it('should contain only one button if there are no decks', () => {
            const results = fixture.debugElement.queryAll(By.css('button'));

            expect(results).toHaveSize(1);
        });

        it('should contain a button for each deck present', () => {
            const decks = mkDecks(5);
            fixture.componentRef.setInput('decks', decks);

            fixture.detectChanges();

            const results = fixture.debugElement.queryAll(By.css('button'));

            expect(results).toHaveSize(6);
        });

        it('should show the number of cards in each deck', () => {
            const decks = mkDecks(5);
            decks[2].cards = [mkDeckEntry({ amount: 4 }), mkDeckEntry({ amount: 1 })];
            fixture.componentRef.setInput('decks', decks);

            fixture.detectChanges();

            const result = fixture.debugElement.query(By.css('button:nth-child(5)'));

            expect(result.nativeElement.innerText.trim()).toEqual(`${decks[2].name}\n5 cards`);
        });

        it('should show an error', () => {
            const decks = mkDecks(5);
            decks[2].cards = [mkDeckEntry({ amount: 4 }), mkDeckEntry({ amount: 1 })];
            fixture.componentRef.setInput('decks', decks);

            fixture.detectChanges();

            const result = fixture.debugElement.query(By.css('button:nth-child(5)'));

            expect(result.nativeElement.innerText.trim()).toEqual(`${decks[2].name}\n5 cards`);
        });

        it("should show the deck's index if it has no name", () => {
            const decks = mkDecks(5);
            decks[2].name = '';
            fixture.componentRef.setInput('decks', decks);

            fixture.detectChanges();

            const result = fixture.debugElement.query(By.css('button:nth-child(5)'));

            expect(result.nativeElement.innerText.trim()).toEqual(`Deck 3\n0 cards`);
        });
    });

    describe('events', () => {
        it('the active button should trigger an "add deck" event', () => {
            let didFire = false;
            component.addDeck.subscribe(() => (didFire = true));

            fixture.debugElement.query(By.css('button:first-child'))?.nativeElement.click();

            expect(didFire).toBeTrue();
        });

        it('the deck buttons should trigger an "edit deck" event', () => {
            const decks = mkDecks(5);
            fixture.componentRef.setInput('decks', decks);
            fixture.detectChanges();

            const buttons = fixture.debugElement.queryAll(By.css('[data-deck]'));
            expect(buttons).toHaveSize(5);

            for (const [i, button] of buttons.entries()) {
                let didFire = '';
                component.editDeck.subscribe((id) => (didFire = id));
                button?.nativeElement.click();
                expect(didFire).toEqual(component.decks()[i]?.id);
            }
        });

        it('the example data button should trigger a "load example data" event', () => {
            fixture.componentRef.setInput('exampleDataState', { state: 'not loaded' });
            fixture.detectChanges();

            const button = fixture.debugElement.query(By.css('button:last-child'));

            let didFire = false;
            component.loadExampleData.subscribe(() => (didFire = true));
            button?.nativeElement.click();
            expect(didFire).toEqual(true);
        });
    });

    describe('example data', () => {
        describe('button', () => {
            it('the example data button should be shown if the example data is not loaded', () => {
                const state: ExampleDataState = { state: 'not loaded' };
                fixture.componentRef.setInput('exampleDataState', state);
                fixture.detectChanges();

                const button = fixture.debugElement.query(By.css('button:last-child'));
                expect(button).toBeTruthy();
            });

            it('the example data button should not be shown if the example data is loaded', () => {
                const states: ExampleDataState[] = [
                    { state: 'loaded', errors: 0 },
                    { state: 'loading', errors: 0, progress: 0 },

                    { state: 'error' },
                ];

                for (const state of states) {
                    fixture.componentRef.setInput('exampleDataState', state);
                    fixture.detectChanges();

                    const button = fixture.debugElement.query(By.css('button:last-child'));
                    expect(button).toBeFalsy();
                }
            });
        });

        describe('loading', () => {
            it('the loading text should have a primary color', () => {
                fixture.componentRef.setInput('exampleDataState', {
                    state: 'loading',
                    errors: 0,
                    progress: Math.random(),
                });
                fixture.detectChanges();

                const button = fixture.debugElement.query(By.css('[data-loading]'));
                expect(button).toBeTruthy();
            });

            it('should show the progress when loading', () => {
                const progress = Math.random();
                fixture.componentRef.setInput('exampleDataState', { state: 'loading', errors: 0, progress });
                fixture.detectChanges();

                const button = fixture.debugElement.query(By.css('[data-loading]'));
                expect(button?.nativeElement.innerText.trim()).toEqual(`Loading: ${progress}%`);
            });
        });

        describe('success', () => {
            it('the success text should have a success color', () => {
                fixture.componentRef.setInput('exampleDataState', { state: 'loaded', errors: 0 });
                fixture.detectChanges();

                const button = fixture.debugElement.query(By.css('[data-loaded]'));
                expect(button).toBeTruthy();
            });

            it('the success text should contain the number of errors', () => {
                const errors = Math.random();
                fixture.componentRef.setInput('exampleDataState', { state: 'loaded', errors });
                fixture.detectChanges();

                const button = fixture.debugElement.query(By.css('[data-loaded]'));
                expect(button?.nativeElement.innerText.trim()).toContain(`${errors}`);
            });
        });

        describe('error', () => {
            it('the error text should have an error color', () => {
                fixture.componentRef.setInput('exampleDataState', { state: 'error' });
                fixture.detectChanges();

                const button = fixture.debugElement.query(By.css('[data-error'));
                expect(button).toBeTruthy();
            });
        });
    });
});
