import { ChangeDetectionStrategy, Component, input, output } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { Deck } from '../analyzer.model';
import { ExampleDataState } from '../analyzer.reducer';

@Component({
    selector: 'az-deck-list',
    templateUrl: './deck-list.component.html',
    styleUrl: './deck-list.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [MatListModule, MatProgressSpinnerModule],
})
export class DeckListComponent {
    decks = input<Deck[]>([]);

    exampleDataState = input<ExampleDataState>();

    addDeck = output<void>();

    editDeck = output<string>();

    loadExampleData = output<void>();

    getExampleDataStateErrorText() {
        const exampleDataState = this.exampleDataState();
        if (exampleDataState && 'errors' in exampleDataState && exampleDataState.errors > 0) {
            return `Errors: ${exampleDataState.errors}`;
        }
        return '';
    }

    getExampleDataStateProgressText() {
        const exampleDataState = this.exampleDataState();
        if (exampleDataState && 'progress' in exampleDataState) {
            return `Loading: ${exampleDataState.progress}%`;
        }
        /* istanbul ignore next */
        return '';
    }

    getNumCardsInDeck(deck: Deck) {
        return deck.cards.reduce((sum, card) => sum + card.amount, 0);
    }

    isExampleDataNotLoaded() {
        const exampleDataState = this.exampleDataState();

        return exampleDataState && exampleDataState.state === 'not loaded';
    }
}
