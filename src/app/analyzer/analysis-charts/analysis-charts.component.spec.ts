import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { mkAnalysisResults } from '@mocks/analysis';
import { Chart } from 'chart.js';

import { AnalysisResults } from '../analyzer.model';
import { AnalysisChartsComponent } from './analysis-charts.component';

@Component({
    template: `<az-analysis-charts [byColor]="results?.byColor" [byType]="results?.byType"></az-analysis-charts>`,
    standalone: true,
    imports: [AnalysisChartsComponent],
})
class TestHostComponent {
    results?: AnalysisResults;
}

describe('AnalysisChartsComponent', () => {
    let host: TestHostComponent;
    let component: AnalysisChartsComponent;
    let fixture: ComponentFixture<TestHostComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [TestHostComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        host = fixture.componentInstance;
        component = fixture.debugElement.query(By.directive(AnalysisChartsComponent)).componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should not have charts when the results are empty', () => {
        expect(component.byColorChart).toBeUndefined();
        expect(component.byTypeChart).toBeUndefined();
    });

    it('should create charts', () => {
        host.results = mkAnalysisResults();

        fixture.detectChanges();

        expect(component.byColorChart).toBeInstanceOf(Chart);
        expect(component.byTypeChart).toBeInstanceOf(Chart);
    });

    it('should update charts', () => {
        host.results = mkAnalysisResults();
        fixture.detectChanges();

        host.results = mkAnalysisResults();
        fixture.detectChanges();

        expect(component.byColorChart).toBeInstanceOf(Chart);
        expect(component.byTypeChart).toBeInstanceOf(Chart);
    });
});
