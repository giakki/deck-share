import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    input,
    OnChanges,
    SimpleChanges,
    viewChild,
} from '@angular/core';
import { CardColor, CardType } from '@shared/card-api/cards.model';
import { PieChartData } from '@shared/charts';
import {
    ArcElement,
    Chart,
    ChartConfiguration,
    ChartTypeRegistry,
    Legend,
    PieController,
    RadialLinearScale,
    ScriptableContext,
    Tooltip,
} from 'chart.js';

Chart.register(ArcElement, Legend, PieController, RadialLinearScale, Tooltip);

const COLORS = ['#c8d6e5', '#10ac84', '#ff9f43', '#ff9ff3', '#341f97', '#ee5253', '#54a0ff'];

@Component({
    selector: 'az-analysis-charts',
    templateUrl: './analysis-charts.component.html',
    styleUrl: './analysis-charts.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
})
export class AnalysisChartsComponent implements OnChanges {
    byColor = input<PieChartData<CardColor, number>>();

    byType = input<PieChartData<CardType, number>>();

    colorPieChart = viewChild<ElementRef<HTMLCanvasElement>>('colorPieChart');

    typePieChart = viewChild<ElementRef<HTMLCanvasElement>>('typePieChart');

    byColorChart?: Chart;
    byTypeChart?: Chart;

    ngOnChanges({ byColor, byType }: SimpleChanges) {
        if (byColor) {
            this.updateByColorChart();
        }
        if (byType) {
            this.updateByTypeChart();
        }
    }

    private makePieChartConfig({
        labels,
        data,
    }: PieChartData<unknown, number>): ChartConfiguration<keyof ChartTypeRegistry, number[], unknown> {
        return {
            type: 'pie',
            data: {
                labels,
                datasets: [
                    {
                        data,
                        backgroundColor(ctx: ScriptableContext<'doughnut'>) {
                            return COLORS[ctx.dataIndex];
                        },
                    },
                ],
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                },
            },
        };
    }

    private updateByColorChart() {
        if (this.byColorChart) {
            this.byColorChart.destroy();
        }
        const chart = this.colorPieChart();
        const data = this.byColor();
        if (chart && data) {
            this.byColorChart = new Chart(chart.nativeElement, this.makePieChartConfig(data));
        }
    }

    private updateByTypeChart() {
        if (this.byTypeChart) {
            this.byTypeChart.destroy();
        }
        const chart = this.typePieChart();
        const data = this.byType();
        if (chart && data) {
            this.byTypeChart = new Chart(chart.nativeElement, this.makePieChartConfig(data));
        }
    }
}
