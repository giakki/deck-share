import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { DeepPartial } from '@utils/types';

import { AnalysisResults, AnalysisSettings, Deck } from './analyzer.model';

export const analyzerActions = createActionGroup({
    source: 'Analyzer',
    events: {
        'Deck Edit Request': props<{ id?: string }>(),
        'Deck Edit Cancelled': emptyProps(),
        'Deck Edit Error': props<{ message: string }>(),
        'Deck Upsert': props<{ deck: Deck }>(),
        'Deck Remove': props<{ deck: Deck }>(),
        'Example Data Load Complete': emptyProps(),
        'Example Data Load Error': emptyProps(),
        'Example Data Loading Add Error': emptyProps(),
        'Example Data Loading Set Progress': props<{ progress: number }>(),
        'Example Data Load Request': emptyProps(),
        'Patch Settings': props<{ settings: DeepPartial<AnalysisSettings> }>(),
        'Run Analysis': emptyProps(),
        'Run Analysis Error': props<{ message: string }>(),
        'Run Analysis Success': props<{ results: AnalysisResults }>(),
    },
});
