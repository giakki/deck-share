import { mkAnalysisResults } from '@mocks/analysis';
import { mkCard } from '@mocks/card.mock';
import clone from 'ramda/es/clone';

import { AnalysisResults, AnalysisSettings } from './analyzer.model';
import * as fromAnalyzer from './analyzer.reducer';
import {
    selectAnalyzerAnalysis,
    selectAnalyzerAnalysisRunning,
    selectAnalyzerDecks,
    selectAnalyzerExampleDataState,
    selectAnalyzerSettings,
    selectAnalyzerState,
} from './analyzer.selectors';

describe('Analyzer Selectors', () => {
    let state: any;
    let featureState: fromAnalyzer.State;

    beforeEach(() => {
        featureState = clone(fromAnalyzer.initialState);
        state = {
            [fromAnalyzer.analyzerFeatureKey]: featureState,
        };
    });

    it('should select the feature state', () => {
        const result = selectAnalyzerState(state);

        expect(result).toBe(featureState);
    });

    describe('selectAnalyzerAnalysis', () => {
        it('should select the analysis result if not present', () => {
            const result = selectAnalyzerAnalysis(state);

            expect(result).toBe(featureState.analysis);
        });

        it('should select the analysis result if present', () => {
            const expected: AnalysisResults = mkAnalysisResults({
                byFrequency: [{ card: mkCard(), amount: 1 }],
                notFound: ['y'],
            });
            featureState.analysis = expected;

            const result = selectAnalyzerAnalysis(state);

            expect(result).toBe(expected);
        });
    });

    describe('selectAnalyzerDecks ', () => {
        it('should select the decks as an array', () => {
            const deck1 = {
                cards: [],
                id: '1',
                name: '2',
                decklist: '_1_',
            };
            const deck2 = {
                cards: [],
                id: '2',
                name: '2',
                decklist: '_2_',
            };
            featureState.decks.entities = {
                '1': deck1,
                '2': deck2,
            };
            featureState.decks.ids = ['1', '2'];

            const result = selectAnalyzerDecks(state);

            expect(result).toEqual([deck1, deck2]);
        });
    });

    describe('selectAnalyzerAnalysisRunning', () => {
        it('should select the correct value', () => {
            featureState.analysisRunning = true;

            const result = selectAnalyzerAnalysisRunning(state);

            expect(result).toEqual(true);
        });

        it('should select the correct value', () => {
            featureState.analysisRunning = false;

            const result = selectAnalyzerAnalysisRunning(state);

            expect(result).toEqual(false);
        });
    });

    describe('selectAnalyzerExampleDataState ', () => {
        it('should select the correct value', () => {
            const expected = fromAnalyzer.mkExampleDataLoaded(0);
            featureState.exampleData = expected;

            const result = selectAnalyzerExampleDataState(state);

            expect(result).toEqual(expected);
        });
    });

    describe('selectAnalyzerSettings', () => {
        it('should select the correct value', () => {
            const expected: AnalysisSettings = {
                byCluster: {
                    numClusters: Math.random(),
                    numIterations: Math.random(),
                    randomSeed: [Math.random(), Math.random(), Math.random(), Math.random()],
                },
            };
            featureState.settings = expected;

            const result = selectAnalyzerSettings(state);

            expect(result).toEqual(expected);
        });
    });
});
