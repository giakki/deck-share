import { ChangeDetectionStrategy, Component, input, output } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { DeepPartial } from '@utils/types';

import { AnalysisByClusterComponent } from '../analysis-by-cluster/analysis-by-cluster.component';
import { AnalysisByFrequencyComponent } from '../analysis-by-frequency/analysis-by-frequency.component';
import { AnalysisChartsComponent } from '../analysis-charts/analysis-charts.component';
import { AnalysisResults, AnalysisSettings, AnalysisSettingsByCluster } from '../analyzer.model';

@Component({
    selector: 'az-analysis-results',
    templateUrl: './analysis-results.component.html',
    styleUrl: './analysis-results.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [MatExpansionModule, AnalysisByClusterComponent, AnalysisChartsComponent, AnalysisByFrequencyComponent],
})
export class AnalysisResultsComponent {
    results = input<AnalysisResults>();

    settings = input<AnalysisSettings>();

    settingsChange = output<DeepPartial<AnalysisSettings>>();

    onClusterSettingsChange(byCluster: Partial<AnalysisSettingsByCluster>) {
        this.settingsChange.emit({
            byCluster,
        });
    }
}
