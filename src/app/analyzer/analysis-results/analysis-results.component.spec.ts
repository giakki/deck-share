import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule, MatExpansionPanel } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { MatTableModule } from '@angular/material/table';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { mkAnalysisResults } from '@mocks/analysis';
import { ReplaySubject } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { AnalysisByClusterComponent } from '../analysis-by-cluster/analysis-by-cluster.component';
import { AnalysisByFrequencyComponent } from '../analysis-by-frequency/analysis-by-frequency.component';
import { AnalysisChartsComponent } from '../analysis-charts/analysis-charts.component';
import { AnalysisResultsComponent } from './analysis-results.component';

describe('AnalysisResultsComponent', () => {
    let component: AnalysisResultsComponent;
    let fixture: ComponentFixture<AnalysisResultsComponent>;
    let testScheduler: TestScheduler;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                MatFormFieldModule,
                MatExpansionModule,
                MatListModule,
                MatSliderModule,
                MatTableModule,
                AnalysisResultsComponent,
                AnalysisByClusterComponent,
                AnalysisByFrequencyComponent,
                AnalysisChartsComponent,
            ],
        }).compileComponents();

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AnalysisResultsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should forward the "settingsChange" event to the store', () => {
        fixture.componentRef.setInput('results', mkAnalysisResults());
        fixture.detectChanges();

        const button = fixture.debugElement.query(By.directive(MatExpansionPanel)).nativeElement;
        button.click();
        const child = fixture.debugElement.query(By.directive(AnalysisByClusterComponent)).componentInstance;

        testScheduler.run(({ expectObservable }) => {
            const obs = new ReplaySubject(2);
            component.settingsChange.subscribe((v) => obs.next(v));

            child.settingsChange.emit({});

            expectObservable(obs).toBe('a', { a: { byCluster: {} } });
        });
    });
});
