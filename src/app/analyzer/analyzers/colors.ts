import { cardColors, Collection } from '@shared/card-api/cards.model';

import { AnalysisResultsByColor, Deck } from '../analyzer.model';

export function analyzeColors(collection: Collection, decks: Deck[]) {
    const cardsByColor: AnalysisResultsByColor = {
        white: 0,
        blue: 0,
        black: 0,
        red: 0,
        green: 0,
    };

    for (const deck of decks) {
        for (const { id, amount } of deck.cards) {
            const card = collection.cards.find((card) => card.id === id);
            if (!card) {
                continue;
            }
            for (const color of cardColors(card)) {
                cardsByColor[color] += amount;
            }
        }
    }

    for (const type in cardsByColor) {
        if (Object.hasOwnProperty.call(cardsByColor, type)) {
            cardsByColor[type as keyof typeof cardsByColor] /= decks.length;
        }
    }

    return cardsByColor;
}
