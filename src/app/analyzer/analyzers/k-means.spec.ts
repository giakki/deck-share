import { mkCard } from '@mocks/card.mock';
import { Collection } from '@shared/card-api/cards.model';
import { times } from 'ramda';

import { Deck } from '../analyzer.model';
import { analyzeKmeans } from './k-means';

describe('kmeans', () => {
    it('should produce the expected result', () => {
        // All decks have `numCommonCardsPerDeck` cards in common with another deck and one unique card.
        const numClusters = 3;
        const numCommonCardsPerDeck = 9;
        const numDecks = 9;
        const collection: Collection = {
            cards: times(() => mkCard(), numCommonCardsPerDeck * numClusters + numDecks),
            notFound: [],
        };
        const decks: Deck[] = times<Deck>((i) => {
            const commonCards = times((commonIndex) => {
                const card =
                    collection.cards[(commonIndex % numCommonCardsPerDeck) + (i % numClusters) * numCommonCardsPerDeck];
                return {
                    amount: 1,
                    id: card.id,
                    name: card.name,
                };
            }, numCommonCardsPerDeck);
            const uniqueCard = collection.cards[numCommonCardsPerDeck * numClusters + i];
            return {
                id: crypto.randomUUID(),
                cards: [
                    ...commonCards,
                    {
                        amount: 1,
                        id: uniqueCard.id,
                        name: uniqueCard.name,
                    },
                ],
                name: '',
                decklist: '',
            };
        }, numDecks);

        const result = analyzeKmeans(collection, decks, {
            numClusters,
            numIterations: 1000,
            randomSeed: [183_929_720, 936_553_056, 181_058_765, 971_326_744],
        });

        expect(result[0].sort((a, b) => a[0] - b[0])).toEqual([
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
        ]);
    });
});
