import { Collection, isBasicLand } from '@shared/card-api/cards.model';
import { isNotUndefined } from '@utils/fn';

import { Deck } from '../analyzer.model';

export function analyzeFrequency(collection: Collection, decks: Deck[]) {
    const cardsByFrequency: Record<string, number> = {};
    for (const deck of decks) {
        for (const card of deck.cards) {
            if (card.name in cardsByFrequency) {
                cardsByFrequency[card.name] += card.amount;
            } else {
                cardsByFrequency[card.name] = card.amount;
            }
        }
    }

    return Object.entries(cardsByFrequency)
        .map(([name, amount]) => {
            const card = collection.cards.find((card) => card.name === name);
            if (!card) {
                return;
            }
            return { card, amount };
        })
        .filter(isNotUndefined)
        .filter(({ card }) => !isBasicLand(card))
        .sort((a, b) => b.amount - a.amount);
}
