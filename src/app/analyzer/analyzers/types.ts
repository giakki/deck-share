import { cardTypes, Collection } from '@shared/card-api/cards.model';

import { AnalysisResultsByType, Deck } from '../analyzer.model';

export function analyzeTypes(collection: Collection, decks: Deck[]) {
    const cardsByType: AnalysisResultsByType = {
        land: 0,
        creature: 0,
        artifact: 0,
        enchantment: 0,
        planeswalker: 0,
        instant: 0,
        sorcery: 0,
    };

    for (const deck of decks) {
        for (const { id, amount } of deck.cards) {
            const card = collection.cards.find((card) => card.id === id);
            if (!card) {
                continue;
            }
            for (const type of cardTypes(card)) {
                cardsByType[type] += amount;
            }
        }
    }

    for (const type in cardsByType) {
        if (Object.hasOwnProperty.call(cardsByType, type)) {
            cardsByType[type as keyof typeof cardsByType] /= decks.length;
        }
    }

    return cardsByType;
}
