import { Collection } from '@shared/card-api/cards.model';
import { IRandom, mkRandom, RandomSeed } from '@utils/random';
import { sum } from 'ramda';

import { AnalysisSettingsByCluster, Deck } from '../analyzer.model';

export function analyzeKmeans(collection: Collection, decks: Deck[], settings: AnalysisSettingsByCluster) {
    // Matrix where rows are decks, columns are cards, and the values are whether a deck includes a certain card.
    const deckCardAssociations = decks.map((deck) =>
        collection.cards.map((card) => (deck.cards.find((c) => c.id === card.id) ? 1 : 0)),
    );

    return kmeans(deckCardAssociations, settings.numClusters, settings.numIterations, mkRandom(settings.randomSeed));
}

function kmeans(data: number[][], numClusters: number, iterations: number, random: IRandom) {
    const centroids = pickCentroids(data, numClusters, random);
    const centroidIndexByDataIndex: number[] = new Array(data.length);

    let converges = false;
    let iter = 0;
    while (!converges && iter < iterations) {
        const numValuesByCentroid: number[] = new Array(numClusters).fill(0);

        // For each value, find the nearest centroid.
        for (let di = 0; di < data.length; di++) {
            let minDistance = Infinity,
                minDistanceIndex = 0;
            for (let ci = 0; ci < centroids.length; ci++) {
                const distance = euclideanDistance(data[di], centroids[ci]);

                if (distance <= minDistance) {
                    minDistance = distance;
                    minDistanceIndex = ci;
                }
            }
            centroidIndexByDataIndex[di] = minDistanceIndex;
            numValuesByCentroid[minDistanceIndex]++;
        }

        // Recalculate centroids.
        const sumByCentroid: number[][] = [];
        const oldCentroids: number[][] = [];

        for (let ci = 0; ci < centroids.length; ci++) {
            sumByCentroid[ci] = new Array(data[0].length).fill(0);
            oldCentroids[ci] = centroids[ci];
        }

        for (let ci = 0; ci < centroids.length; ci++) {
            centroids[ci] = [];
        }

        // Sum values and count for each centroid.
        for (let di = 0; di < data.length; di++) {
            const ci = centroidIndexByDataIndex[di];
            const centroidSum = sumByCentroid[ci];
            const datum = data[di];

            for (let vi = 0; vi < data[0].length; vi++) {
                centroidSum[vi] += datum[vi];
            }
        }

        // Calculate the average for each centroid
        converges = true;
        for (let ci = 0; ci < centroids.length; ci++) {
            const centroid = centroids[ci];
            const centroidSum = sumByCentroid[ci];
            const oldCentroid = oldCentroids[ci];
            const numValuesInCentroid = numValuesByCentroid[ci];

            for (let vi = 0; vi < data[0].length; vi++) {
                centroid[vi] = centroidSum[vi] / numValuesInCentroid || 0;
            }

            if (converges) {
                for (let vi = 0; vi < data[0].length; vi++) {
                    if (oldCentroid[vi] != centroid[vi]) {
                        converges = false;
                        break;
                    }
                }
            }
        }
        iter += 1;
    }

    const result = Object.values(
        centroidIndexByDataIndex.reduce(
            (acc, ci, i) => {
                if (!acc[ci]) {
                    acc[ci] = [i];
                } else {
                    acc[ci].push(i);
                }
                return acc;
            },
            {} as Record<number, number[]>,
        ),
    );

    return [result, random.serialize()] satisfies [number[][], RandomSeed];
}

function euclideanDistance(a: number[], b: number[]) {
    return a.reduce((sum, va, i) => {
        const d = va - b[i];
        return sum + d * d;
    }, 0);
}

function pickCentroids(data: number[][], numCentroids: number, random: IRandom) {
    const initialCentroid = data[random.int(data.length)];
    const centroids = [initialCentroid];

    // Retrieve next centroids
    while (centroids.length < numCentroids) {
        // Min Distances between current centroids and data points.
        const distancesByDataIndex = data.map((datum) => {
            return centroids.reduce((min, centroid) => {
                const dist = euclideanDistance(datum, centroid);
                if (dist <= min) {
                    return dist;
                }

                return min;
            }, Infinity);
        });

        // Sum all min distances.
        const sumOfDistances = sum(distancesByDataIndex);

        // Probabilities and cummulative prob.
        const cumulativeProbabilities = data
            .map((datum, index) => ({
                datum,
                probability: distancesByDataIndex[index] / sumOfDistances,
                cumulativeProbability: 0,
            }))
            .sort((a, b) => a.probability - b.probability);

        // Recalculate cumulative probabilities.
        cumulativeProbabilities[0].cumulativeProbability = cumulativeProbabilities[0].probability;
        for (let ci = 1; ci < cumulativeProbabilities.length; ci++) {
            cumulativeProbabilities[ci].cumulativeProbability =
                cumulativeProbabilities[ci - 1].cumulativeProbability + cumulativeProbabilities[ci].probability;
        }

        // Find next centroid by weighted probability.
        const rnd = random.double();
        let idx = 0;
        while (idx < data.length - 1 && cumulativeProbabilities[idx++].cumulativeProbability < rnd);
        centroids.push(cumulativeProbabilities[idx - 1].datum);
    }

    return centroids;
}
