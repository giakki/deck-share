import { importProvidersFrom } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { Routes } from '@angular/router';
import { provideState } from '@ngrx/store';
import { provideCardsEffects } from '@shared/card-api/cards.effects';

import { provideAnalyzerEffects } from './analyzer.effects';
import * as fromAnalyzer from './analyzer.reducer';
import { AnalyzerPageComponent } from './analyzer-page/analyzer-page.component';

export const routes: Routes = [
    {
        path: '',
        providers: [
            provideState(fromAnalyzer.analyzerFeatureKey, fromAnalyzer.reducer),
            provideAnalyzerEffects(),
            provideCardsEffects(),
            importProvidersFrom(MatDialogModule),
        ],
        children: [
            {
                path: '',
                component: AnalyzerPageComponent,
            },
        ],
    },
];
