import { inject, Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Actions, createEffect, ofType, provideEffects } from '@ngrx/effects';
import { concatLatestFrom } from '@ngrx/operators';
import { select, Store } from '@ngrx/store';
import {
    catchError,
    concatAll,
    concatMap,
    endWith,
    exhaustMap,
    from,
    map,
    Observable,
    of,
    startWith,
    switchMap,
} from 'rxjs';

import { analyzerActions } from './analyzer.actions';
import { AnalyzerFirestore } from './analyzer.firestore';
import { AnalysisSettings, Deck, fromRawAnalysisResults } from './analyzer.model';
import { selectAnalyzerDecks, selectAnalyzerSettings } from './analyzer.selectors';
import { AnalyzerService } from './analyzer.service';
import { AddDeckModalComponent, AddDeckModalInput, AddDeckModalOuput } from './deck-modal/deck-modal.component';

export const deckEditRequest$ = createEffect(
    (actions$ = inject(Actions), dialog = inject(MatDialog), store = inject(Store)) => {
        return actions$.pipe(
            ofType(analyzerActions.deckEditRequest),
            concatLatestFrom(() => store.pipe(select(selectAnalyzerDecks))),
            exhaustMap(([{ id }, decks]) => openDeckModal(dialog, decks, id)),
        );
    },
    { functional: true },
);

export const decksChanges$ = createEffect(
    (firestore = inject(AnalyzerFirestore)) => {
        return firestore.decksActions$;
    },
    { functional: true },
);

export const deckUpsert$ = createEffect(
    (actions$ = inject(Actions), firestore = inject(AnalyzerFirestore)) => {
        return actions$.pipe(
            ofType(analyzerActions.deckUpsert),
            concatMap(({ deck }) => firestore.upsertDeck(deck)),
            catchError((_) => of()),
        );
    },
    { dispatch: false, functional: true },
);

export const loadExampleData$ = createEffect(
    (actions$ = inject(Actions), analyzer = inject(AnalyzerService), exampleData = inject(ExampleDataService)) => {
        return actions$.pipe(
            ofType(analyzerActions.exampleDataLoadRequest),
            exhaustMap(() =>
                loadExampleData(analyzer, exampleData).pipe(
                    catchError(() => of(analyzerActions.exampleDataLoadingAddError())),
                    startWith(analyzerActions.exampleDataLoadingSetProgress({ progress: 0 })),
                    endWith(analyzerActions.exampleDataLoadComplete()),
                ),
            ),
        );
    },
    { functional: true },
);

export const runAnalysis$ = createEffect(
    (actions$ = inject(Actions), analyzer = inject(AnalyzerService), store = inject(Store)) => {
        return actions$.pipe(
            ofType(analyzerActions.runAnalysis),
            concatLatestFrom(() => [
                store.pipe(select(selectAnalyzerDecks)),
                store.pipe(select(selectAnalyzerSettings)),
            ]),
            exhaustMap(([_, decks, settings]) => runAnalysis(analyzer, decks, settings)),
        );
    },
    { functional: true },
);

export const provideAnalyzerEffects = () =>
    provideEffects({
        deckEditRequest$,
        decksChanges$,
        deckUpsert$,
        loadExampleData$,
        runAnalysis$,
    });

function loadExampleData(analyzer: AnalyzerService, exampleData: ExampleDataService) {
    return exampleData.get().pipe(
        switchMap((decks) =>
            decks.map(({ name, decklist }, i) =>
                analyzer.getCardsFromDecklist(decklist).pipe(
                    map((cards) =>
                        analyzerActions.deckUpsert({ deck: { cards, decklist, id: crypto.randomUUID(), name } }),
                    ),
                    catchError(() => of(analyzerActions.deckEditError({ message: 'Unable to load deck' }))),
                    endWith(
                        analyzerActions.exampleDataLoadingSetProgress({
                            progress: Math.floor(((i + 1) / decks.length) * 100),
                        }),
                    ),
                ),
            ),
        ),
        concatAll(),
    );
}

function openDeckModal(dialog: MatDialog, decks: Deck[], id?: string) {
    let originalDeck: Deck | undefined;

    if (id) {
        originalDeck = decks.find((deck) => deck.id === id);
        if (!originalDeck) {
            return of(analyzerActions.deckEditError({ message: 'Deck not found' }));
        }
    }

    return dialog
        .open<AddDeckModalComponent, AddDeckModalInput, AddDeckModalOuput>(AddDeckModalComponent, {
            data: { deck: originalDeck },
        })
        .afterClosed()
        .pipe(
            map((deck) => {
                if (!deck) {
                    throw new Error('Cancelled');
                }
                return analyzerActions.deckUpsert({
                    deck: {
                        ...deck,
                        name: deck.name?.trim() ?? '',
                    },
                });
            }),
            catchError(() => of(analyzerActions.deckEditCancelled())),
        );
}

function runAnalysis(analyzer: AnalyzerService, decks: Deck[], settings: AnalysisSettings) {
    return from(analyzer.run(decks, settings)).pipe(
        switchMap(({ results, newSettings }) => [
            analyzerActions.runAnalysisSuccess({ results: fromRawAnalysisResults(results) }),
            analyzerActions.patchSettings({ settings: newSettings }),
        ]),
        catchError((err) => of(analyzerActions.runAnalysisError({ message: err.message }))),
    );
}

/**
 * To simplify testing
 */
@Injectable({
    providedIn: 'root',
})
export class ExampleDataService {
    get(): Observable<{ name: string; decklist: string }[]> {
        return from(import('../../tests/mocks/decks.mock.json')).pipe(map((module) => module.default));
    }
}
