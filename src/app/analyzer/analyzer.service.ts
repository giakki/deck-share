import { Injectable } from '@angular/core';
import { ParsedDecklistItem, parseDeck } from '@utils/deck-parser';
import { clone } from 'ramda';
import uniqBy from 'ramda/es/uniqBy';
import { map, Observable } from 'rxjs';

import { Card, IncompleteCollectionError } from '../shared/card-api/cards.model';
import { CardsService } from '../shared/card-api/cards.service';
import { AnalysisSettings, Deck, DeckEntry } from './analyzer.model';
import { analyzeColors } from './analyzers/colors';
import { analyzeFrequency } from './analyzers/frequency';
import { analyzeKmeans } from './analyzers/k-means';
import { analyzeTypes } from './analyzers/types';

@Injectable({
    providedIn: 'root',
})
export class AnalyzerService {
    constructor(
        private api: CardsService,
        private cardsService: CardsService,
    ) {}

    getCardsFromDecklist(value: string): Observable<DeckEntry[]> {
        return this.loadCollection(value).pipe(
            map(({ collection, cards }) => {
                if (collection.notFound && collection.notFound.length > 0) {
                    throw new IncompleteCollectionError(collection.notFound);
                }

                return this.loadCards(cards, collection.cards);
            }),
            map((cards) =>
                cards.map(([amount, card]) => ({
                    amount: amount,
                    id: card.id,
                    name: card.name,
                })),
            ),
        );
    }

    run(decks: Deck[], settings: AnalysisSettings) {
        const uniqueCards = uniqBy(
            (card) => card.id,
            decks.flatMap((d) => d.cards),
        );

        return this.cardsService.collection(uniqueCards.map((card) => ({ name: card.name }))).pipe(
            map((collection) => {
                const [byCluster, randomSeed] = analyzeKmeans(collection, decks, clone(settings.byCluster));
                return {
                    results: {
                        byCluster,
                        byColor: analyzeColors(collection, decks),
                        byFrequency: analyzeFrequency(collection, decks),
                        byType: analyzeTypes(collection, decks),
                        notFound: collection.notFound.map((c) => c.name),
                    },
                    newSettings: { ...settings, byCluster: { ...settings.byCluster, randomSeed } },
                };
            }),
        );
    }

    private loadCards(items: ParsedDecklistItem[], collection: Card[]) {
        return items.map((item) => {
            const returned = collection.find(
                (c) => c.name === item.name || c.card_faces?.some((face) => face.name === item.name),
            );
            if (!returned) {
                throw new Error(`Mismatching deck entry: ${item.name}`);
            }

            return [item.amount, returned] as const;
        });
    }

    private loadCollection(value: string) {
        const cards = parseDeck(value);

        const ids = cards.map((card) => ({ name: card.name }));

        return this.api.collection(ids).pipe(
            map((collection) => ({
                cards,
                collection,
            })),
        );
    }
}
