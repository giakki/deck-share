import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogConfig, MatDialogModule } from '@angular/material/dialog';
import { mkRawAnalysisResults } from '@mocks/analysis';
import { mkDeck, mkDeckEntry } from '@mocks/deck.mock';
import { provideMockFirebaseApp, provideMockFirebaseAuth, provideMockFirestore } from '@mocks/firebase';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { testDiForCoverage } from '@tests/util';
import { times } from 'ramda';
import { delay, firstValueFrom, Observable, of, throwError } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { analyzerActions } from './analyzer.actions';
import {
    deckEditRequest$,
    decksChanges$,
    deckUpsert$,
    ExampleDataService,
    loadExampleData$,
    provideAnalyzerEffects,
    runAnalysis$,
} from './analyzer.effects';
import { AnalyzerFirestore } from './analyzer.firestore';
import { AnalysisSettings, Deck, fromRawAnalysisResults } from './analyzer.model';
import { selectAnalyzerDecks, selectAnalyzerSettings } from './analyzer.selectors';
import { AnalyzerService } from './analyzer.service';
import { AddDeckModalComponent } from './deck-modal/deck-modal.component';

const DEFAULT_ANALYSIS_SETTINGS: AnalysisSettings = {
    byCluster: { numClusters: 0, numIterations: 0, randomSeed: [0, 0, 0, 0] },
};

describe('AnalyzerEffects', () => {
    let actions$: Observable<any>;
    let analyzer: AnalyzerService;
    let dialog: MatDialog;
    let exampleData: ExampleDataService;
    let firestore: AnalyzerFirestore;
    let testScheduler: TestScheduler;
    let mockDeckId: string;
    let mockDeck: Deck;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule],
            providers: [
                provideMockFirebaseApp(),
                provideMockFirebaseAuth(),
                provideMockFirestore(),
                provideMockStore({
                    selectors: [{ selector: selectAnalyzerSettings, value: DEFAULT_ANALYSIS_SETTINGS }],
                }),
                provideMockActions(() => actions$),
                provideHttpClient(withInterceptorsFromDi()),
                provideHttpClientTesting(),
            ],
        });

        analyzer = TestBed.inject(AnalyzerService);
        dialog = TestBed.inject(MatDialog);
        exampleData = TestBed.inject(ExampleDataService);
        firestore = TestBed.inject(AnalyzerFirestore);
        store = TestBed.inject(MockStore);
        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });

        mockDeckId = '__DECK_ID__';
        mockDeck = mkDeck({ id: mockDeckId });

        TestBed.inject(MockStore).overrideSelector(selectAnalyzerDecks, [mockDeck]);
    });

    afterEach(() => {
        TestBed.inject(MockStore).resetSelectors();
    });

    describe('provideAnalyzerEffects', () => {
        it('should not throw', () => {
            expect(() => provideAnalyzerEffects()).not.toThrow();
        });
    });

    describe('decksChanges$', () => {
        it('should auto inject services', () => {
            testDiForCoverage(decksChanges$, firestore);
        });
    });

    describe('deckEditRequest$', () => {
        let modalRequest: MatDialogConfig<any> | undefined;
        let modalResponse: any;
        let modalSpy: jasmine.Spy;

        beforeEach(() => {
            modalResponse = {
                componentInstance: {},
                result: Promise.resolve(),
            };
            modalRequest = undefined;
            modalSpy = spyOn(TestBed.inject(MatDialog), 'open').and.callFake((_, config) => {
                modalRequest = config;
                return { afterClosed: () => of(modalResponse) } as any;
            });
        });

        it('should auto inject services', () => {
            testDiForCoverage(deckEditRequest$, actions$, dialog, store);
        });

        it('should open a modal with the correct component', () => {
            testScheduler.run(({ hot, flush }) => {
                actions$ = hot('-a', { a: analyzerActions.deckEditRequest({ id: mockDeckId }) });

                TestBed.runInInjectionContext(() => deckEditRequest$().subscribe());
                flush();

                expect(modalSpy).toHaveBeenCalledOnceWith(AddDeckModalComponent, { data: { deck: mockDeck } });
            });
        });

        it('should assign the correct deck to the modal component', () => {
            testScheduler.run(({ hot, flush }) => {
                actions$ = hot('-a', { a: analyzerActions.deckEditRequest({ id: mockDeckId }) });

                TestBed.runInInjectionContext(() => deckEditRequest$().subscribe());
                flush();

                expect(modalRequest?.data?.deck).toEqual(mockDeck);
            });
        });

        it('should return an error action if the deck is not found in the store', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                actions$ = hot('-a', { a: analyzerActions.deckEditRequest({ id: '__MISSING__' }) });

                const result = TestBed.runInInjectionContext(() => deckEditRequest$());

                expectObservable(result).toBe('-a', {
                    a: analyzerActions.deckEditError({ message: 'Deck not found' }),
                });
            });
        });

        it('should dispatch an add deck cancelled action if the modal returns no value', (done) => {
            actions$ = of(analyzerActions.deckEditRequest({ id: mockDeckId }));
            modalResponse = undefined;

            TestBed.runInInjectionContext(() => deckEditRequest$()).subscribe((action) => {
                expect(action).toEqual(analyzerActions.deckEditCancelled());
                done();
            });
        });

        it('should dispatch an add deck success action if the modal returns a value', (done) => {
            actions$ = of(analyzerActions.deckEditRequest({ id: mockDeckId }));
            modalResponse = mockDeck;

            TestBed.runInInjectionContext(() => deckEditRequest$()).subscribe((action) => {
                expect(action).toEqual(analyzerActions.deckUpsert({ deck: mockDeck }));
                done();
            });
        });
    });

    describe('deckUpsert$', () => {
        it('should auto inject services', () => {
            testDiForCoverage(deckUpsert$, actions$, firestore);
        });

        it('should upsert the deck into the firestore', () => {
            testScheduler.run(({ hot, flush }) => {
                actions$ = hot('-a', { a: analyzerActions.deckUpsert({ deck: mockDeck }) });
                const spy = spyOn(TestBed.inject(AnalyzerFirestore), 'upsertDeck').and.returnValue(of());

                TestBed.runInInjectionContext(() => deckUpsert$().subscribe());
                flush();

                expect(spy).toHaveBeenCalledOnceWith(mockDeck);
            });
        });

        it('should catch errors from the firestore', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                actions$ = hot('-a', { a: analyzerActions.deckUpsert({ deck: mockDeck }) });
                spyOn(TestBed.inject(AnalyzerFirestore), 'upsertDeck').and.returnValue(throwError(() => new Error()));

                TestBed.runInInjectionContext(() => deckUpsert$().subscribe());

                expectObservable(TestBed.runInInjectionContext(() => runAnalysis$())).toBe('-');
            });
        });
    });

    describe('loadExampleData', () => {
        it('should auto inject services', () => {
            testDiForCoverage(loadExampleData$, actions$, analyzer, exampleData);
        });

        it('should produce the expected actions', async () => {
            const numDecks = 3;
            const exampleData = (await import('../../tests/mocks/decks.mock.json')).default.slice(0, numDecks);
            const deckEntries = times(() => times(() => mkDeckEntry(), 5), numDecks);
            let mockEntryIndex = 0;

            testScheduler.run(({ hot, expectObservable }) => {
                spyOn(TestBed.inject(ExampleDataService), 'get').and.returnValue(of(exampleData).pipe(delay(1)));
                spyOn(TestBed.inject(AnalyzerService), 'getCardsFromDecklist').and.callFake(() =>
                    of(deckEntries[mockEntryIndex++]).pipe(delay(mockEntryIndex * 10)),
                );
                actions$ = hot('-a', { a: analyzerActions.exampleDataLoadRequest() });

                const result = TestBed.runInInjectionContext(() => loadExampleData$());

                expectObservable(result).toBe('1ms a 10ms (bc) 16ms (de) 26ms (fgh)', {
                    a: analyzerActions.exampleDataLoadingSetProgress({ progress: 0 }),
                    b: analyzerActions.deckUpsert({
                        deck: {
                            cards: deckEntries[0],
                            decklist: exampleData[0].decklist,
                            id: jasmine.any(String) as any,
                            name: exampleData[0].name,
                        },
                    }),
                    c: analyzerActions.exampleDataLoadingSetProgress({ progress: 33 }),
                    d: analyzerActions.deckUpsert({
                        deck: {
                            cards: deckEntries[1],
                            decklist: exampleData[1].decklist,
                            id: jasmine.any(String) as any,
                            name: exampleData[1].name,
                        },
                    }),
                    e: analyzerActions.exampleDataLoadingSetProgress({ progress: 66 }),
                    f: analyzerActions.deckUpsert({
                        deck: {
                            cards: deckEntries[2],
                            decklist: exampleData[2].decklist,
                            id: jasmine.any(String) as any,
                            name: exampleData[2].name,
                        },
                    }),
                    g: analyzerActions.exampleDataLoadingSetProgress({ progress: 100 }),
                    h: analyzerActions.exampleDataLoadComplete(),
                });
            });
        });

        it('should handle deck loading errors and continue', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                spyOn(TestBed.inject(ExampleDataService), 'get').and.returnValue(throwError(() => new Error()));
                actions$ = hot('-a', { a: analyzerActions.exampleDataLoadRequest() });

                const result = TestBed.runInInjectionContext(() => loadExampleData$());

                expectObservable(result).toBe('-(abc)', {
                    a: analyzerActions.exampleDataLoadingSetProgress({ progress: 0 }),
                    b: analyzerActions.exampleDataLoadingAddError(),
                    c: analyzerActions.exampleDataLoadComplete(),
                });
            });
        });

        it('should handle deck creation errors and continue', async () => {
            const numDecks = 3;
            const exampleData = (await import('../../tests/mocks/decks.mock.json')).default.slice(0, numDecks);
            const deckEntries = times(() => times(() => mkDeckEntry(), 5), numDecks);
            let mockEntryIndex = 0;

            testScheduler.run(({ hot, expectObservable }) => {
                spyOn(TestBed.inject(ExampleDataService), 'get').and.returnValue(of(exampleData));
                spyOn(TestBed.inject(AnalyzerService), 'getCardsFromDecklist').and.callFake(() =>
                    mockEntryIndex++ === 1 ? throwError(() => new Error()) : of(deckEntries[mockEntryIndex - 1]),
                );
                actions$ = hot('-a', { a: analyzerActions.exampleDataLoadRequest() });

                const result = TestBed.runInInjectionContext(() => loadExampleData$());

                expectObservable(result).toBe('-(abcdefgh)', {
                    a: analyzerActions.exampleDataLoadingSetProgress({ progress: 0 }),
                    b: analyzerActions.deckUpsert({
                        deck: {
                            cards: deckEntries[0],
                            decklist: exampleData[0].decklist,
                            id: jasmine.any(String) as any,
                            name: exampleData[0].name,
                        },
                    }),
                    c: analyzerActions.exampleDataLoadingSetProgress({ progress: 33 }),
                    d: analyzerActions.deckEditError({ message: 'Unable to load deck' }),
                    e: analyzerActions.exampleDataLoadingSetProgress({ progress: 66 }),
                    f: analyzerActions.deckUpsert({
                        deck: {
                            cards: deckEntries[2],
                            decklist: exampleData[2].decklist,
                            id: jasmine.any(String) as any,
                            name: exampleData[2].name,
                        },
                    }),
                    g: analyzerActions.exampleDataLoadingSetProgress({ progress: 100 }),
                    h: analyzerActions.exampleDataLoadComplete(),
                });
            });
        });
    });

    describe('runAnalysis$', () => {
        it('should auto inject services', () => {
            testDiForCoverage(runAnalysis$, actions$, analyzer, store);
        });

        it('should call the analyzer with the decks from the store', () => {
            testScheduler.run(({ hot, flush }) => {
                actions$ = hot('-a', { a: analyzerActions.runAnalysis() });
                const spy = spyOn(TestBed.inject(AnalyzerService), 'run').and.returnValue(
                    of({ results: mkRawAnalysisResults(), newSettings: DEFAULT_ANALYSIS_SETTINGS }),
                );

                TestBed.runInInjectionContext(() => runAnalysis$().subscribe());
                flush();

                expect(spy).toHaveBeenCalledOnceWith([mockDeck], DEFAULT_ANALYSIS_SETTINGS);
            });
        });

        it('should return an analysis success action if the analysis was successful', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const expected = mkRawAnalysisResults();
                actions$ = hot('-a-', { a: analyzerActions.runAnalysis() });
                spyOn(TestBed.inject(AnalyzerService), 'run').and.returnValue(
                    of({ results: expected, newSettings: DEFAULT_ANALYSIS_SETTINGS }),
                );

                const result = TestBed.runInInjectionContext(() => runAnalysis$());

                expectObservable(result).toBe('-(ab)-', {
                    a: analyzerActions.runAnalysisSuccess({ results: fromRawAnalysisResults(expected) }),
                    b: analyzerActions.patchSettings({ settings: DEFAULT_ANALYSIS_SETTINGS }),
                });
            });
        });

        it('should return an analysis error action if the analysis threw', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const expected = '__ERR__';
                actions$ = hot('-a-', { a: analyzerActions.runAnalysis() });
                spyOn(TestBed.inject(AnalyzerService), 'run').and.returnValue(throwError(() => new Error(expected)));

                const result = TestBed.runInInjectionContext(() => runAnalysis$());

                expectObservable(result).toBe('-a-', {
                    a: analyzerActions.runAnalysisError({ message: expected }),
                });
            });
        });
    });
});

describe('ExampleDataService', () => {
    it('should return the correct data', async () => {
        const service = TestBed.inject(ExampleDataService);
        const expected = (await import('../../tests/mocks/decks.mock.json')).default;

        const actual = await firstValueFrom(service.get());

        expect(actual).toEqual(expected);
    });
});
