import { Injectable } from '@angular/core';
import type {
    DocumentChange,
    DocumentData,
    FirestoreDataConverter,
    PartialWithFieldValue,
    QueryDocumentSnapshot,
    SetOptions,
    SnapshotOptions,
    WithFieldValue,
} from '@angular/fire/firestore';
import { Firestore } from '@angular/fire/firestore';
import { Action, select, Store } from '@ngrx/store';
import firebase from '@shared/firebase';
import { catchError, concatMap, from, map, NEVER, of, switchMap, take } from 'rxjs';

import { User } from '../auth/auth.reducer';
import { selectAuthUser } from '../auth/auth.selectors';
import { analyzerActions } from './analyzer.actions';
import { Deck } from './analyzer.model';

export class DeckConverter implements FirestoreDataConverter<Deck> {
    toFirestore(modelObject: WithFieldValue<Deck>): DocumentData;
    toFirestore(modelObject: PartialWithFieldValue<Deck>, options: SetOptions): DocumentData;
    toFirestore(modelObject: WithFieldValue<Deck> | PartialWithFieldValue<Deck>, _?: SetOptions): DocumentData {
        return modelObject;
    }
    fromFirestore(snapshot: QueryDocumentSnapshot<DocumentData>, _?: SnapshotOptions): Deck {
        const data = snapshot.data();

        return {
            cards: data['cards'],
            id: data['id'],
            name: data['name'],
            decklist: data['decklist'],
        };
    }
}

@Injectable({
    providedIn: 'root',
})
export class AnalyzerFirestore {
    private decksCollection$ = this.store.pipe(
        select(selectAuthUser),
        map((user) => this.getDecksCollection(user)),
        firebase.performance.traceUntilFirst('decks'),
    );

    decksActions$ = this.decksCollection$.pipe(
        switchMap((collection) => (collection ? from(firebase.firestore.getDocs(collection)) : NEVER)),
        concatMap((docs) => of(...docs.docChanges().map((change) => this.changeToAction(change)))),
        catchError(() => of()),
    );

    constructor(
        private firestore: Firestore,
        private store: Store,
    ) {}

    upsertDeck(deck: Deck) {
        return this.decksCollection$.pipe(
            take(1),
            switchMap((collection) => {
                if (collection) {
                    return from(
                        firebase.firestore.setDoc(firebase.firestore.doc(collection, deck.id), deck, { merge: true }),
                    );
                }
                return of();
            }),
        );
    }

    private changeToAction(change: DocumentChange<Deck>): Action {
        switch (change.type) {
            case 'added':
                return analyzerActions.deckUpsert({ deck: change.doc.data() });
            case 'modified':
                return analyzerActions.deckUpsert({ deck: change.doc.data() });
            case 'removed':
                return analyzerActions.deckRemove({ deck: change.doc.data() });
        }
    }

    private getDecksCollection(user: User | null) {
        if (!user) {
            return null;
        }

        return firebase.firestore
            .collection(this.firestore, `/apps/deck-share/analyzer/${user.uid}/decks`)
            .withConverter(new DeckConverter());
    }
}
