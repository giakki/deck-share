import { mkCard } from '@mocks/card.mock';
import { CARD_COLORS, CARD_TYPES, CardColor, CardType } from '@shared/card-api/cards.model';

import { AnalysisResults, deckEntryFromCard, fromRawAnalysisResults, RawAnalysisResults } from './analyzer.model';

describe('deckEntryFromCard', () => {
    it('should generate a deck entry with the specified arguments', () => {
        const card = mkCard();
        const amount = Math.random();

        const actual = deckEntryFromCard(card, amount);

        expect(actual).toEqual({ id: card.id, name: card.name, amount });
    });
});

describe('fromRawAnalysisResults', () => {
    it('should produce the expected result', () => {
        const raw: RawAnalysisResults = {
            byCluster: [[Math.random()]],
            byColor: {
                black: Math.random(),
                blue: Math.random(),
                green: Math.random(),
                red: Math.random(),
                white: Math.random(),
            },
            byFrequency: [{ amount: Math.random(), card: mkCard() }],
            byType: {
                artifact: Math.random(),
                creature: Math.random(),
                enchantment: Math.random(),
                instant: Math.random(),
                land: Math.random(),
                planeswalker: Math.random(),
                sorcery: Math.random(),
            },
            notFound: [crypto.randomUUID()],
        };

        const expected: AnalysisResults = {
            byCluster: raw.byCluster,
            byColor: CARD_COLORS.reduce(
                (acc, color: CardColor) => ({
                    data: [...acc.data, raw.byColor[color]],
                    labels: [...acc.labels, color],
                }),
                { data: [] as number[], labels: [] as CardColor[] },
            ),
            byFrequency: raw.byFrequency,
            byType: CARD_TYPES.reduce(
                (acc, type: CardType) => ({
                    data: [...acc.data, raw.byType[type]],
                    labels: [...acc.labels, type],
                }),
                { data: [] as number[], labels: [] as CardType[] },
            ),
            notFound: raw.notFound,
        };

        expect(fromRawAnalysisResults(raw)).toEqual(expected);
    });
});
