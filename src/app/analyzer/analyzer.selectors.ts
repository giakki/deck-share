import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromAnalyzer from './analyzer.reducer';

const deckEntitySelectors = fromAnalyzer.deckAdapter.getSelectors();

export const selectAnalyzerState = createFeatureSelector<fromAnalyzer.State>(fromAnalyzer.analyzerFeatureKey);

export const selectAnalyzerAnalysis = createSelector(selectAnalyzerState, (state) => state.analysis);

export const selectAnalyzerAnalysisRunning = createSelector(selectAnalyzerState, (state) => state.analysisRunning);

export const selectAnalyzerDecks = createSelector(selectAnalyzerState, (state) =>
    deckEntitySelectors.selectAll(state.decks),
);

export const selectAnalyzerExampleDataState = createSelector(selectAnalyzerState, (state) => state.exampleData);

export const selectAnalyzerSettings = createSelector(selectAnalyzerState, (state) => state.settings);
