import { mkAnalysisResults } from '@mocks/analysis';
import { mkCard } from '@mocks/card.mock';
import { mkDeck } from '@mocks/deck.mock';
import { RandomSeed } from '@utils/random';
import { reduce } from 'ramda';
import clone from 'ramda/es/clone';

import { analyzerActions } from './analyzer.actions';
import { AnalysisResults, Deck } from './analyzer.model';
import { initialState as originalInitialState, reducer, State } from './analyzer.reducer';

describe('Analyzer Reducer', () => {
    let initialState: State;
    let initialStateForCheck: State;

    beforeEach(() => {
        initialState = clone(originalInitialState);
        initialStateForCheck = clone(originalInitialState);
    });

    afterEach(() => {
        expect(initialState).toEqual(initialStateForCheck);
    });

    describe('an unknown action', () => {
        it('should return the previous state', () => {
            const action = {} as any;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        });
    });

    describe('deckUpsert action', () => {
        it('should add a deck if not already present', () => {
            const deck: Deck = mkDeck();

            const action = analyzerActions.deckUpsert({ deck });

            const result = reducer(initialState, action);

            expect(result).toEqual({
                ...initialState,
                decks: {
                    ...initialState.decks,
                    ids: [...(initialState.decks.ids as string[]), deck.id],
                    entities: { ...initialState.decks.entities, [deck.id]: deck },
                },
            });
        });

        it('should update a deck if it is already present', () => {
            const deck1 = mkDeck();
            const deck2 = mkDeck({ id: deck1.id });

            const action1 = analyzerActions.deckUpsert({ deck: deck1 });
            const action2 = analyzerActions.deckUpsert({ deck: deck2 });

            const state1 = reducer(initialState, action1);
            const state2 = reducer(state1, action2);

            expect(state2).toEqual({
                ...state1,
                decks: {
                    ...state1.decks,
                    entities: { ...state1.decks.entities, [deck2.id]: deck2 },
                },
            });
        });

        it('should set a default name if none is provided', () => {
            const deck = mkDeck({ name: '' });

            const action = analyzerActions.deckUpsert({ deck });

            const result = reducer(initialState, action);

            expect(result.decks.entities[deck.id]?.name).toEqual(`Deck ${initialState.decks.ids.length + 1}`);
        });
    });

    describe('exampleDataLoadComplete action', () => {
        it('should set the state to loaded', () => {
            const action = analyzerActions.exampleDataLoadComplete();

            const result = reducer(initialState, action);

            expect(result.exampleData).toEqual({ state: 'loaded', errors: 0 });
        });

        it('should bring errors from the previous state', () => {
            const result = reduce(reducer, initialState, [
                analyzerActions.exampleDataLoadingAddError(),
                analyzerActions.exampleDataLoadComplete(),
            ]);

            expect(result.exampleData).toEqual({ state: 'loaded', errors: 1 });
        });
    });

    describe('exampleDataLoadingAddError action', () => {
        it('should set the state to loading', () => {
            const action = analyzerActions.exampleDataLoadingAddError();

            const result = reducer(initialState, action);

            expect(result.exampleData).toEqual({ state: 'loading', errors: 1, progress: 0 });
        });

        it('should update a loading state', () => {
            const result = reduce(reducer, initialState, [
                analyzerActions.exampleDataLoadingSetProgress({ progress: 2 }),
                analyzerActions.exampleDataLoadingAddError(),
            ]);

            expect(result.exampleData).toEqual({ state: 'loading', errors: 1, progress: 2 });
        });
    });

    describe('exampleDataLoadingSetProgress action', () => {
        it('should set the state to loading', () => {
            const action = analyzerActions.exampleDataLoadingSetProgress({ progress: 1 });

            const result = reducer(initialState, action);

            expect(result.exampleData).toEqual({ state: 'loading', errors: 0, progress: 1 });
        });

        it('should update a loading state', () => {
            const result = reduce(reducer, initialState, [
                analyzerActions.exampleDataLoadingSetProgress({ progress: 1 }),
                analyzerActions.exampleDataLoadingSetProgress({ progress: 2 }),
            ]);

            expect(result.exampleData).toEqual({ state: 'loading', errors: 0, progress: 2 });
        });
    });

    describe('exampleDataLoadError action', () => {
        it('should set the state to error', () => {
            const action = analyzerActions.exampleDataLoadError();

            const result = reducer(initialState, action);

            expect(result.exampleData).toEqual({ state: 'error' });
        });
    });

    describe('exampleDataLoadRequest action', () => {
        it('should set the state to loading', () => {
            const action = analyzerActions.exampleDataLoadRequest();

            const result = reducer(initialState, action);

            expect(result.exampleData).toEqual({ state: 'loading', errors: 0, progress: 0 });
        });
    });

    describe('patchSettings action', () => {
        it('should update the settings', () => {
            const numClusters = Math.random();
            const numIterations = Math.random();
            const randomSeed: RandomSeed = [Math.random(), Math.random(), Math.random(), Math.random()];
            const action = analyzerActions.patchSettings({
                settings: { byCluster: { numClusters, numIterations, randomSeed } },
            });

            const result = reducer(initialState, action);

            expect(result).toEqual({
                ...initialState,
                settings: {
                    ...initialState.settings,
                    byCluster: {
                        numClusters,
                        numIterations,
                        randomSeed,
                    },
                },
            });
        });

        it('should update only the specified settings', () => {
            const numClusters = Math.random();
            const numIterations = Math.random();
            const randomSeed: RandomSeed = [Math.random(), Math.random(), Math.random(), Math.random()];
            const action1 = analyzerActions.patchSettings({
                settings: { byCluster: { numClusters, numIterations: Math.random(), randomSeed } },
            });
            const action2 = analyzerActions.patchSettings({
                settings: { byCluster: { numIterations } },
            });

            const result1 = reducer(initialState, action1);
            const result2 = reducer(result1, action2);

            expect(result2).toEqual({
                ...result1,
                settings: {
                    ...result1.settings,
                    byCluster: {
                        numClusters,
                        numIterations,
                        randomSeed,
                    },
                },
            });
        });
    });

    describe('runAnalysis action', () => {
        it('should set the analysis state as running', () => {
            const state = reducer(initialState, analyzerActions.runAnalysis());

            expect(state).toEqual({
                ...initialState,
                analysisRunning: true,
            });
        });
    });

    describe('runAnalysisSuccess action', () => {
        it('should produce the expected state', () => {
            const expected: AnalysisResults = mkAnalysisResults({
                byFrequency: [{ card: mkCard(), amount: 1 }],
                notFound: ['y'],
            });

            const state = reducer(
                reducer(initialState, analyzerActions.runAnalysis()),
                analyzerActions.runAnalysisSuccess({ results: expected }),
            );

            expect(state).toEqual({
                ...initialState,
                analysis: expected,
                analysisRunning: false,
            });
        });
    });

    describe('runAnalysisError action', () => {
        it('should set the analysis state as not running', () => {
            const result = reduce(reducer, initialState, [
                analyzerActions.runAnalysis(),
                analyzerActions.runAnalysisError({ message: crypto.randomUUID() }),
            ]);

            expect(result).toEqual({
                ...initialState,
                analysis: undefined,
                analysisRunning: false,
            });
        });
    });
});
