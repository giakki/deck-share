import { Card, CardColor, CardType } from '@shared/card-api/cards.model';
import { PieChartData, toPieChartData } from '@shared/charts';
import { RandomSeed } from '@utils/random';

const COLOR_SORT_MAP: Record<CardColor, number> = {
    white: 0,
    blue: 1,
    black: 2,
    red: 3,
    green: 4,
};

const TYPE_SORT_MAP: Record<CardType, number> = {
    land: 0,
    creature: 1,
    artifact: 2,
    enchantment: 3,
    planeswalker: 4,
    instant: 5,
    sorcery: 6,
};

export interface DeckEntry {
    id: string;
    name: string;
    amount: number;
}

export function deckEntryFromCard(card: Card, amount: number): DeckEntry {
    return {
        amount,
        id: card.id,
        name: card.name,
    };
}

export interface Deck {
    id: string;
    name: string;
    cards: DeckEntry[];
    decklist: string;
}

export interface AnalysisSettingsByCluster {
    numClusters: number;
    numIterations: number;
    randomSeed: RandomSeed;
}

export interface AnalysisSettings {
    byCluster: AnalysisSettingsByCluster;
}

export type AnalysisResultsByColor = Record<CardColor, number>;

export interface AnalysisResultByFrequency { card: Card; amount: number }

export type AnalysisResultsByFrequency = AnalysisResultByFrequency[];

export type AnalysisResultsByType = Record<CardType, number>;

export type AnalysisResultsByCluster = number[][];

export interface RawAnalysisResults {
    byCluster: AnalysisResultsByCluster;
    byColor: AnalysisResultsByColor;
    byFrequency: AnalysisResultsByFrequency;
    byType: AnalysisResultsByType;
    notFound: string[];
}

export interface AnalysisResults {
    byCluster: AnalysisResultsByCluster;
    byColor: PieChartData<CardColor, number>;
    byFrequency: AnalysisResultsByFrequency;
    byType: PieChartData<CardType, number>;
    notFound: string[];
}

export function fromRawAnalysisResults(results: RawAnalysisResults): AnalysisResults {
    return {
        byCluster: results.byCluster,
        byColor: toPieChartData(results.byColor, COLOR_SORT_MAP),
        byFrequency: results.byFrequency,
        byType: toPieChartData(results.byType, TYPE_SORT_MAP),
        notFound: results.notFound,
    };
}
