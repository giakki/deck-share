import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LetDirective, PushPipe } from '@ngrx/component';
import { select, Store } from '@ngrx/store';
import { DeepPartial } from '@utils/types';

import { AnalysisResultsComponent } from '../analysis-results/analysis-results.component';
import { analyzerActions } from '../analyzer.actions';
import { AnalysisSettings } from '../analyzer.model';
import {
    selectAnalyzerAnalysis,
    selectAnalyzerAnalysisRunning,
    selectAnalyzerDecks,
    selectAnalyzerExampleDataState,
    selectAnalyzerSettings,
} from '../analyzer.selectors';
import { DeckListComponent } from '../deck-list/deck-list.component';

@Component({
    templateUrl: './analyzer-page.component.html',
    styleUrl: './analyzer-page.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [
        DeckListComponent,
        LetDirective,
        MatButtonModule,
        MatProgressSpinnerModule,
        AnalysisResultsComponent,
        PushPipe,
    ],
})
export class AnalyzerPageComponent {
    analysisResults$ = this.store.pipe(select(selectAnalyzerAnalysis));

    decks$ = this.store.pipe(select(selectAnalyzerDecks));

    exampleDataState$ = this.store.pipe(select(selectAnalyzerExampleDataState));

    isAnalysisRunning$ = this.store.pipe(select(selectAnalyzerAnalysisRunning));

    settings$ = this.store.pipe(select(selectAnalyzerSettings));

    constructor(private store: Store) {}

    onAddDeck() {
        this.store.dispatch(analyzerActions.deckEditRequest({}));
    }

    onAnalyze() {
        this.store.dispatch(analyzerActions.runAnalysis());
    }

    onEditDeck(id: string) {
        this.store.dispatch(analyzerActions.deckEditRequest({ id }));
    }

    onLoadExampleData() {
        this.store.dispatch(analyzerActions.exampleDataLoadRequest());
    }

    onSettingsChange(settings: DeepPartial<AnalysisSettings>) {
        this.store.dispatch(analyzerActions.patchSettings({ settings }));
    }
}
