import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSliderModule } from '@angular/material/slider';
import { MatTableModule } from '@angular/material/table';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { mkAnalysisResults } from '@mocks/analysis';
import { MemoizedSelector } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { AnalysisByClusterComponent } from '../analysis-by-cluster/analysis-by-cluster.component';
import { AnalysisByFrequencyComponent } from '../analysis-by-frequency/analysis-by-frequency.component';
import { AnalysisChartsComponent } from '../analysis-charts/analysis-charts.component';
import { AnalysisResultsComponent } from '../analysis-results/analysis-results.component';
import { analyzerActions } from '../analyzer.actions';
import { AnalysisResults, Deck } from '../analyzer.model';
import * as fromAnalyzer from '../analyzer.reducer';
import * as AnalyzerSelectors from '../analyzer.selectors';
import { DeckListComponent } from '../deck-list/deck-list.component';
import { AnalyzerPageComponent } from './analyzer-page.component';

describe('AnalyzerPageComponent', () => {
    let component: AnalyzerPageComponent;
    let fixture: ComponentFixture<AnalyzerPageComponent>;
    let store: MockStore;
    let selectMockAnalysis: MemoizedSelector<any, AnalysisResults | undefined>;
    let selectMockDecks: MemoizedSelector<any, Deck[]>;
    let selectMockAnalysisRunning: MemoizedSelector<any, boolean>;

    function getDeckList(): DeckListComponent | undefined {
        return fixture.debugElement.query(By.directive(DeckListComponent))?.componentInstance;
    }

    function getAnalysisResults(): AnalysisResultsComponent | undefined {
        return fixture.debugElement.query(By.directive(AnalysisResultsComponent))?.componentInstance;
    }

    function getAnalyzeButton(): DebugElement {
        return fixture.debugElement.query(By.css('[name="run-analysis-button"]'));
    }

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                MatFormFieldModule,
                MatExpansionModule,
                MatListModule,
                MatProgressSpinnerModule,
                MatSliderModule,
                MatTableModule,
                AnalyzerPageComponent,
                DeckListComponent,
                AnalysisResultsComponent,
                AnalysisByClusterComponent,
                AnalysisByFrequencyComponent,
                AnalysisChartsComponent,
            ],
            providers: [
                provideMockStore({
                    initialState: { [fromAnalyzer.analyzerFeatureKey]: fromAnalyzer.initialState },
                }),
            ],
        }).compileComponents();
        store = TestBed.inject(MockStore);
        selectMockAnalysis = store.overrideSelector(AnalyzerSelectors.selectAnalyzerAnalysis, undefined);
        selectMockDecks = store.overrideSelector(AnalyzerSelectors.selectAnalyzerDecks, []);
        selectMockAnalysisRunning = store.overrideSelector(AnalyzerSelectors.selectAnalyzerAnalysisRunning, false);
    });

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AnalyzerPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('children', () => {
        it('should contain the deck list', () => {
            expect(getDeckList()).toBeTruthy();
        });

        it('should pass the store data to the deck list', () => {
            const expected = [{ cards: [], id: '__ID__', name: '__NAME__', decklist: '__ORIGINAL_TEXT__' }];

            selectMockDecks.setResult(expected);
            store.refreshState();
            fixture.detectChanges();

            expect(getDeckList()?.decks()).toEqual(expected);
        });

        it('should not contain the analysis results, if they are not present', () => {
            expect(getAnalysisResults()).toBeFalsy();
        });

        it('should contain the analysis results, if they are present', () => {
            selectMockAnalysis.setResult(mkAnalysisResults());
            store.refreshState();
            fixture.detectChanges();
            expect(getAnalysisResults()).toBeTruthy();
        });

        it('should pass the store data to the analysis results', () => {
            const expected = mkAnalysisResults();

            selectMockAnalysis.setResult(expected);
            store.refreshState();
            fixture.detectChanges();

            expect(getAnalysisResults()?.results()).toEqual(expected);
        });
    });

    describe('events', () => {
        it('should dispatch the correct action when clicking on the "analyze" button', () => {
            const spy = spyOn(store, 'dispatch');

            getAnalyzeButton()?.nativeElement.click();

            expect(spy).toHaveBeenCalledOnceWith(analyzerActions.runAnalysis());
        });

        it('should forward the "addDeck" event to the store', () => {
            const spy = spyOn(store, 'dispatch');
            const cmp = getDeckList();

            cmp?.addDeck.emit();

            expect(spy).toHaveBeenCalledOnceWith(analyzerActions.deckEditRequest({}));
        });

        it('should forward the "deckEditRequest" event to the store', () => {
            const spy = spyOn(store, 'dispatch');
            const cmp = getDeckList();

            cmp?.editDeck.emit('__ID__');

            expect(spy).toHaveBeenCalledOnceWith(analyzerActions.deckEditRequest({ id: '__ID__' }));
        });

        it('should forward the "settingsChange" event to the store', () => {
            selectMockAnalysis.setResult(mkAnalysisResults());
            store.refreshState();
            fixture.detectChanges();
            const spy = spyOn(store, 'dispatch');
            const cmp = getAnalysisResults();
            const settings = { byCluster: {} };

            cmp?.settingsChange.emit(settings);

            expect(spy).toHaveBeenCalledOnceWith(analyzerActions.patchSettings({ settings }));
        });

        it('should forward the "addDeck" event to the store', () => {
            const spy = spyOn(store, 'dispatch');
            const cmp = getDeckList();

            cmp?.loadExampleData.emit();

            expect(spy).toHaveBeenCalledOnceWith(analyzerActions.exampleDataLoadRequest());
        });
    });

    describe('analysis running', () => {
        it('should not show a spinner if the analysis is not running', () => {
            const spinner = getAnalyzeButton().query(By.css('mat-spinner'));

            expect(spinner).toBeFalsy();
        });

        it('should show a spinner if the analysis is running', () => {
            selectMockAnalysisRunning.setResult(true);
            store.refreshState();
            fixture.detectChanges();

            const spinner = getAnalyzeButton().query(By.css('mat-spinner'));

            expect(spinner).toBeTruthy();
        });

        it('should not disable the analyze button if the analysis is not running', () => {
            const button = getAnalyzeButton();

            expect(button.nativeElement.hasAttribute('disabled')).toEqual(false);
        });

        it('should not disable the analyze button if the analysis is not running', () => {
            selectMockAnalysisRunning.setResult(true);
            store.refreshState();
            fixture.detectChanges();

            const button = getAnalyzeButton();

            expect(button.nativeElement.hasAttribute('disabled')).toEqual(true);
        });
    });
});
