import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { RandomSeed } from '@utils/random';
import { mergeDeepRight } from 'ramda';

import { analyzerActions } from './analyzer.actions';
import { AnalysisResults, AnalysisSettings, Deck } from './analyzer.model';

export const analyzerFeatureKey = 'analyzer';

export const deckAdapter: EntityAdapter<Deck> = createEntityAdapter<Deck>();

interface ExampleDataNotLoaded {
    state: 'not loaded';
}
export function mkExampleDataNotLoaded(): ExampleDataNotLoaded {
    return { state: 'not loaded' };
}

interface ExampleDataError {
    state: 'error';
}
export function mkExampleDataError(): ExampleDataError {
    return { state: 'error' };
}

interface ExampleDataLoading {
    state: 'loading';
    errors: number;
    progress: number;
}
export function mkExampleDataLoading(progress: number, errors: number): ExampleDataLoading {
    return { state: 'loading', errors, progress };
}

interface ExampleDataLoaded {
    state: 'loaded';
    errors: number;
}
export function mkExampleDataLoaded(errors: number): ExampleDataLoaded {
    return { state: 'loaded', errors };
}

export type ExampleDataState = ExampleDataNotLoaded | ExampleDataError | ExampleDataLoading | ExampleDataLoaded;

function exampleDataGetErrors(state: ExampleDataState) {
    if (!('errors' in state)) {
        return 0;
    }
    return state.errors;
}
function exampleDataGetProgress(state: ExampleDataState) {
    if (!('progress' in state)) {
        return 0;
    }
    return state.progress;
}

export interface State {
    analysis: AnalysisResults | undefined;
    analysisRunning: boolean;
    decks: EntityState<Deck>;
    exampleData: ExampleDataState;
    settings: AnalysisSettings;
}

export const initialState: State = {
    analysis: undefined,
    analysisRunning: false,
    decks: deckAdapter.getInitialState(),
    exampleData: mkExampleDataNotLoaded(),
    settings: {
        byCluster: {
            numClusters: 3,
            numIterations: 100,
            randomSeed: [
                Math.trunc(Math.random() * 2 ** 32),
                Math.trunc(Math.random() * 2 ** 32),
                Math.trunc(Math.random() * 2 ** 32),
                Math.trunc(Math.random() * 2 ** 32),
            ] satisfies RandomSeed,
        },
    },
};

export const reducer = createReducer(
    initialState,
    on(analyzerActions.deckUpsert, (state, { deck }) => {
        if (!deck.name) {
            deck.name = `Deck ${state.decks.ids.length + 1}`;
        }
        return {
            ...state,
            decks: deckAdapter.upsertOne(deck, state.decks),
        };
    }),
    on(analyzerActions.exampleDataLoadComplete, (state) => ({
        ...state,
        exampleData: mkExampleDataLoaded(exampleDataGetErrors(state.exampleData)),
    })),
    on(analyzerActions.exampleDataLoadError, (state) => ({
        ...state,
        exampleData: mkExampleDataError(),
    })),
    on(analyzerActions.exampleDataLoadingAddError, (state) => ({
        ...state,
        exampleData: mkExampleDataLoading(
            exampleDataGetProgress(state.exampleData),
            exampleDataGetErrors(state.exampleData) + 1,
        ),
    })),
    on(analyzerActions.exampleDataLoadingSetProgress, (state, { progress }) => ({
        ...state,
        exampleData: mkExampleDataLoading(progress, exampleDataGetErrors(state.exampleData)),
    })),
    on(analyzerActions.exampleDataLoadRequest, (state) => ({
        ...state,
        exampleData: mkExampleDataLoading(0, 0),
    })),
    on(analyzerActions.patchSettings, (state, { settings }) => ({
        ...state,
        settings: mergeDeepRight(state.settings, settings),
    })),
    on(
        analyzerActions.runAnalysis,
        (state): State => ({
            ...state,
            analysisRunning: true,
        }),
    ),
    on(
        analyzerActions.runAnalysisSuccess,
        (state, { results }): State => ({
            ...state,
            analysis: results,
            analysisRunning: false,
        }),
    ),
    on(
        analyzerActions.runAnalysisError,
        (state): State => ({
            ...state,
            analysis: undefined,
            analysisRunning: false,
        }),
    ),
);
