import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { CardPopupDirective } from '@shared/card-popup/card-popup.directive';

import { AnalysisResultsByFrequency } from '../analyzer.model';

@Component({
    selector: 'az-analysis-by-frequency',
    templateUrl: './analysis-by-frequency.component.html',
    styleUrl: './analysis-by-frequency.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [MatTableModule, CardPopupDirective],
})
export class AnalysisByFrequencyComponent {
    results = input<AnalysisResultsByFrequency>();
}
