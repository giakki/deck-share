import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTableModule } from '@angular/material/table';
import { By } from '@angular/platform-browser';
import { mkAnalysisResultsByFrequency } from '@mocks/analysis';

import { AnalysisByFrequencyComponent } from './analysis-by-frequency.component';

describe('AnalysisByFrequencyComponent', () => {
    let component: AnalysisByFrequencyComponent;
    let fixture: ComponentFixture<AnalysisByFrequencyComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [MatTableModule, AnalysisByFrequencyComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AnalysisByFrequencyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have a row for each entry', () => {
        const expectedNumRows = 5;
        fixture.componentRef.setInput('results', mkAnalysisResultsByFrequency(expectedNumRows));
        fixture.detectChanges();

        const rows = fixture.debugElement.queryAll(By.css('tbody tr'));
        expect(rows.length).toEqual(expectedNumRows);
    });

    it('should show the frequency in the first column', () => {
        fixture.componentRef.setInput('results', mkAnalysisResultsByFrequency(6));
        fixture.detectChanges();

        const tds = fixture.debugElement.queryAll(By.css('tbody td:nth-child(1)'));

        expect(tds).toHaveSize(6);
        for (const [i, td] of tds.entries()) {
            expect(td.nativeElement.innerHTML.trim()).toEqual(component.results()?.[i].amount.toString());
        }
    });

    it('should show the name in the second column', () => {
        fixture.componentRef.setInput('results', mkAnalysisResultsByFrequency(7));
        fixture.detectChanges();

        const tds = fixture.debugElement.queryAll(By.css('tbody td:nth-child(2) u'));

        expect(tds).toHaveSize(7);
        for (const [i, td] of tds.entries()) {
            expect(td.nativeElement.innerHTML.trim()).toEqual(component.results()?.[i].card.name);
        }
    });
});
