import { createReducer, on } from '@ngrx/store';

import { authActions } from './auth.actions';

export const authFeatureKey = 'auth';

export interface User {
    uid: string;
}

export function makeUser(uid: string): User {
    return { uid };
}

export interface State {
    user: User | null;
}

export const initialState: State = {
    user: null,
};

export const reducer = createReducer(
    initialState,
    on(authActions.userSet, (state, { user }): State => ({ ...state, user })),
    on(authActions.userUnset, (state): State => ({ ...state, user: null })),
);
