import * as fromAuth from './auth.reducer';
import { selectAuthState, selectAuthUser } from './auth.selectors';

describe('Auth Selectors', () => {
    it('should select the feature state', () => {
        const result = selectAuthState({
            [fromAuth.authFeatureKey]: fromAuth.initialState,
        });

        expect(result).toEqual(fromAuth.initialState);
    });

    describe('selectAuthUser', () => {
        it('should select the authenticated user if present', () => {
            const expected = fromAuth.makeUser(crypto.randomUUID());
            const result = selectAuthUser({
                [fromAuth.authFeatureKey]: { ...fromAuth.initialState, user: expected },
            });

            expect(result).toEqual(expected);
        });

        it('should select the authenticated user if missing', () => {
            const result = selectAuthUser({
                [fromAuth.authFeatureKey]: fromAuth.initialState,
            });

            expect(result).toEqual(null);
        });
    });
});
