import { inject } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { Actions, createEffect, ofType, provideEffects } from '@ngrx/effects';
import firebase from '@shared/firebase';
import { catchError, EMPTY, exhaustMap, from, map } from 'rxjs';

import { authActions } from './auth.actions';
import { makeUser } from './auth.reducer';

export const authState$ = createEffect(
    (auth = inject(Auth)) => {
        return firebase.auth.authState(auth).pipe(
            firebase.performance.traceUntilFirst('auth'),
            map((user) => (user ? authActions.userSet({ user: makeUser(user.uid) }) : authActions.userUnset())),
        );
    },
    { functional: true },
);

export const login$ = createEffect(
    (actions$ = inject(Actions), auth = inject(Auth)) => {
        return actions$.pipe(
            ofType(authActions.login),
            exhaustMap(() => from(firebase.auth.signInWithPopup(auth, new firebase.auth.GoogleAuthProvider()))),
            catchError((_) => EMPTY),
        );
    },
    { functional: true, dispatch: false },
);

export const logout$ = createEffect(
    (actions$ = inject(Actions), auth = inject(Auth)) => {
        return actions$.pipe(
            ofType(authActions.logout),
            exhaustMap(() => from(firebase.auth.signOut(auth))),
            catchError((_) => EMPTY),
        );
    },
    { functional: true, dispatch: false },
);

export const provideAuthEffects = () => provideEffects({ authState$, login$, logout$ });
