import { authActions } from './auth.actions';
import { initialState, makeUser, reducer } from './auth.reducer';

describe('Auth Reducer', () => {
    describe('an unknown action', () => {
        it('should return the previous state', () => {
            const action = {} as any;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        });
    });

    describe('userSet action', () => {
        it('should set the user', () => {
            const action = authActions.userSet({ user: makeUser(crypto.randomUUID()) });

            const result = reducer(initialState, action);

            expect(result).toEqual({ ...initialState, user: action.user });
        });
    });

    describe('userUnset action', () => {
        it('should unset the user', () => {
            const action1 = authActions.userSet({ user: makeUser(crypto.randomUUID()) });
            const action2 = authActions.userUnset();

            const result = reducer(reducer(initialState, action1), action2);

            expect(result).toEqual({ ...initialState, user: null });
        });
    });
});
