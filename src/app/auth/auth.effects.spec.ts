import { TestBed } from '@angular/core/testing';
import { Auth, type User } from '@angular/fire/auth';
import { provideMockFirebaseApp, provideMockFirebaseAuth } from '@mocks/firebase';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import firebase from '@shared/firebase';
import { testDiForCoverage } from '@tests/util';
import { defer, Observable, of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { authActions } from './auth.actions';
import { authState$, login$, logout$, provideAuthEffects } from './auth.effects';
import { makeUser } from './auth.reducer';

describe('AuthEffects', () => {
    let actions$: Observable<any>;
    let auth: Auth;
    let mockAuthState$: Observable<User | null>;
    let testScheduler: TestScheduler;

    beforeEach(() => {
        spyOn(firebase.auth, 'authState').and.returnValue(defer(() => mockAuthState$));

        TestBed.configureTestingModule({
            providers: [
                provideMockFirebaseApp(),
                provideMockFirebaseAuth(),
                provideMockActions(() => actions$),
                provideMockStore(),
            ],
        });

        auth = TestBed.inject(Auth);
        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    describe('provideAuthEffects', () => {
        it('should not throw', () => {
            expect(provideAuthEffects).not.toThrow();
        });
    });

    describe('authState', () => {
        it('should auto inject services', () => {
            testDiForCoverage(authState$, auth);
        });

        it('should listen to authState changes', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const expected = makeUser(crypto.randomUUID());
                mockAuthState$ = hot('-a---b-a', { a: null, b: expected as any });

                const result = TestBed.runInInjectionContext(() => authState$());

                expectObservable(result).toBe('-a---b-a', {
                    a: authActions.userUnset(),
                    b: authActions.userSet({ user: expected }),
                });
            });
        });
    });

    describe('login$', () => {
        it('should auto inject services', () => {
            testDiForCoverage(login$, actions$, auth);
        });

        it('should login with a popup', () => {
            const spy = spyOn(firebase.auth, 'signInWithPopup').and.resolveTo();
            actions$ = of(authActions.login());

            TestBed.runInInjectionContext(() => login$().subscribe());

            expect(spy).toHaveBeenCalled();
        });

        it('should catch login errors', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                spyOn(firebase.auth, 'signInWithPopup').and.rejectWith(new Error());
                actions$ = hot('-a', { a: authActions.login() });

                const result = TestBed.runInInjectionContext(() => login$());

                expectObservable(result).toBe('');
            });
        });
    });

    describe('logout$', () => {
        it('should auto inject services', () => {
            testDiForCoverage(logout$, actions$, auth);
        });

        it('should logout', () => {
            const spy = spyOn(firebase.auth, 'signOut').and.resolveTo();
            actions$ = of(authActions.logout());

            TestBed.runInInjectionContext(() => logout$().subscribe());

            expect(spy).toHaveBeenCalled();
        });

        it('should catch logout errors', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                spyOn(firebase.auth, 'signOut').and.rejectWith(new Error());
                actions$ = hot('-a', { a: authActions.logout() });

                const result = TestBed.runInInjectionContext(() => logout$());

                expectObservable(result).toBe('');
            });
        });
    });
});
