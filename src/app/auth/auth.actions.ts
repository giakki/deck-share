import { createActionGroup, emptyProps, props } from '@ngrx/store';

import { User } from './auth.reducer';

export const authActions = createActionGroup({
    source: 'Auth',
    events: {
        Login: emptyProps(),
        Logout: emptyProps(),
        'User Set': props<{ user: User }>(),
        'User Unset': emptyProps(),
    },
});
