import { authActions } from './auth.actions';
import { makeUser } from './auth.reducer';

describe('login', () => {
    it('should return an action', () => {
        expect(authActions.login().type).toBe('[Auth] Login');
    });
});

describe('logout', () => {
    it('should return an action', () => {
        expect(authActions.logout().type).toBe('[Auth] Logout');
    });
});

describe('userSet', () => {
    it('should return an action', () => {
        expect(authActions.userSet({ user: makeUser(crypto.randomUUID()) }).type).toBe('[Auth] User Set');
    });

    it('should wrap its props', () => {
        const expected = makeUser(crypto.randomUUID());
        expect(authActions.userSet({ user: expected }).user).toEqual(expected);
    });
});

describe('userUnset', () => {
    it('should return an action', () => {
        expect(authActions.userUnset().type).toBe('[Auth] User Unset');
    });
});
