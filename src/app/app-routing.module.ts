import { inject } from '@angular/core';
import { NavigationExtras, provideRouter, Router, Routes } from '@angular/router';
import { Actions, createEffect, ofType, provideEffects } from '@ngrx/effects';
import { createActionGroup, props } from '@ngrx/store';
import { tap } from 'rxjs';

import { FeatureSelectorComponent } from './feature-selector/feature-selector.component';
import { featureSelectorGuard } from './feature-selector/feature-selector.guard';

export const APP_ROUTES: Routes = [
    {
        path: '',
        canActivate: [featureSelectorGuard],
        component: FeatureSelectorComponent,
    },
    {
        path: 'life-counter',
        loadChildren: () => import('./life-counter/life-counter.routes').then((m) => m.routes),
    },
    {
        path: 'analyzer',
        loadChildren: () => import('./analyzer/analyzer.routes').then((m) => m.routes),
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '',
    },
];

export const provideAppRouter = () => provideRouter(APP_ROUTES);

export const routerActions = createActionGroup({
    source: 'Router',
    events: {
        Navigate: props<{ commands: string[]; extras?: NavigationExtras }>(),
        NavigateByUrl: props<{ url: string; extras?: NavigationExtras }>(),
    },
});

export const navigateEffect = createEffect(
    (actions$ = inject(Actions), router = inject(Router)) => {
        return actions$.pipe(
            ofType(routerActions.navigate),
            tap(({ commands, extras }) => {
                router.navigate(commands, extras);
            }),
        );
    },
    { dispatch: false, functional: true },
);

export const navigateByUrlEffect = createEffect(
    (actions$ = inject(Actions), router = inject(Router)) => {
        return actions$.pipe(
            ofType(routerActions.navigateByUrl),
            tap(({ url, extras }) => {
                router.navigateByUrl(url, extras);
            }),
        );
    },
    { dispatch: false, functional: true },
);

export const provideAppRouterEffects = () =>
    provideEffects({
        navigateEffect,
        navigateByUrlEffect,
    });
