import { Update } from '@ngrx/entity';
import { createActionGroup, emptyProps, props } from '@ngrx/store';

import { Game, Player, PlayerBackground } from './life-counter.model';
import * as fromLifeCounter from './life-counter.reducer';

export const lifeCounterActions = createActionGroup({
    source: 'LifeCounter',
    events: {
        'Add Player': props<{ player: Player }>(),
        'Edit Player Start': props<{ id: string }>(),
        'Edit Player End': props<{ id: string; changes: { name: string; background: PlayerBackground | null } }>(),
        'Update Player': props<{ update: Update<Player> }>(),
        'Delete Player': props<{ id: string }>(),
        'New Game': emptyProps(),
        'Player Current Edit Change': props<{ playerId: string; total: number }>(),
        'Start Game': props<{ game: Game }>(),
        'Toggle Editing Mode': props<{ mode: fromLifeCounter.EditingMode }>(),
        'Toggle Options': emptyProps(),
    },
});
