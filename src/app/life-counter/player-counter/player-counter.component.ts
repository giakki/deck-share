import { NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject, input, OnDestroy, output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { mkPlayer } from '../life-counter.model';
import { EditingMode } from '../life-counter.reducer';

// Starts at 1000, then after a second ~450, ~350 ... ~250
export const editingTimeout = (t: number) => 100_000 / (Math.sqrt(t) + 100 + t / 10);

@Component({
    selector: 'lf-player-counter',
    styleUrl: './player-counter.component.scss',
    templateUrl: './player-counter.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgIf, MatIconModule],
})
export class PlayerCounterComponent implements OnDestroy {
    mode = input<EditingMode | undefined>('life');

    player = input(mkPlayer());

    totalChange = output<number>();

    private editingTimeout?: number;

    private readonly sanitizer = inject(DomSanitizer);

    get backgroundImage() {
        const player = this.player();
        if (player.background?.image) {
            return this.sanitizer.bypassSecurityTrustUrl(player.background.image);
        }
        return;
    }

    get editingModeDescription() {
        switch (this.mode()) {
            case 'poison':
                return 'poison counter';
            default:
                return 'life';
        }
    }

    get total() {
        const player = this.player();
        switch (this.mode()) {
            case 'poison':
                return player.poisonCounters;
            default:
                return player.lifeTotal;
        }
    }

    ngOnDestroy(): void {
        this.stopModifying();
    }

    startModifying(diff: number) {
        this.doModify(Date.now(), diff);
    }

    stopModifying() {
        clearTimeout(this.editingTimeout);
    }

    private doModify(startTime: number, diff: number) {
        this.totalChange.emit(this.total + diff);

        const now = Date.now();
        const elapsed = now - startTime;
        if (elapsed > 10_000) {
            startTime = now;
            diff *= 5;
        }

        this.editingTimeout = window.setTimeout(() => this.doModify(startTime, diff), editingTimeout(elapsed));
    }
}
