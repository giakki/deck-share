import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By, DomSanitizer } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { mkPlayer } from '../life-counter.model';
import { editingTimeout, PlayerCounterComponent } from './player-counter.component';

describe('PlayerCounterComponent', () => {
    let component: PlayerCounterComponent;
    let fixture: ComponentFixture<PlayerCounterComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [BrowserAnimationsModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayerCounterComponent);
        fixture.detectChanges();
        component = fixture.componentInstance;
        fixture.componentRef.setInput(
            'player',
            mkPlayer({
                lifeTotal: Math.random(),
                poisonCounters: Math.random(),
            }),
        );
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('backgroundImage', () => {
        it('should return undefined when no background image', () => {
            expect(component.backgroundImage).toBeUndefined();
        });

        it('should return the background image when present', () => {
            const image = Math.random().toString();
            fixture.componentRef.setInput(
                'player',
                mkPlayer({ ...component.player(), background: { image, name: '' } }),
            );
            expect(component.backgroundImage).toEqual(TestBed.inject(DomSanitizer).bypassSecurityTrustUrl(image));
        });
    });

    describe('display', () => {
        it('should display life total by default', () => {
            const text = fixture.debugElement.query(By.css('.total'));

            expect(text.nativeElement.textContent.trim()).toEqual(component.player().lifeTotal.toString());
        });

        it('should display poison counters when specified', () => {
            fixture.componentRef.setInput('mode', 'poison');
            fixture.detectChanges();

            const text = fixture.debugElement.query(By.css('.total'));

            expect(text.nativeElement.textContent.trim()).toEqual(component.player().poisonCounters.toString());
        });
    });

    describe('editing', () => {
        it('should edit life total by default', () => {
            const spy = spyOn(component.totalChange, 'emit');
            const diff = Math.random();

            component.startModifying(diff);

            expect(spy).toHaveBeenCalledOnceWith(component.player().lifeTotal + diff);
        });

        it('should edit poison counters when specified', () => {
            fixture.componentRef.setInput('mode', 'poison');
            const spy = spyOn(component.totalChange, 'emit');
            const diff = Math.random();
            fixture.detectChanges();

            component.startModifying(diff);

            expect(spy).toHaveBeenCalledOnceWith(component.player().poisonCounters + diff);
        });

        it('should edit at the specified pace', fakeAsync(() => {
            const spy = spyOn(component.totalChange, 'emit');
            const diff = 11;
            component.startModifying(diff);

            let total = 0;
            let mul = 1;
            let elapsed = 0;
            let i = 1;
            do {
                expect(spy.calls.all().length).toEqual(i);
                expect(spy.calls.mostRecent().args).toEqual([component.player().lifeTotal + diff * mul]);
                ++i;
                if (elapsed > 10_000) {
                    mul *= 5;
                    ++i;
                    elapsed = 0;
                }
                const next = editingTimeout(elapsed);
                elapsed += next;
                total += next;
                tick(next);
            } while (total < 30_000);
            component.stopModifying();
        }));
    });
});
