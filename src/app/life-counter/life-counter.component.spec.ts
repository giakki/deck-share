import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import { lifeCounterActions } from './life-counter.actions';
import { LifeCounterComponent } from './life-counter.component';
import * as fromLifeCounter from './life-counter.reducer';

describe('LifeCounterComponent', () => {
    let component: LifeCounterComponent;
    let fixture: ComponentFixture<LifeCounterComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [BrowserAnimationsModule],
            providers: [
                provideMockStore({
                    initialState: { [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.initialState },
                }),
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LifeCounterComponent);
        fixture.detectChanges();
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('events', () => {
        it('should dispatch an action when onCreateNew is invoked', () => {
            const spy = spyOn(TestBed.inject(Store), 'dispatch');

            component.onCreateNew();

            expect(spy).toHaveBeenCalledOnceWith(lifeCounterActions.newGame());
        });

        it('should dispatch an action when onPlayerCounterChange is invoked', () => {
            const expected = [crypto.randomUUID(), Math.random()] as [string, number];
            const spy = spyOn(TestBed.inject(Store), 'dispatch');

            component.onPlayerCounterChange(expected[0], expected[1]);

            expect(spy).toHaveBeenCalledOnceWith(
                lifeCounterActions.playerCurrentEditChange({
                    playerId: expected[0],
                    total: expected[1],
                }),
            );
        });

        it('should dispatch an action when onToggleOptions is invoked', () => {
            const spy = spyOn(TestBed.inject(Store), 'dispatch');

            component.onToggleOptions();

            expect(spy).toHaveBeenCalledOnceWith(lifeCounterActions.toggleOptions());
        });

        it('should dispatch an action when onToggleEditingMode is invoked', () => {
            const spy = spyOn(TestBed.inject(Store), 'dispatch');

            component.onToggleEditingMode('poison');

            expect(spy).toHaveBeenCalledOnceWith(lifeCounterActions.toggleEditingMode({ mode: 'poison' }));
        });
    });
});
