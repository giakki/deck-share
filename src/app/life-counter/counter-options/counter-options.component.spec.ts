import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CounterOptionsComponent } from './counter-options.component';

async function showAndGetOptions(fixture: ComponentFixture<CounterOptionsComponent>) {
    fixture.componentRef.setInput('show', true);
    fixture.detectChanges();

    return fixture.debugElement.queryAll(By.css('.option'));
}

describe('CounterOptionsComponent', () => {
    let component: CounterOptionsComponent;
    let fixture: ComponentFixture<CounterOptionsComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [BrowserAnimationsModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CounterOptionsComponent);
        fixture.detectChanges();
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('options toggle', () => {
        it('should not display the options when not specified', () => {
            const options = fixture.debugElement.queryAll(By.css('.option'));

            expect(options.length).toBe(0);
        });

        it('should display the options when specified', async () => {
            const options = await showAndGetOptions(fixture);

            expect(options.length).toBe(2);
        });

        it('should toggle the options when clicking on the main button', () => {
            const spy = spyOn(component.toggleOptions, 'emit');
            const button = fixture.debugElement.query(By.css('[role="button"]'));

            button.nativeElement.click();

            expect(spy).toHaveBeenCalledOnceWith();
        });
    });

    describe('options', () => {
        it('should emit an event when clicking the "New Game" button', async () => {
            const spy = spyOn(component.createNew, 'emit');
            await showAndGetOptions(fixture);
            const button = fixture.debugElement.query(By.css('.new-game'));

            button.nativeElement.click();

            expect(spy).toHaveBeenCalledOnceWith();
        });

        it('should emit an event when clicking the "Poison" button', async () => {
            const spy = spyOn(component.toggleEditingMode, 'emit');
            await showAndGetOptions(fixture);
            const button = fixture.debugElement.query(By.css('.poison'));

            button.nativeElement.click();

            expect(spy).toHaveBeenCalledOnceWith('poison');
        });
    });
});
