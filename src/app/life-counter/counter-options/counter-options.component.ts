import { animate, style, transition, trigger } from '@angular/animations';
import { NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, input, output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { EditingMode } from '../life-counter.reducer';

@Component({
    selector: 'lf-counter-options',
    styleUrl: './counter-options.component.scss',
    templateUrl: './counter-options.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgIf, MatButtonModule, MatIconModule],
    animations: [
        trigger('showHide', [
            transition(':enter', [
                style({
                    transform: 'translate(-50%, -50%)',
                }),
                animate(150),
            ]),
        ]),
    ],
})
export class CounterOptionsComponent {
    show = input<boolean | undefined>(false);

    createNew = output<void>();

    toggleEditingMode = output<EditingMode>();

    toggleOptions = output<void>();
}
