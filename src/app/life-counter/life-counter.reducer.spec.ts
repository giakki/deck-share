import { Action } from '@ngrx/store';
import { groupBy, map, prop, reduce, times } from 'ramda';

import { lifeCounterActions } from './life-counter.actions';
import { Game, mkGame, mkGameKingdom, mkPlayer, Player, playerAdatapter } from './life-counter.model';
import { initialState, reducer } from './life-counter.reducer';

describe('LifeCounter Reducer', () => {
    describe('an unknown action', () => {
        it('should return the previous state', () => {
            const action = {} as Action;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        });
    });

    describe('addPlayer action', () => {
        it('should produce the expected state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));

            const result = reduce(reducer, initialState, actions).players;

            expect(playerAdatapter.getSelectors().selectAll(result)).toEqual(players);
        });

        it('should assign an unique order to the players', () => {
            const players = times(() => mkPlayer(), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));

            const result = reduce(reducer, initialState, actions).players;

            expect(playerAdatapter.getSelectors().selectAll(result)).toEqual([
                { ...players[0], order: 0 },
                { ...players[1], order: 1 },
                { ...players[2], order: 2 },
            ]);
        });

        it('should not modify other parts of the state', () => {
            const players = times(() => mkPlayer(), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));

            const result = reduce(reducer, initialState, actions);

            expect(result).toEqual({
                ...initialState,
                players: jasmine.any(Object) as any,
            });
        });
    });

    describe('updatePlayer action', () => {
        it('should produce the expected state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));
            const state = reduce(reducer, initialState, actions);
            const expected = { ...players[1], lifeTotal: Math.random() };
            const action = lifeCounterActions.updatePlayer({ update: { changes: expected, id: players[1].id } });

            const result = reducer(state, action);

            expect(playerAdatapter.getSelectors().selectAll(result.players)).toEqual([
                players[0],
                expected,
                players[2],
            ]);
        });

        it('should not modify other parts of the state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));
            const state = reduce(reducer, initialState, actions);
            const action = lifeCounterActions.updatePlayer({
                update: { changes: { ...players[1], lifeTotal: Math.random() }, id: players[1].id },
            });

            const result = reducer(state, action);

            expect(result).toEqual({
                ...state,
                players: jasmine.any(Object),
            });
        });
    });

    describe('newGame action', () => {
        it('should return the initial state', () => {
            const result = reduce(reducer, initialState, [
                lifeCounterActions.startGame({ game: mkGame('edh') }),
                lifeCounterActions.newGame(),
            ]);

            expect(result).toEqual(initialState);
        });

        it('should not reset the players', () => {
            const players = [mkPlayer({ lifeTotal: 40, order: 0 }), mkPlayer({ lifeTotal: 40, order: 1 })];

            const result = reduce(reducer, initialState, [
                lifeCounterActions.addPlayer({ player: players[0] }),
                lifeCounterActions.addPlayer({ player: players[1] }),
                lifeCounterActions.startGame({ game: mkGame('edh') }),
                lifeCounterActions.newGame(),
            ]);

            expect(result).toEqual({
                ...initialState,
                players: playerAdatapter.addMany(players, initialState.players),
            });
        });
    });

    describe('playerCurrentEditChange action', () => {
        it('should produce the expected state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));
            const state = reduce(reducer, initialState, actions);
            const expected = { ...players[1], lifeTotal: Math.random() };
            const action = lifeCounterActions.playerCurrentEditChange({
                total: expected.lifeTotal,
                playerId: expected.id,
            });

            const result = reducer(state, action);

            expect(playerAdatapter.getSelectors().selectAll(result.players)).toEqual([
                players[0],
                expected,
                players[2],
            ]);
        });

        it('should produce the expected state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = [
                ...players.map((player) => lifeCounterActions.addPlayer({ player }) as Action),
                lifeCounterActions.toggleEditingMode({ mode: 'poison' }),
            ];
            const state = reduce(reducer, initialState, actions);
            const expected = { ...players[1], poisonCounters: Math.random() };
            const action = lifeCounterActions.playerCurrentEditChange({
                total: expected.poisonCounters,
                playerId: expected.id,
            });

            const result = reducer(state, action);

            expect(playerAdatapter.getSelectors().selectAll(result.players)).toEqual([
                players[0],
                expected,
                players[2],
            ]);
        });

        it('should not modify other parts of the state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));
            const state = reduce(reducer, initialState, actions);
            const action = lifeCounterActions.playerCurrentEditChange({
                total: Math.random(),
                playerId: players[1].id,
            });

            const result = reducer(state, action);

            expect(result).toEqual({
                ...state,
                players: jasmine.any(Object),
            });
        });
    });

    describe('deletePlayer action', () => {
        it('should produce the expected state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));
            const state = reduce(reducer, initialState, actions);
            const action = lifeCounterActions.deletePlayer({ id: players[1].id });

            const result = reducer(state, action);

            expect(playerAdatapter.getSelectors().selectAll(result.players)).toEqual([players[0], players[2]]);
        });

        it('should not modify other parts of the state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const actions = players.map((player) => lifeCounterActions.addPlayer({ player }));
            const state = reduce(reducer, initialState, actions);
            const action = lifeCounterActions.deletePlayer({ id: players[1].id });

            const result = reducer(state, action);

            expect(result).toEqual({
                ...state,
                players: jasmine.any(Object),
            });
        });
    });

    describe('startGame action', () => {
        it('should produce the expected state', () => {
            const players = times((i) => mkPlayer({ lifeTotal: i == 0 ? 50 : 40, order: i }), 3);
            const game: Game = mkGameKingdom(players[0].id);

            const result = reduce(reducer, initialState, [
                ...players.map((player) => lifeCounterActions.addPlayer({ player })),
                lifeCounterActions.startGame({ game }),
            ]);

            expect(result).toEqual({
                ...initialState,
                game,
                isGameRunning: true,
                players: {
                    entities: map((a) => a[0], groupBy(prop('id'), players) as Record<string, Player[]>),
                    ids: jasmine.arrayContaining(players.map((p) => p.id)),
                },
            });
        });

        it('should not modify other parts of the state', () => {
            const players = times((i) => mkPlayer({ order: i }), 3);
            const game: Game = mkGame('edh');

            const result = reduce(reducer, initialState, [
                ...players.map((player) => lifeCounterActions.addPlayer({ player })),
                lifeCounterActions.startGame({ game }),
            ]);

            expect(result).toEqual({
                ...initialState,
                game,
                isGameRunning: true,
                players: jasmine.any(Object),
            });
        });
    });

    describe('toggleEditingMode action', () => {
        it('should enable the selected mode', () => {
            const expected = { ...initialState, editingMode: 'poison' as const };

            const result = reducer(initialState, lifeCounterActions.toggleEditingMode({ mode: 'poison' }));

            expect(result).toEqual(expected);
        });

        it('should disable the selected mode if it is already enabled', () => {
            let result = reducer(initialState, lifeCounterActions.toggleEditingMode({ mode: 'poison' }));

            result = reducer(result, lifeCounterActions.toggleEditingMode({ mode: 'poison' }));

            expect(result).toEqual(initialState);
        });
    });

    describe('toggleOptions action', () => {
        it('should enable the options', () => {
            const expected = { ...initialState, showOptions: true };

            const result = reducer(initialState, lifeCounterActions.toggleOptions());

            expect(result).toEqual(expected);
        });

        it('should disable the options if they are already enabled', () => {
            let result = reducer(initialState, lifeCounterActions.toggleOptions());

            result = reducer(result, lifeCounterActions.toggleOptions());

            expect(result).toEqual(initialState);
        });
    });
});
