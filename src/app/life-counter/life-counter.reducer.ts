import { EntityState } from '@ngrx/entity';
import { createFeature, createReducer, on } from '@ngrx/store';
import { prop } from 'ramda';

import { lifeCounterActions } from './life-counter.actions';
import { Game, mkPlayer, Player, playerAdatapter, startingLifeTotalForGameType } from './life-counter.model';
import { playerSelectors } from './life-counter.selectors';

export const lifeCounterFeatureKey = 'lifeCounter';

export type EditingMode = 'life' | 'poison';

export interface State {
    editingMode: EditingMode;
    game: Game;
    isGameRunning: boolean;
    players: EntityState<Player>;
    showOptions: boolean;
}

export const initialState: State = {
    editingMode: 'life',
    game: { type: 'other' },
    isGameRunning: false,
    players: playerAdatapter.getInitialState(),
    showOptions: false,
};

export const reducer = createReducer(
    initialState,
    on(lifeCounterActions.addPlayer, (state, { player }) => ({
        ...state,
        players: playerAdatapter.addOne(
            {
                ...player,
                order: Math.max(...playerSelectors.selectAll(state.players).map(prop('order')), -1) + 1,
            },
            state.players,
        ),
    })),
    on(lifeCounterActions.updatePlayer, (state, { update }) => ({
        ...state,
        players: playerAdatapter.updateOne(update, state.players),
    })),
    on(lifeCounterActions.newGame, ({ players }): State => ({ ...initialState, players })),
    on(lifeCounterActions.playerCurrentEditChange, (state, { playerId: id, total }) => ({
        ...state,
        players: playerAdatapter.updateOne(
            {
                changes:
                    state.editingMode === 'life'
                        ? {
                              lifeTotal: Math.max(total, 0),
                          }
                        : {
                              poisonCounters: Math.max(total, 0),
                          },
                id,
            },
            state.players,
        ),
    })),
    on(lifeCounterActions.deletePlayer, (state, { id }) => ({
        ...state,
        players: playerAdatapter.removeOne(id, state.players),
    })),
    on(lifeCounterActions.startGame, (state, { game }) => ({
        ...state,
        isGameRunning: true,
        game,
        players: playerAdatapter.setAll(
            playerSelectors.selectAll(state.players).map(({ id, name, background }, i) =>
                mkPlayer({
                    id,
                    name,
                    background,
                    lifeTotal: startingLifeTotalForGameType(game, id.toString()),
                    order: i,
                }),
            ),
            playerAdatapter.getInitialState(),
        ),
    })),
    on(
        lifeCounterActions.toggleEditingMode,
        (state, { mode }): State => ({
            ...state,
            editingMode: state.editingMode === mode ? 'life' : mode,
        }),
    ),
    on(
        lifeCounterActions.toggleOptions,
        (state): State => ({
            ...state,
            showOptions: !state.showOptions,
        }),
    ),
);

export const lifeCounterFeature = createFeature({
    name: lifeCounterFeatureKey,
    reducer,
});
