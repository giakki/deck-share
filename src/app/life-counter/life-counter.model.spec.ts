import {
    isPlayerKing,
    mkGame,
    mkGameKingdom,
    mkPlayer,
    Player,
    playerOrderComparer,
    startingLifeTotalForGameType,
    tryMkGame,
} from './life-counter.model';

describe('LifeCounter Model', () => {
    describe('mkPlayer', () => {
        it('should create the default player', () => {
            const expected: Player = {
                id: jasmine.any(String) as any,
                lifeTotal: 0,
                poisonCounters: 0,
                name: 'Player',
                order: 0,
                background: null,
            };

            expect(mkPlayer()).toEqual(expected);
        });

        it('should create unique ids', () => {
            const one = mkPlayer();
            const two = mkPlayer();

            expect(one.id).not.toEqual(two.id);
        });

        it('should generate an id if it is not supplied', () => {
            const one = mkPlayer({ lifeTotal: Math.random() });

            expect(one.id).toEqual(jasmine.any(String));
        });

        it('should copy overridable properties', () => {
            const expected: Player = {
                id: crypto.randomUUID(),
                lifeTotal: Math.random(),
                poisonCounters: Math.random(),
                name: crypto.randomUUID(),
                order: Math.random(),
                background: {
                    image: crypto.randomUUID(),
                    name: crypto.randomUUID(),
                },
            };

            const actual = mkPlayer(expected);

            expect(actual).toEqual(expected);
        });
    });

    describe('playerOrderComparer', () => {
        it('should sort the players by their order', () => {
            const players = [mkPlayer({ order: 2 }), mkPlayer({ order: 0 }), mkPlayer({ order: 1 })];

            const result = players.sort(playerOrderComparer);

            for (let i = 0; i < players.length; ++i) {
                expect(result[i].order).toEqual(i);
            }
        });
    });

    describe('isPlayerKing', () => {
        it('should return whether the player is a king', () => {
            const kingId = crypto.randomUUID();
            const game = mkGameKingdom(kingId);

            expect(isPlayerKing(game, kingId)).toBeTrue();
            expect(isPlayerKing(game, '')).toBeFalse();
        });
    });

    describe('mkGame', () => {
        it('should make an EDH game', () => {
            const game = mkGame('edh');

            expect(game).toEqual({ type: 'edh' });
        });

        it('should make other game types', () => {
            const game = mkGame('other');

            expect(game).toEqual({ type: 'other' });
        });
    });

    describe('mkGameKingdom', () => {
        it('should make a kingdom game', () => {
            const kingId = crypto.randomUUID();
            const game = mkGameKingdom(kingId);

            expect(game).toEqual({ type: 'kingdom', king: kingId });
        });
    });

    describe('tryMkGame', () => {
        it('should make an EDH game', () => {
            const game = tryMkGame('edh');

            expect(game).toEqual({ type: 'edh' });
        });

        it('should make other game types', () => {
            const game = tryMkGame('other');

            expect(game).toEqual({ type: 'other' });
        });

        it('should make a kingdom game', () => {
            const kingId = crypto.randomUUID();
            const game = tryMkGame('kingdom', kingId);

            expect(game).toEqual({ type: 'kingdom', king: kingId });
        });

        it('should return null for an invalid game type', () => {
            const game = tryMkGame('invalid' as any);

            expect(game).toBeNull();
        });

        it('should return null for an invalid king id', () => {
            const game = tryMkGame('kingdom', '');

            expect(game).toBeNull();
        });

        it('should return null for an invalid game type and king id', () => {
            const game = tryMkGame('invalid' as any, '');

            expect(game).toBeNull();
        });
    });

    describe('startingLifeTotalForGameType', () => {
        it('should return the correct value for an EDH game', () => {
            const game = mkGame('edh');

            expect(startingLifeTotalForGameType(game, '')).toEqual(40);
        });

        it('should return the correct value for other game types', () => {
            const game = mkGame('other');

            expect(startingLifeTotalForGameType(game, '')).toEqual(20);
        });

        it('should return the correct value for the king of a kingdom game', () => {
            const kingId = crypto.randomUUID();
            const game = mkGameKingdom(kingId);

            expect(startingLifeTotalForGameType(game, kingId)).toEqual(50);
        });

        it('should return the correct value for an EDH game', () => {
            const game = mkGameKingdom(crypto.randomUUID());

            expect(startingLifeTotalForGameType(game, crypto.randomUUID())).toEqual(40);
        });
    });
});
