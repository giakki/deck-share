import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType, provideEffects } from '@ngrx/effects';
import { routerNavigatedAction } from '@ngrx/router-store';
import { select, Store } from '@ngrx/store';
import { combineLatest, filter, map, of, switchMap, withLatestFrom } from 'rxjs';

import { routerActions } from '../app-routing.module';
import { lifeCounterActions } from './life-counter.actions';
import { Player } from './life-counter.model';
import { selectLifeCounterIsGameRunning, selectLifeCounterPlayers } from './life-counter.selectors';

function isLifePageFeatureActive(router: Router) {
    return router.isActive('life-counter', {
        fragment: 'ignored',
        matrixParams: 'ignored',
        paths: 'subset',
        queryParams: 'ignored',
    });
}

function getNextUrl(originalUrl: string, isRunning: boolean, players: Player[]) {
    if (isRunning) {
        return '/life-counter';
    }

    const playerId = /life-counter\/new-game\/player\/(.+)/.exec(originalUrl);
    if (playerId && players.some((p) => p.id === playerId[1])) {
        return originalUrl;
    }

    return '/life-counter/new-game';
}

export const editPlayerStart$ = createEffect(
    (actions$ = inject(Actions), store = inject(Store)) => {
        return actions$.pipe(
            ofType(lifeCounterActions.editPlayerStart),
            withLatestFrom(store.pipe(select(selectLifeCounterPlayers))),
            filter(([{ id }, players]) => players.some((p) => p.id === id)),
            map(([{ id }]) => routerActions.navigate({ commands: ['life-counter', 'new-game', 'player', id] })),
        );
    },
    { functional: true },
);

export const editPlayerEndUpdatePlayer$ = createEffect(
    (actions$ = inject(Actions)) => {
        return actions$.pipe(ofType(lifeCounterActions.editPlayerEnd)).pipe(
            map(({ id, changes }) =>
                lifeCounterActions.updatePlayer({
                    update: {
                        id,
                        changes,
                    },
                }),
            ),
        );
    },
    { functional: true },
);

export const editPlayerEndNavigateToNewGame$ = createEffect(
    (actions$ = inject(Actions)) => {
        return actions$
            .pipe(ofType(lifeCounterActions.editPlayerEnd))
            .pipe(map(() => routerActions.navigate({ commands: ['life-counter', 'new-game'] })));
    },
    { functional: true },
);

export const pageRedirect$ = createEffect(
    (actions$ = inject(Actions), router = inject(Router), store = inject(Store)) => {
        return combineLatest([
            actions$.pipe(ofType(routerNavigatedAction)),
            store.pipe(select(selectLifeCounterIsGameRunning)),
            store.pipe(select(selectLifeCounterPlayers)),
        ]).pipe(
            filter(() => isLifePageFeatureActive(router)),
            switchMap(([action, isRunning, players]) => {
                const nextUrl = getNextUrl(action.payload.event.url, isRunning, players);

                if (action.payload.event.urlAfterRedirects !== nextUrl.toString()) {
                    return of(routerActions.navigateByUrl({ url: nextUrl }));
                }

                return of({ type: 'NOOP' });
            }),
        );
    },
    { functional: true },
);

export const provideLifeCounterEffects = () =>
    provideEffects({
        pageRedirect$,
        editPlayerStart$,
        editPlayerEndNavigateToNewGame$,
        editPlayerEndUpdatePlayer$,
    });
