import { NgIf } from '@angular/common';
import { Component, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ActivatedRoute } from '@angular/router';
import { PushPipe } from '@ngrx/component';
import { select, Store } from '@ngrx/store';
import { CardsService } from '@shared/card-api/cards.service';
import { isNotNullOrUndefined } from '@utils/fn';
import { catchError, debounceTime, distinctUntilChanged, filter, finalize, map, of, switchMap, tap } from 'rxjs';

import { lifeCounterActions } from '../life-counter.actions';
import { selectLifeCounterPlayers } from '../life-counter.selectors';

@Component({
    selector: 'lf-edit-player',
    standalone: true,
    imports: [
        ReactiveFormsModule,
        NgIf,
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        PushPipe,
    ],
    templateUrl: './edit-player.component.html',
    styleUrl: './edit-player.component.scss',
})
export class EditPlayerComponent {
    readonly form = inject(FormBuilder).group({
        name: ['', Validators.required],
        backgroundCardImage: [null as string | null],
        backgroundCardName: [''],
    });

    readonly backgroundCards$ = this.form.valueChanges.pipe(
        takeUntilDestroyed(),
        debounceTime(250),
        map((value) => value?.backgroundCardName),
        distinctUntilChanged(),
        switchMap((name) => {
            if (name) {
                this.isLoadingBackground = true;
                return this.cards.autocomplete(name).pipe(finalize(() => (this.isLoadingBackground = false)));
            }
            if (this.form.value.backgroundCardImage !== null) {
                this.form.patchValue({
                    backgroundCardImage: null,
                });
            }
            return of(undefined);
        }),
        catchError(() => of()),
    );

    readonly player$ = this.store.pipe(
        takeUntilDestroyed(),
        select(selectLifeCounterPlayers),
        map((players) => players.find((p) => p.id === this.route.snapshot.params['id'])),
        tap((player) => {
            this.form.patchValue({
                backgroundCardImage: player?.background?.image,
                backgroundCardName: player?.background?.name,
                name: player?.name,
            });
        }),
        filter(isNotNullOrUndefined),
    );

    isLoadingBackground = false;

    constructor(
        private cards: CardsService,
        private route: ActivatedRoute,
        private store: Store,
    ) {
        this.player$.subscribe();
    }

    onDelete($event: MouseEvent) {
        $event.preventDefault();
        this.store.dispatch(lifeCounterActions.deletePlayer({ id: this.route.snapshot.params['id'] }));
    }

    onSubmit() {
        const background =
            this.form.value.backgroundCardImage && this.form.value.backgroundCardName
                ? { image: this.form.value.backgroundCardImage, name: this.form.value.backgroundCardName }
                : null;

        const name = this.form.value.name?.trim();
        if (!name) {
            return;
        }

        this.store.dispatch(
            lifeCounterActions.editPlayerEnd({
                id: this.route.snapshot.params['id'],
                changes: {
                    background,
                    name,
                },
            }),
        );
    }

    onSelectBackground(evt: MatAutocompleteSelectedEvent) {
        this.isLoadingBackground = true;
        this.cards
            .byName(evt.option.value)
            .pipe(finalize(() => (this.isLoadingBackground = false)))
            .subscribe((card) => {
                this.form.patchValue({
                    backgroundCardImage: card.image_uris?.art_crop,
                    backgroundCardName: card.name,
                });
            });
    }
}
