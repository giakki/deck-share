import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { mkCard } from '@mocks/card.mock';
import { provideStore, Store } from '@ngrx/store';
import { CardsService } from '@shared/card-api/cards.service';
import { of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { lifeCounterActions } from '../life-counter.actions';
import { mkPlayer, Player } from '../life-counter.model';
import * as fromLifeCounter from '../life-counter.reducer';
import { EditPlayerComponent } from './edit-player.component';

describe('EditPlayerComponent', () => {
    let component: EditPlayerComponent;
    let fixture: ComponentFixture<EditPlayerComponent>;
    let routeParamId: string;
    let store: Store;
    let testScheduler: TestScheduler;

    beforeEach(async () => {
        routeParamId = crypto.randomUUID();
        await TestBed.configureTestingModule({
            imports: [NoopAnimationsModule, EditPlayerComponent],
            providers: [
                provideHttpClient(withInterceptorsFromDi()),
                provideHttpClientTesting(),
                provideStore({
                    [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.reducer,
                }),
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: { params: { id: routeParamId } },
                    },
                },
            ],
        }).compileComponents();

        store = TestBed.inject(Store);
        testScheduler = new TestScheduler((actual, expected) => expect(actual).toEqual(expected));

        fixture = TestBed.createComponent(EditPlayerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('backgroundCards$', () => {
        let autocompleteSpy: jasmine.Spy;

        beforeEach(() => {
            autocompleteSpy = spyOn(TestBed.inject(CardsService), 'autocomplete').and.returnValue(
                of([crypto.randomUUID()]),
            );
        });

        it('should emit undefined and reset the form if the name is not specified', fakeAsync(() => {
            TestBed.runInInjectionContext(() => {
                const values: (string[] | undefined)[] = [];
                const sub = component.backgroundCards$.subscribe((v) => values.push(v));

                tick(500);
                component.form.patchValue({ backgroundCardName: '', backgroundCardImage: crypto.randomUUID() });
                tick(500);

                expect(values).toEqual([undefined]);
                expect(component.form.value.backgroundCardImage).toEqual(null);

                sub.unsubscribe();
                fixture.destroy();
            });
        }));

        it('should emit the correct value', fakeAsync(() => {
            TestBed.runInInjectionContext(() => {
                const sub = component.backgroundCards$.subscribe();
                const backgroundCardName = crypto.randomUUID();

                tick(500);
                component.form.patchValue({ backgroundCardName });
                tick(500);

                // TODO: Investigate why there are two calls
                expect(autocompleteSpy.calls.allArgs()).toEqual([[backgroundCardName], [backgroundCardName]]);

                sub.unsubscribe();
            });
        }));

        it('should catch errors', fakeAsync(() => {
            TestBed.runInInjectionContext(() => {
                const sub = component.backgroundCards$.subscribe();
                const backgroundCardName = crypto.randomUUID();
                autocompleteSpy.and.throwError('error');

                tick(500);
                component.form.patchValue({ backgroundCardName });
                tick(500);

                expect(autocompleteSpy).toHaveBeenCalled();

                sub.unsubscribe();
            });
        }));
    });

    describe('player$', () => {
        let player: Player;

        beforeEach(() => {
            player = mkPlayer({ id: routeParamId });
            store.dispatch(lifeCounterActions.addPlayer({ player }));
        });

        it('should update the form', () => {
            const background = { image: crypto.randomUUID(), name: crypto.randomUUID() };
            store.dispatch(
                lifeCounterActions.updatePlayer({
                    update: {
                        id: routeParamId,
                        changes: {
                            background,
                        },
                    },
                }),
            );
            const spy = spyOn(component.form, 'patchValue');

            component.player$.subscribe();

            expect(spy).toHaveBeenCalledOnceWith({
                backgroundCardImage: background.image,
                backgroundCardName: background.name,
                name: player.name,
            });
        });

        it('should produce the correct values', () => {
            testScheduler.run(({ expectObservable }) => {
                expectObservable(component.player$).toBe('a', { a: player });
            });
        });

        it('should filter out falsy values', () => {
            store.dispatch(lifeCounterActions.deletePlayer({ id: player.id }));

            testScheduler.run(({ expectObservable }) => {
                expectObservable(component.player$).toBe('');
            });
        });
    });

    describe('onDelete', () => {
        it('should dispatch a delete action', () => {
            const spy = spyOn(store, 'dispatch');

            component.onDelete(new MouseEvent('click'));

            expect(spy).toHaveBeenCalled();
        });

        it('should not submit the form', () => {
            const event = new MouseEvent('click');
            const spy = spyOn(event, 'preventDefault');

            component.onDelete(event);

            expect(spy).toHaveBeenCalled();
        });
    });

    describe('onSubmit', () => {
        let spy: jasmine.Spy;

        beforeEach(() => {
            spy = spyOn(store, 'dispatch');
        });

        it('should not dispatch any action if the player has a name', () => {
            component.form.patchValue({ name: '  ' });

            component.onSubmit();

            expect(spy).not.toHaveBeenCalled();
        });

        it('should dispatch the correct action', () => {
            const value = {
                name: crypto.randomUUID() as string,
                backgroundCardImage: crypto.randomUUID() as string,
                backgroundCardName: crypto.randomUUID() as string,
            };
            component.form.patchValue(value);

            component.onSubmit();

            expect(spy).toHaveBeenCalledOnceWith(
                lifeCounterActions.editPlayerEnd({
                    changes: {
                        background: { image: value.backgroundCardImage, name: value.backgroundCardName },
                        name: value.name,
                    },
                    id: routeParamId,
                }),
            );
        });

        it('should dispatch the correct action when the background image is not specified', () => {
            const value = {
                name: crypto.randomUUID() as string,
                backgroundCardImage: '',
                backgroundCardName: crypto.randomUUID() as string,
            };
            component.form.patchValue(value);

            component.onSubmit();

            expect(spy).toHaveBeenCalledOnceWith(
                lifeCounterActions.editPlayerEnd({
                    changes: {
                        background: null,
                        name: value.name,
                    },
                    id: routeParamId,
                }),
            );
        });
    });

    describe('onSelectBackground', () => {
        let player: Player;

        beforeEach(() => {
            player = mkPlayer({ id: routeParamId });
            store.dispatch(lifeCounterActions.addPlayer({ player }));
        });

        it('should call the service', () => {
            const value = crypto.randomUUID();
            const spy = spyOn(TestBed.inject(CardsService), 'byName').and.returnValue(of(mkCard()));

            component.onSelectBackground({ option: { value } } as MatAutocompleteSelectedEvent);

            expect(spy).toHaveBeenCalledOnceWith(value);
        });

        it('should update the form', () => {
            const card = mkCard({
                image_uris: { art_crop: crypto.randomUUID() },
                name: crypto.randomUUID(),
            });
            spyOn(TestBed.inject(CardsService), 'byName').and.returnValue(of(card));

            component.onSelectBackground({ option: { value: undefined } } as MatAutocompleteSelectedEvent);

            expect(component.form.value).toEqual({
                backgroundCardImage: card.image_uris?.art_crop,
                backgroundCardName: card.name,
                name: player.name,
            });
        });
    });
});
