import { TestBed } from '@angular/core/testing';
import { EventType, Router, RouterModule } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store, StoreModule } from '@ngrx/store';
import { testDiForCoverage } from '@tests/util';
import { times } from 'ramda';
import { TestScheduler } from 'rxjs/testing';

import { routerActions } from '../app-routing.module';
import { lifeCounterActions } from './life-counter.actions';
import {
    editPlayerEndNavigateToNewGame$,
    editPlayerEndUpdatePlayer$,
    editPlayerStart$,
    pageRedirect$,
    provideLifeCounterEffects,
} from './life-counter.effects';
import { mkGame, mkPlayer } from './life-counter.model';
import * as fromLifeCounter from './life-counter.reducer';

describe('lifeCounter Effects', () => {
    let actions: Actions;
    let router: Router;
    let store: Store;
    let testScheduler: TestScheduler;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterModule.forRoot([]),
                StoreModule.forRoot({
                    [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.reducer,
                }),
            ],
            providers: [provideMockActions(() => new Actions())],
        });

        actions = TestBed.inject(Actions);
        router = TestBed.inject(Router);
        store = TestBed.inject(Store);

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    describe('provideLifeCounterEffects', () => {
        it('should not throw', () => {
            expect(() => provideLifeCounterEffects()).not.toThrow();
        });
    });

    describe('editPlayerStart', () => {
        it('should auto inject services', () => {
            testDiForCoverage(editPlayerStart$, actions, store);
        });

        it('should not run if the player does not exist', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('a', {
                    a: lifeCounterActions.editPlayerStart({ id: crypto.randomUUID() }),
                });

                const result$ = TestBed.runInInjectionContext(() => editPlayerStart$(actions$));

                expectObservable(result$).toBe('---');
            });
        });

        it('should navigate to the player edit page', () => {
            const player = mkPlayer();
            store.dispatch(lifeCounterActions.addPlayer({ player }));

            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('a', {
                    a: lifeCounterActions.editPlayerStart({ id: player.id }),
                });

                const result$ = TestBed.runInInjectionContext(() => editPlayerStart$(actions$));

                expectObservable(result$).toBe('a', {
                    a: routerActions.navigate({
                        commands: ['life-counter', 'new-game', 'player', player.id],
                    }),
                });
            });
        });
    });

    describe('editPlayerEndUpdatePlayer', () => {
        it('should auto inject services', () => {
            testDiForCoverage(editPlayerEndUpdatePlayer$, actions);
        });

        it('should dispatch the correct action', () => {
            const player = mkPlayer();

            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('a', {
                    a: lifeCounterActions.editPlayerEnd({ changes: player, id: player.id }),
                });

                const result$ = TestBed.runInInjectionContext(() => editPlayerEndUpdatePlayer$(actions$));

                expectObservable(result$).toBe('a', {
                    a: lifeCounterActions.updatePlayer({
                        update: { id: player.id, changes: player },
                    }),
                });
            });
        });
    });

    describe('editPlayerEndNavigateToNewGame', () => {
        it('should auto inject services', () => {
            testDiForCoverage(editPlayerEndNavigateToNewGame$, actions);
        });

        it('should dispatch the correct action', () => {
            const player = mkPlayer();

            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('a', {
                    a: lifeCounterActions.editPlayerEnd({ changes: player, id: player.id }),
                });

                const result$ = TestBed.runInInjectionContext(() => editPlayerEndNavigateToNewGame$(actions$));

                expectObservable(result$).toBe('a', {
                    a: routerActions.navigate({ commands: ['life-counter', 'new-game'] }),
                });
            });
        });
    });

    describe('pageRedirect', () => {
        it('should auto inject services', () => {
            testDiForCoverage(pageRedirect$, actions, router, store);
        });

        it('should not act if the feature is not active', () => {
            spyOn(router, 'isActive').and.returnValue(false);

            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('a', {
                    a: routerNavigatedAction({
                        payload: {
                            event: { id: 0, type: EventType.NavigationEnd, url: '', urlAfterRedirects: '/' },
                            routerState: { root: {} as any, url: '/' },
                        },
                    }),
                });

                const result$ = TestBed.runInInjectionContext(() => pageRedirect$(actions$));

                expectObservable(result$).toBe('---');
            });
        });

        it('should redirect to the game page if the game is running', () => {
            spyOn(router, 'isActive').and.returnValue(true);
            store.dispatch(lifeCounterActions.startGame({ game: mkGame('edh') }));

            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('-a-', {
                    a: routerNavigatedAction({
                        payload: {
                            event: { id: 0, type: EventType.NavigationEnd, url: '', urlAfterRedirects: '/' },
                            routerState: { root: {} as any, url: '/' },
                        },
                    }),
                });

                const result$ = TestBed.runInInjectionContext(() => pageRedirect$(actions$));

                expectObservable(result$).toBe('-a-', {
                    a: routerActions.navigateByUrl({
                        url: '/life-counter',
                    }),
                });
            });
        });

        it('should allow navigating to the add player url', () => {
            const player = mkPlayer();
            spyOn(router, 'isActive').and.returnValue(true);
            store.dispatch(lifeCounterActions.addPlayer({ player }));

            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('-a-', {
                    a: routerNavigatedAction({
                        payload: {
                            event: {
                                id: 0,
                                type: EventType.NavigationEnd,
                                url: `/life-counter/new-game/player/${player.id}`,
                                urlAfterRedirects: '/',
                            },
                            routerState: { root: {} as any, url: '/' },
                        },
                    }),
                });

                const result$ = TestBed.runInInjectionContext(() => pageRedirect$(actions$));

                expectObservable(result$).toBe('-a-', {
                    a: routerActions.navigateByUrl({
                        url: `/life-counter/new-game/player/${player.id}`,
                    }),
                });
            });
        });

        it('should act on changes to the inner observables', () => {
            spyOn(router, 'isActive').and.returnValues(...times((i) => i > 0, 10));

            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('-a-b-c-d-e-f-', {
                    a: routerNavigatedAction({
                        payload: {
                            event: { id: 0, type: EventType.NavigationEnd, url: '', urlAfterRedirects: '/' },
                            routerState: { root: {} as any, url: '/' },
                        },
                    }),
                    b: routerNavigatedAction({
                        payload: {
                            event: {
                                id: 0,
                                type: EventType.NavigationEnd,
                                url: '',
                                urlAfterRedirects: '/life-counter/new-game',
                            },
                            routerState: { root: {} as any, url: '/life-counter/new-game' },
                        },
                    }),
                    c: lifeCounterActions.addPlayer({ player: mkPlayer() }),
                    d: lifeCounterActions.startGame({ game: mkGame('edh') }),
                    e: routerNavigatedAction({
                        payload: {
                            event: {
                                id: 0,
                                type: EventType.NavigationEnd,
                                url: '',
                                urlAfterRedirects: '/life-counter',
                            },
                            routerState: { root: {} as any, url: '/life-counter' },
                        },
                    }),
                    f: lifeCounterActions.newGame(),
                });

                const result$ = TestBed.runInInjectionContext(() => pageRedirect$(actions$));

                expectObservable(result$).toBe('---a-----b-', {
                    a: { type: 'NOOP' },
                    b: routerActions.navigateByUrl({
                        url: '/life-counter/new-game',
                    }),
                });
            });
        });
    });
});
