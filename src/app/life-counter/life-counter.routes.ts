import { Routes } from '@angular/router';

import { EditPlayerComponent } from './edit-player/edit-player.component';
import { LifeCounterComponent } from './life-counter.component';
import { provideLifeCounterEffects } from './life-counter.effects';
import { NewGameComponent } from './new-game/new-game.component';

export const routes: Routes = [
    {
        path: '',
        providers: [provideLifeCounterEffects()],
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: LifeCounterComponent,
                data: { ui: { layout: 'empty' } },
            },
            {
                path: 'new-game',
                pathMatch: 'full',
                component: NewGameComponent,
            },
            {
                path: 'new-game/player/:id',
                component: EditPlayerComponent,
            },
        ],
    },
];
