import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LetDirective, PushPipe } from '@ngrx/component';
import { select, Store } from '@ngrx/store';

import { CounterOptionsComponent } from './counter-options/counter-options.component';
import { lifeCounterActions } from './life-counter.actions';
import { EditingMode } from './life-counter.reducer';
import {
    selectLifeCounterEditingMode,
    selectLifeCounterPlayers,
    selectLifeCounterShowOptions,
} from './life-counter.selectors';
import { PlayerCounterComponent } from './player-counter/player-counter.component';

@Component({
    styleUrl: './life-counter.component.scss',
    templateUrl: './life-counter.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [LetDirective, PushPipe, PlayerCounterComponent, CounterOptionsComponent],
})
export class LifeCounterComponent {
    mode$ = this.store.pipe(select(selectLifeCounterEditingMode));

    players$ = this.store.pipe(select(selectLifeCounterPlayers));

    showOptions$ = this.store.pipe(select(selectLifeCounterShowOptions));

    constructor(private store: Store) {}

    onCreateNew() {
        this.store.dispatch(lifeCounterActions.newGame());
    }

    onPlayerCounterChange(playerId: string, total: number) {
        this.store.dispatch(lifeCounterActions.playerCurrentEditChange({ playerId, total }));
    }

    onToggleEditingMode(mode: EditingMode) {
        this.store.dispatch(lifeCounterActions.toggleEditingMode({ mode }));
    }

    onToggleOptions() {
        this.store.dispatch(lifeCounterActions.toggleOptions());
    }
}
