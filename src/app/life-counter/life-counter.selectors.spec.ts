import { times } from 'ramda';

import { mkPlayer, playerAdatapter } from './life-counter.model';
import * as fromLifeCounter from './life-counter.reducer';
import {
    selectLifeCounterEditingMode,
    selectLifeCounterIsGameRunning,
    selectLifeCounterPlayers,
    selectLifeCounterShowOptions,
    selectLifeCounterState,
} from './life-counter.selectors';

describe('LifeCounter Selectors', () => {
    describe('selectLifeCounterState', () => {
        it('should select the feature state', () => {
            const result = selectLifeCounterState({
                [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.initialState,
            });

            expect(result).toEqual(fromLifeCounter.initialState);
        });
    });

    describe('selectPlayersEntityState', () => {
        it('should select the all the player entities', () => {
            const expected = times((i) => mkPlayer({ order: i }), 5);
            const state: fromLifeCounter.State = {
                ...fromLifeCounter.initialState,
                players: playerAdatapter.addMany(expected, fromLifeCounter.initialState.players),
            };
            const result = selectLifeCounterPlayers({
                [fromLifeCounter.lifeCounterFeatureKey]: state,
            });

            expect(result).toEqual(expected);
        });

        it('should sort the resulting entities', () => {
            const orders = [1, 4, 5, 3, 2];
            const expected = times((i) => mkPlayer({ order: orders[i] }), 5);
            const state: fromLifeCounter.State = {
                ...fromLifeCounter.initialState,
                players: playerAdatapter.addMany(expected, fromLifeCounter.initialState.players),
            };
            const result = selectLifeCounterPlayers({
                [fromLifeCounter.lifeCounterFeatureKey]: state,
            });

            for (let i = 0; i < orders.length; ++i) {
                expect(result[i].order).toEqual(i + 1);
            }
        });
    });

    describe('selectLifeCounterEditingMode', () => {
        it('should select the correct value', () => {
            const result = selectLifeCounterEditingMode({
                [fromLifeCounter.lifeCounterFeatureKey]: {
                    ...fromLifeCounter.initialState,
                    editingMode: 'poison',
                },
            });

            expect(result).toEqual('poison');
        });
    });

    describe('selectLifeCounterIsGameRunning', () => {
        it('should select the correct value', () => {
            const result = selectLifeCounterIsGameRunning({
                [fromLifeCounter.lifeCounterFeatureKey]: {
                    ...fromLifeCounter.initialState,
                    isGameRunning: true,
                },
            });

            expect(result).toEqual(true);
        });
    });

    describe('selectLifeCounterShowOptions', () => {
        it('should select the correct value', () => {
            const result = selectLifeCounterShowOptions({
                [fromLifeCounter.lifeCounterFeatureKey]: {
                    ...fromLifeCounter.initialState,
                    showOptions: true,
                },
            });

            expect(result).toEqual(true);
        });
    });
});
