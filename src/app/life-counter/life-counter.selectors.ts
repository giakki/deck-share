import { createFeatureSelector, createSelector } from '@ngrx/store';

import { playerAdatapter } from './life-counter.model';
import * as fromLifeCounter from './life-counter.reducer';

export const selectLifeCounterState = createFeatureSelector<fromLifeCounter.State>(
    fromLifeCounter.lifeCounterFeatureKey,
);

export const playerSelectors = playerAdatapter.getSelectors();

const selectPlayersEntityState = createSelector(selectLifeCounterState, (state) => state.players);

export const selectLifeCounterEditingMode = createSelector(selectLifeCounterState, (state) => state.editingMode);

export const selectLifeCounterPlayers = createSelector(selectPlayersEntityState, playerSelectors.selectAll);

export const selectLifeCounterIsGameRunning = createSelector(selectLifeCounterState, (state) => state.isGameRunning);

export const selectLifeCounterShowOptions = createSelector(selectLifeCounterState, (state) => state.showOptions);
