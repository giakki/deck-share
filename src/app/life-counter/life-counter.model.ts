import { createEntityAdapter } from '@ngrx/entity';

export interface PlayerBackground {
    image: string;
    name: string;
}

export interface NewGamePlayer {
    id: string;
    name: string;
    background: PlayerBackground | null;
}

export interface Player extends NewGamePlayer {
    lifeTotal: number;
    order: number;
    poisonCounters: number;
}

export function mkPlayer(player?: Partial<Player>): Player {
    const id = player?.id || crypto.randomUUID();

    return {
        lifeTotal: 0,
        name: 'Player',
        order: 0,
        poisonCounters: 0,
        background: null,
        ...player,
        id,
    };
}

export function playerOrderComparer(a: Player, b: Player) {
    return a.order - b.order;
}

export const playerAdatapter = createEntityAdapter<Player>({
    sortComparer: playerOrderComparer,
});

export const GAME_TYPES = ['edh', 'kingdom', 'other'] as const;

export type GameType = (typeof GAME_TYPES)[number];

export interface GameBase {
    readonly type: GameType;
}

export interface GameEDH extends GameBase {
    readonly type: 'edh';
}

export interface GameKingdom extends GameBase {
    readonly type: 'kingdom';
    king: string;
}

export function isPlayerKing(game: GameKingdom, playerId: string) {
    return game.king === playerId;
}

export interface GameOther extends GameBase {
    readonly type: 'other';
}

export type Game = GameEDH | GameKingdom | GameOther;

export function mkGame(type: 'edh' | 'other'): Game {
    return { type };
}

export function mkGameKingdom(king: string): GameKingdom {
    return { type: 'kingdom', king };
}

export function tryMkGame(type: GameType | null | undefined, king?: string | null): Game | null {
    switch (type) {
        case 'kingdom': {
            if (king) {
                return mkGameKingdom(king);
            }
            return null;
        }
        case 'edh':
        case 'other':
            return mkGame(type);
        default:
            return null;
    }
}

export function startingLifeTotalForGameType(game: Game, playerId: string) {
    switch (game.type) {
        case 'edh':
            return 40;
        case 'other':
            return 20;
        case 'kingdom':
            return isPlayerKing(game, playerId) ? 50 : 40;
    }
}
