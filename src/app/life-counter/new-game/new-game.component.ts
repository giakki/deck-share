import { NgIf } from '@angular/common';
import { Component, inject } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PushPipe } from '@ngrx/component';
import { select, Store } from '@ngrx/store';
import { map, take, tap } from 'rxjs';

import { lifeCounterActions } from '../life-counter.actions';
import { GameType, mkPlayer, tryMkGame } from '../life-counter.model';
import { selectLifeCounterPlayers } from '../life-counter.selectors';

interface NewGameFormValue {
    king?: string | null;
    gameType?: GameType | null;
}

const validatePlayers: () => AsyncValidatorFn =
    (store = inject(Store)) =>
    (_: AbstractControl<NewGameFormValue>) =>
        store.pipe(
            select(selectLifeCounterPlayers),
            take(1),
            map((players) => {
                if (players.length < 2) {
                    return { notEnoughPlayers: true };
                }
                if (players.length > 5) {
                    return { tooManyPlayers: true };
                }
                return null;
            }),
        );

@Component({
    templateUrl: './new-game.component.html',
    styleUrl: './new-game.component.scss',
    standalone: true,
    imports: [
        ReactiveFormsModule,
        NgIf,
        PushPipe,
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatOptionModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
    ],
})
export class NewGameComponent {
    readonly form = inject(FormBuilder).group(
        {
            gameType: ['edh' as GameType, { validators: Validators.required }],
            king: [null as string | null],
        },
        {
            asyncValidators: [validatePlayers()],
        },
    );

    readonly gameTypes = [
        { id: 'other', value: '20-life' },
        { id: 'edh', value: 'EDH' },
        { id: 'kingdom', value: 'Kingdom' },
    ];

    readonly players = this.store
        .pipe(select(selectLifeCounterPlayers))
        .pipe(tap(() => this.form.updateValueAndValidity()));

    constructor(private store: Store) {}

    onAddPlayer($event: MouseEvent) {
        const player = mkPlayer();

        this.store.dispatch(lifeCounterActions.addPlayer({ player }));
        this.onEditPlayer($event, player.id);
    }

    onDeletePlayer($event: MouseEvent, id: string) {
        $event.preventDefault();
        this.store.dispatch(lifeCounterActions.deletePlayer({ id }));
    }

    onEditPlayer($event: MouseEvent, id: string) {
        $event.preventDefault();
        this.store.dispatch(lifeCounterActions.editPlayerStart({ id }));
    }

    onSubmit() {
        if (!this.form.valid) {
            return;
        }

        const game = tryMkGame(this.form.value.gameType, this.form.value.king);
        if (!game) {
            return;
        }

        this.store.dispatch(lifeCounterActions.startGame({ game }));
    }
}
