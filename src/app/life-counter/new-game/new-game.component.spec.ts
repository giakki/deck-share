import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Store, StoreModule } from '@ngrx/store';

import { lifeCounterActions } from '../life-counter.actions';
import { mkGameKingdom, mkPlayer } from '../life-counter.model';
import * as fromLifeCounter from '../life-counter.reducer';
import { NewGameComponent } from './new-game.component';

describe('NewGameComponent', () => {
    let component: NewGameComponent;
    let fixture: ComponentFixture<NewGameComponent>;
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                HttpClientTestingModule,
                StoreModule.forRoot(
                    {
                        [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.reducer,
                    },
                    {
                        initialState: {
                            [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.initialState,
                        },
                    },
                ),
                MatCardModule,
                MatFormFieldModule,
                MatIconModule,
                MatInputModule,
                MatListModule,
                MatSelectModule,
                MatTooltipModule,
                NewGameComponent,
            ],
        });

        store = TestBed.inject(Store);

        fixture = TestBed.createComponent(NewGameComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('form', () => {
        it('should check the minimum number of players', () => {
            component.form.updateValueAndValidity();

            expect(component.form.valid).toBeFalse();
        });

        it('should check the maximum number of players', () => {
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));

            component.form.updateValueAndValidity();

            expect(component.form.valid).toBeFalse();
        });

        it('should accept the correct nuber of players', () => {
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            component.form.updateValueAndValidity();
            expect(component.form.valid).toBeTrue();

            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            component.form.updateValueAndValidity();
            expect(component.form.valid).toBeTrue();

            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            component.form.updateValueAndValidity();
            expect(component.form.valid).toBeTrue();

            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            component.form.updateValueAndValidity();
            expect(component.form.valid).toBeTrue();
        });

        it('should check the game type', () => {
            component.form.patchValue({ gameType: null });
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));

            component.form.updateValueAndValidity();

            expect(component.form.valid).toBeFalse();
        });

        it('should check the king', () => {
            component.form.patchValue({ gameType: 'kingdom', king: null });
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            store.dispatch(lifeCounterActions.addPlayer({ player: mkPlayer() }));
            fixture.detectChanges();

            component.form.updateValueAndValidity();

            expect(component.form.valid).toBeFalse();
        });
    });

    describe('onAddPlayer', () => {
        it('should dispatch the correct action', () => {
            const spy = spyOn(store, 'dispatch');

            component.onAddPlayer(new MouseEvent('click'));

            expect(spy).toHaveBeenCalledWith(lifeCounterActions.addPlayer({ player: jasmine.any(Object) as any }));
            expect(spy).toHaveBeenCalledWith(lifeCounterActions.editPlayerStart({ id: jasmine.any(String) as any }));
        });

        it('should not submit the form', () => {
            const event = new MouseEvent('click');
            const spy = spyOn(event, 'preventDefault');

            component.onAddPlayer(event);

            expect(spy).toHaveBeenCalled();
        });
    });

    describe('onDeletePlayer', () => {
        it('should dispatch the correct action', () => {
            const spy = spyOn(store, 'dispatch');
            const id = crypto.randomUUID();

            component.onDeletePlayer(new MouseEvent('click'), id);

            expect(spy).toHaveBeenCalledWith(lifeCounterActions.deletePlayer({ id }));
        });

        it('should not submit the form', () => {
            const event = new MouseEvent('click');
            const spy = spyOn(event, 'preventDefault');

            component.onDeletePlayer(event, crypto.randomUUID());

            expect(spy).toHaveBeenCalled();
        });
    });

    describe('onEditPlayer', () => {
        it('should dispatch the correct action', () => {
            const spy = spyOn(store, 'dispatch');
            const id = crypto.randomUUID();

            component.onEditPlayer(new MouseEvent('click'), id);

            expect(spy).toHaveBeenCalledWith(lifeCounterActions.editPlayerStart({ id }));
        });

        it('should not submit the form', () => {
            const event = new MouseEvent('click');
            const spy = spyOn(event, 'preventDefault');

            component.onEditPlayer(event, crypto.randomUUID());

            expect(spy).toHaveBeenCalled();
        });
    });

    describe('onSubmit', () => {
        let spy: jasmine.Spy;

        beforeEach(() => {
            spy = spyOn(store, 'dispatch');
        });

        it('should not dispatch any action if the form is not valid', () => {
            component.form.patchValue({ gameType: 'kingdom', king: null });

            component.onSubmit();

            expect(spy).not.toHaveBeenCalled();
        });

        it('should not dispatch any action if the game cannot be created', () => {
            spyOnProperty(component.form, 'valid').and.returnValue(true);
            component.form.patchValue({ gameType: 'error' as any });

            component.onSubmit();

            expect(spy).not.toHaveBeenCalled();
        });

        it('should dispatch the correct action', () => {
            const game = mkGameKingdom(crypto.randomUUID());
            spyOnProperty(component.form, 'valid').and.returnValue(true);
            component.form.patchValue({ gameType: game.type, king: game.king });
            component.form.updateValueAndValidity();

            component.onSubmit();

            expect(spy).toHaveBeenCalledOnceWith(lifeCounterActions.startGame({ game }));
        });
    });
});
