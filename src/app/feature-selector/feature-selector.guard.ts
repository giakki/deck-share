import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';
import { omit } from 'ramda';

export const featureSelectorGuard: CanActivateFn = (route: ActivatedRouteSnapshot, _: RouterStateSnapshot) => {
    if (route.queryParams['origin']) {
        const queryParams = { ...route.queryParams, origin: undefined };

        const newUrl = inject(Router).parseUrl(`${route.queryParams['origin']}`);
        newUrl.queryParams = omit(['origin'], queryParams);

        return newUrl;
    }
    return true;
};
