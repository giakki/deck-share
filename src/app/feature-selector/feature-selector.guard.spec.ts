import { TestBed } from '@angular/core/testing';
import { provideRouter, UrlTree } from '@angular/router';

import { featureSelectorGuard } from './feature-selector.guard';

describe('featureSelectorGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [provideRouter([])],
        });
    });

    it("should return a redirect if the url contains an 'origin' query parameter", () => {
        const origin = crypto.randomUUID();

        const result = TestBed.runInInjectionContext(() =>
            featureSelectorGuard({ queryParams: { origin } } as any, {} as any),
        );

        expect(result).toBeInstanceOf(UrlTree);
    });

    it('should produce the expected url', () => {
        const origin = crypto.randomUUID();

        const result = TestBed.runInInjectionContext(() =>
            featureSelectorGuard({ queryParams: { origin } } as any, {} as any),
        );

        expect((result as UrlTree).toString()).toEqual(`/${origin}`);
    });

    it('should preserve query parameters on redirect', () => {
        const origin = crypto.randomUUID();
        const expectedQueryParams = { __test__: crypto.randomUUID() };

        const result = TestBed.runInInjectionContext(() =>
            featureSelectorGuard({ queryParams: { ...expectedQueryParams, origin } } as any, {} as any),
        );

        expect((result as UrlTree).toString()).toEqual(`/${origin}?__test__=${expectedQueryParams.__test__}`);
    });

    it('should always return true in other cases', () => {
        const result1 = TestBed.runInInjectionContext(() =>
            featureSelectorGuard({ queryParams: {} } as any, {} as any),
        );
        expect(result1).toBeTrue();
    });
});
