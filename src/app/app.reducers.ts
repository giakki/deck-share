import { ROOT_EFFECTS_INIT } from '@ngrx/effects';
import {
    DEFAULT_ROUTER_FEATURENAME,
    routerReducer,
    RouterReducerState,
    SerializedRouterStateSnapshot,
} from '@ngrx/router-store';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { mergeDeepRight } from 'ramda';

import * as fromAuth from './auth/auth.reducer';
import * as fromLifeCounter from './life-counter/life-counter.reducer';
import * as fromCards from './shared/card-api/cards.reducer';
import * as fromUi from './ui/ui.reducer';

export interface State {
    [fromAuth.authFeatureKey]: fromAuth.State;
    [fromCards.cardsFeatureKey]: fromCards.State;
    [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.State;
    [fromUi.uiFeatureKey]: fromUi.State;
    [DEFAULT_ROUTER_FEATURENAME]: RouterReducerState<SerializedRouterStateSnapshot> | undefined;
}

export const storableFeatureKeys = [fromLifeCounter.lifeCounterFeatureKey] as const;

function getFeatureLocalstorageKey(feature: string) {
    return `${feature}StoredState`;
}

export const initialState: State = {
    [DEFAULT_ROUTER_FEATURENAME]: undefined,
    [fromAuth.authFeatureKey]: fromAuth.initialState,
    [fromCards.cardsFeatureKey]: fromCards.initialState,
    [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.initialState,
    [fromUi.uiFeatureKey]: fromUi.initialState,
};

export const metaReducers: MetaReducer<State>[] = [
    (reducer) => (state, action) => {
        if (!state) {
            return reducer(state, action);
        }

        if (action.type === ROOT_EFFECTS_INIT) {
            for (const key of storableFeatureKeys) {
                const feature = localStorage.getItem(getFeatureLocalstorageKey(key));
                if (feature) {
                    state = mergeDeepRight(state, { [key]: JSON.parse(feature) });
                }
            }
            return reducer(state, action);
        }

        const newState = reducer(state, action);
        for (const key of storableFeatureKeys) {
            if (newState[key] !== state[key]) {
                localStorage.setItem(getFeatureLocalstorageKey(key), JSON.stringify(newState[key]));
            }
        }
        return newState;
    },
];

export const reducers: ActionReducerMap<State> = {
    [DEFAULT_ROUTER_FEATURENAME]: routerReducer,
    [fromAuth.authFeatureKey]: fromAuth.reducer,
    [fromCards.cardsFeatureKey]: fromCards.reducer,
    [fromLifeCounter.lifeCounterFeatureKey]: fromLifeCounter.reducer,
    [fromUi.uiFeatureKey]: fromUi.reducer,
};
