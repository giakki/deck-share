import { getApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import {
    initializeFirestore,
    persistentLocalCache,
    persistentMultipleTabManager,
    provideFirestore,
} from '@angular/fire/firestore';
import { provideServiceWorker } from '@angular/service-worker';

export const extModules = [
    provideServiceWorker('/deck-share/ngsw-worker.js', {
        registrationStrategy: 'registerWhenStable:30000',
    }),
    provideAuth(() => getAuth()),
    provideFirestore(() =>
        initializeFirestore(getApp(), {
            localCache: persistentLocalCache({ tabManager: persistentMultipleTabManager() }),
        }),
    ),
];
