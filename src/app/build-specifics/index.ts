import { getApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import {
    connectFirestoreEmulator,
    initializeFirestore,
    persistentLocalCache,
    persistentMultipleTabManager,
    provideFirestore,
} from '@angular/fire/firestore';
import { connectAuthEmulator } from '@firebase/auth';
import { provideStoreDevtools } from '@ngrx/store-devtools';

export const extModules = [
    provideStoreDevtools({
        maxAge: 25,
    }),
    provideAuth(() => {
        const auth = getAuth();
        connectAuthEmulator(auth, 'http://localhost:9099', { disableWarnings: true });
        return auth;
    }),
    provideFirestore(() => {
        const firestore = initializeFirestore(getApp(), {
            localCache: persistentLocalCache({ tabManager: persistentMultipleTabManager() }),
        });

        connectFirestoreEmulator(firestore, 'localhost', 8080);

        return firestore;
    }),
];
