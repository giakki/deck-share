import { Component } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { mkCard } from '@mocks/card.mock';

import { CardPopupDirective } from './card-popup.directive';

@Component({
    imports: [CardPopupDirective],
    standalone: true,
    template: `<div [appCardPopup]="card"></div>`,
})
class HostComponent {
    card = mkCard();
}

function dispatchMouseEvent(element: HTMLElement, name: string) {
    element.dispatchEvent(
        new MouseEvent(name, {
            view: window,
            bubbles: true,
            cancelable: true,
        }),
    );
}
function getPopupElement(fixture: ComponentFixture<unknown>) {
    return fixture.debugElement.nativeElement?.ownerDocument?.querySelector('app-card-popup');
}
function getHostElement(fixture: ComponentFixture<unknown>) {
    return fixture.debugElement.nativeElement.children[0];
}

describe('CardPopupDirective', () => {
    let fixture: ComponentFixture<HostComponent>;
    let directive: CardPopupDirective;

    beforeEach(() => {
        fixture = TestBed.configureTestingModule({ imports: [HostComponent] }).createComponent(HostComponent);
        fixture.detectChanges();
        directive = fixture.debugElement.query(By.directive(CardPopupDirective)).injector.get(CardPopupDirective);
    });

    it('should create an instance', () => {
        expect(directive).toBeTruthy();
    });

    it('should show a popup while hovering on the element', () => {
        dispatchMouseEvent(getHostElement(fixture), 'mouseenter');

        expect(getPopupElement(fixture)).toBeTruthy();
    });

    it('should close the popup after 500ms', fakeAsync(() => {
        dispatchMouseEvent(getHostElement(fixture), 'mouseenter');
        dispatchMouseEvent(getHostElement(fixture), 'mouseleave');
        tick(499);

        expect(getPopupElement(fixture)).toBeTruthy();

        tick(1);

        expect(getPopupElement(fixture)).toBeFalsy();
    }));

    it('should toggle the popup when clicking on the element', () => {
        dispatchMouseEvent(getHostElement(fixture), 'click');
        expect(getPopupElement(fixture)).toBeTruthy();

        dispatchMouseEvent(getHostElement(fixture), 'click');
        expect(getPopupElement(fixture)).toBeFalsy();
    });

    it('should listen for mouse events on the popup component', fakeAsync(() => {
        dispatchMouseEvent(getHostElement(fixture), 'click');
        dispatchMouseEvent(getHostElement(fixture), 'mouseleave');
        dispatchMouseEvent(getPopupElement(fixture), 'mouseenter');
        tick(501);

        expect(getPopupElement(fixture)).toBeTruthy();

        dispatchMouseEvent(getPopupElement(fixture), 'mouseleave');
        tick(501);

        expect(getPopupElement(fixture)).toBeFalsy();
    }));

    it('should handle strange events', () => {
        dispatchMouseEvent(getHostElement(fixture), 'mouseleave');
        dispatchMouseEvent(getHostElement(fixture), 'mouseleave');
        dispatchMouseEvent(getHostElement(fixture), 'mouseleave');

        expect(getPopupElement(fixture)).toBeFalsy();
    });
});
