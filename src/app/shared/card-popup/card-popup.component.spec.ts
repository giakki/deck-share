import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { mkCard } from '@mocks/card.mock';
import { ReplaySubject } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { CardPopupComponent } from './card-popup.component';

describe('CardPopupComponent', () => {
    let component: CardPopupComponent;
    let fixture: ComponentFixture<CardPopupComponent>;
    let testScheduler: TestScheduler;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [CardPopupComponent],
        }).compileComponents();

        fixture = TestBed.createComponent(CardPopupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        testScheduler = new TestScheduler((actual, expected) => expect(actual).toEqual(expected));
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should emit an event on mouseenter', () => {
        testScheduler.run(({ expectObservable }) => {
            const events = new ReplaySubject(1);
            const sub = component.mouseEnter.subscribe((e) => events.next(e));

            fixture.debugElement.nativeElement.dispatchEvent(
                new MouseEvent('mouseenter', {
                    view: window,
                    bubbles: true,
                    cancelable: true,
                }),
            );

            expectObservable(events).toBe('a', { a: undefined });
            sub.unsubscribe();
        });
    });

    it('should emit an event on mouseleave', () => {
        testScheduler.run(({ expectObservable }) => {
            const events = new ReplaySubject(1);
            const sub = component.mouseLeave.subscribe((e) => events.next(e));

            fixture.debugElement.nativeElement.dispatchEvent(
                new MouseEvent('mouseleave', {
                    view: window,
                    bubbles: true,
                    cancelable: true,
                }),
            );

            expectObservable(events).toBe('a', { a: undefined });
            sub.unsubscribe();
        });
    });

    it("should display the card's image", () => {
        const expected = crypto.randomUUID();
        fixture.componentRef.setInput(
            'card',
            mkCard({
                image_uris: {
                    normal: expected,
                },
            }),
        );
        fixture.detectChanges();

        const imgs = fixture.debugElement.queryAll(By.css('img')).map((i) => i.nativeElement.src);

        expect(imgs).toEqual([`${window.location.origin}/${expected}`]);
    });

    it('should handle multi-face cards', () => {
        const expected1 = crypto.randomUUID();
        const expected2 = crypto.randomUUID();
        fixture.componentRef.setInput(
            'card',
            mkCard({
                image_uris: undefined,
                card_faces: [
                    {
                        name: crypto.randomUUID(),
                        image_uris: { normal: expected1 },
                        type_line: '',
                    },
                    {
                        name: crypto.randomUUID(),
                        image_uris: { normal: expected2 },
                        type_line: '',
                    },
                ],
            }),
        );
        fixture.detectChanges();

        let imgs = fixture.debugElement.queryAll(By.css('img')).map((i) => i.nativeElement.src);
        expect(imgs).toEqual([`${window.location.origin}/${expected1}`]);

        let button = fixture.debugElement.query(By.css('[role="button"]'));
        expect(button).toBeTruthy();

        button.nativeElement.click();
        fixture.detectChanges();

        imgs = fixture.debugElement.queryAll(By.css('img')).map((i) => i.nativeElement.src);
        expect(imgs).toEqual([`${window.location.origin}/${expected2}`]);

        button = fixture.debugElement.query(By.css('[role="button"]'));
        expect(button).toBeTruthy();

        button.nativeElement.click();
        fixture.detectChanges();

        imgs = fixture.debugElement.queryAll(By.css('img')).map((i) => i.nativeElement.src);
        expect(imgs).toEqual([`${window.location.origin}/${expected1}`]);
    });

    describe('coverage', () => {
        it('should handle undefined card', () => {
            fixture.componentRef.setInput('card', mkCard());
            fixture.detectChanges();
            fixture.componentRef.setInput('card', undefined);
            fixture.detectChanges();

            const imgs = fixture.debugElement.queryAll(By.css('img')).map((i) => i.nativeElement.src);
            expect(imgs).toEqual([]);
        });
    });
});
