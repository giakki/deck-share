import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Directive, ElementRef, HostListener, inject, input, OnDestroy, ViewContainerRef } from '@angular/core';
import { Card } from '@shared/card-api/cards.model';

import { CardPopupComponent } from './card-popup.component';

interface PopupStateOpen {
    overlayRef: OverlayRef;
}
interface PopupStateClosing {
    overlayRef: OverlayRef;
    timeout: number;
}
interface PopupStateClosed {
    state: 'closed';
}

type PopupState = PopupStateOpen | PopupStateClosing | PopupStateClosed;

function isPopupStateOpen(popupState: PopupState) {
    return 'overlayRef' in popupState;
}

function popupStateCancelClosing(state: PopupState): PopupState {
    if ('timeout' in state) {
        clearTimeout(state.timeout);
        return { overlayRef: state.overlayRef };
    }
    return state;
}

function popupStateTransitionClosed(state: PopupState): PopupState {
    if ('timeout' in state) {
        clearTimeout(state.timeout);
    }
    if ('overlayRef' in state) {
        state.overlayRef.dispose();
    }
    return { state: 'closed' };
}

function popupStateTransitionClosing(state: PopupState, cb: () => void) {
    if ('overlayRef' in state) {
        return {
            ...state,
            timeout: setTimeout(cb, 500) as unknown as number,
        };
    }

    return state;
}

function popupStateTransitionOpen(state: PopupState, overlayRef: OverlayRef) {
    state = popupStateTransitionClosed(state);

    return {
        overlayRef,
    };
}

@Directive({
    selector: '[appCardPopup]',
    standalone: true,
})
export class CardPopupDirective implements OnDestroy {
    appCardPopup = input<Card>();

    @HostListener('mouseenter')
    onMouseEnter() {
        this.openTooltip();
    }

    @HostListener('mouseleave')
    onMouseLeave() {
        this.startCloseTooltipTimeout();
    }

    @HostListener('click')
    onClick() {
        this.toggleTooltip();
    }

    private readonly el = inject(ElementRef);

    private readonly overlay = inject(Overlay);

    private readonly vcr = inject(ViewContainerRef);

    private popupState: PopupState = { state: 'closed' };

    ngOnDestroy(): void {
        this.popupState = popupStateTransitionClosed(this.popupState);
    }

    private openTooltip() {
        this.popupState = popupStateCancelClosing(this.popupState);
        if (isPopupStateOpen(this.popupState)) {
            return;
        }
        this.closeTooltip();
        const overlayRef = this.overlay.create({
            positionStrategy: this.overlay
                .position()
                .flexibleConnectedTo(this.el)
                .withPositions([
                    {
                        originX: 'center',
                        originY: 'top',
                        overlayX: 'center',
                        overlayY: 'bottom',
                        offsetY: -8,
                    },
                    {
                        originX: 'center',
                        originY: 'bottom',
                        overlayX: 'center',
                        overlayY: 'top',
                        offsetY: 8,
                    },
                ]),
            scrollStrategy: this.overlay.scrollStrategies.close(),
        });
        const portal = new ComponentPortal(CardPopupComponent, this.vcr);

        const componentRef = overlayRef.attach(portal);
        componentRef.setInput('card', this.appCardPopup());
        const subs = [
            componentRef.instance.mouseEnter.subscribe(() => this.onMouseEnter()),
            componentRef.instance.mouseLeave.subscribe(() => this.onMouseLeave()),
        ];
        componentRef.onDestroy(() => {
            for (const sub of subs) {
                sub.unsubscribe();
            }
        });

        this.popupState = popupStateTransitionOpen(this.popupState, overlayRef);
    }

    private closeTooltip() {
        this.popupState = popupStateTransitionClosed(this.popupState);
    }

    private startCloseTooltipTimeout() {
        this.popupState = popupStateTransitionClosing(this.popupState, () => this.closeTooltip());
    }

    private toggleTooltip() {
        if (isPopupStateOpen(this.popupState)) {
            this.closeTooltip();
        } else {
            this.openTooltip();
        }
    }
}
