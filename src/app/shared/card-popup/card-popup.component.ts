import {
    ChangeDetectionStrategy,
    Component,
    computed,
    HostListener,
    inject,
    input,
    OnChanges,
    output,
    SimpleChanges,
} from '@angular/core';
import { PushPipe } from '@ngrx/component';
import { patchState, signalStore, withComputed, withMethods, withState } from '@ngrx/signals';

import { Card, toAllCardFaces } from '../card-api/cards.model';

export interface CardPopupState {
    faces: Card[];
    activeFace: number;
}

const initialState: CardPopupState = {
    faces: [],
    activeFace: -1,
};

const CardPopupStore = signalStore(
    withState(initialState),
    withComputed(({ faces, activeFace }) => ({
        cardImage: computed(() => {
            const fs = faces();
            return {
                image: fs[activeFace()]?.image_uris?.normal,
                isMultiFace: fs.length > 1,
            };
        }),
    })),
    withMethods((store) => ({
        setCard(card: Card | undefined) {
            patchState(store, (state) => {
                if (!card) {
                    return initialState;
                }
                const faces = toAllCardFaces(card).filter((face) => face.image_uris?.normal);
                return {
                    ...state,
                    faces,
                    activeFace: 0,
                };
            });
        },
        toggleFace() {
            patchState(store, (state) => ({
                ...state,
                activeFace: state.activeFace === state.faces.length - 1 ? 0 : state.activeFace + 1,
            }));
        },
    })),
);

@Component({
    selector: 'app-card-popup',
    standalone: true,
    imports: [PushPipe],
    providers: [CardPopupStore],
    templateUrl: './card-popup.component.html',
    styleUrl: './card-popup.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardPopupComponent implements OnChanges {
    private readonly componentStore = inject(CardPopupStore);

    card = input<Card>();

    readonly cardImage = this.componentStore.cardImage;

    mouseEnter = output<void>();

    mouseLeave = output<void>();

    @HostListener('mouseenter')
    onMouseEnter() {
        this.mouseEnter.emit();
    }

    @HostListener('mouseleave')
    onMouseLeave() {
        this.mouseLeave.emit();
    }

    ngOnChanges({ card }: SimpleChanges) {
        if (card) {
            this.componentStore.setCard(card.currentValue as Card | undefined);
        }
    }

    onToggleFace() {
        this.componentStore.toggleFace();
    }
}
