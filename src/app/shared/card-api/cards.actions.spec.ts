import { mkCard } from '@mocks/card.mock';

import { cardsActions } from './cards.actions';

describe('addCard', () => {
    it('should return an action', () => {
        const card = mkCard();

        expect(cardsActions.addCard({ card })).toEqual({
            type: '[Cards] Add card',
            card,
        });
    });
});

describe('addCards', () => {
    it('should return an action', () => {
        const cards = [mkCard(), mkCard(), mkCard()];

        expect(cardsActions.addCards({ cards })).toEqual({
            type: '[Cards] Add cards',
            cards,
        });
    });
});
