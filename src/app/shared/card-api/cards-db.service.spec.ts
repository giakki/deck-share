import { TestBed } from '@angular/core/testing';
import { mkCard } from '@mocks/card.mock';
import { firstValueFrom } from 'rxjs';

import { CardsDb } from './cards-db.service';

describe('CardsDb', () => {
    let service: CardsDb;

    beforeEach(async () => {
        service = TestBed.inject(CardsDb);

        await firstValueFrom(service.clear());
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('get', () => {
        it('should return the requested card', async () => {
            const expected = mkCard();
            await firstValueFrom(service.store([expected]));

            const actual = await firstValueFrom(service.get(expected.name));

            expect(actual).toEqual(expected);
        });

        it('should return undefined, if the card is not found', async () => {
            const actual = await firstValueFrom(service.get('__MISSING__'));

            expect(actual).toBeUndefined();
        });
    });

    describe('getMany', () => {
        it('should return the requested cards', async () => {
            const expected1 = mkCard();
            const expected2 = mkCard();
            await firstValueFrom(service.store([expected1]));
            await firstValueFrom(service.store([expected2]));

            const actual = await firstValueFrom(service.getMany([expected1.name, expected2.name]));

            expect(actual).toEqual([expected1, expected2]);
        });

        it('should return undefined for the missing cards', async () => {
            const expected = mkCard();
            await firstValueFrom(service.store([expected]));

            const actual = await firstValueFrom(service.getMany([expected.name, '__MISSING__']));

            expect(actual).toEqual([expected, undefined]);
        });
    });

    describe('getAll', () => {
        it('should return all the cards', async () => {
            const expected1 = mkCard();
            const expected2 = mkCard();
            await firstValueFrom(service.store([expected1]));
            await firstValueFrom(service.store([expected2]));

            const actual = await firstValueFrom(service.getAll());

            expect(actual).toContain(expected1);
            expect(actual).toContain(expected2);
        });
    });

    describe('store', () => {
        it("should return the stored cards' keys", async () => {
            const expected = mkCard();

            const actual = await firstValueFrom(service.store([expected]));

            expect(actual as string[]).toEqual([expected.name]);
        });
    });

    describe('clear', () => {
        it('should empty the database', async () => {
            await firstValueFrom(service.store([mkCard()]));

            await firstValueFrom(service.clear());

            expect(await firstValueFrom(service.getAll())).toEqual([]);
        });
    });
});
