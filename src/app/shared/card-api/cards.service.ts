import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import splitEvery from 'ramda/es/splitEvery';
import uniqBy from 'ramda/es/uniqBy';
import { catchError, forkJoin, from, map, mergeMap, Observable, of, reduce, switchMap, take, tap, timer } from 'rxjs';

import { cardsActions } from './cards.actions';
import {
    ApiAutocomplete,
    ApiCard,
    apiCardToDTO,
    ApiList,
    Card,
    CardIdentifier,
    CardIdentifierByName,
    Collection,
    mergeCollections,
    mkEmptyCollection,
    toAllCardFaces,
} from './cards.model';
import { selectCardsByIdentifier } from './cards.selectors';

export const BASE_URL = 'https://api.scryfall.com';
export const REQUEST_THROTTLE_MS = 500;

@Injectable({ providedIn: 'root' })
export class CardsService {
    private lastRequestTime = 0;

    constructor(
        private http: HttpClient,
        private store: Store,
    ) {}

    autocomplete(name: string): Observable<string[]> {
        return this.delayed().pipe(
            switchMap(() =>
                this.http.get<ApiAutocomplete>(`${BASE_URL}/cards/autocomplete`, {
                    params: {
                        q: name,
                    },
                }),
            ),
            map((res) => res.data),
        );
    }

    byName(name: string) {
        return this.delayed().pipe(
            switchMap(() =>
                this.http.get<ApiCard>(`${BASE_URL}/cards/named`, {
                    params: {
                        exact: name,
                    },
                }),
            ),
            map(apiCardToDTO),
            tap((card) => this.store.dispatch(cardsActions.addCards({ cards: toAllCardFaces(card) }))),
        );
    }

    collection(identifiers: CardIdentifier[]): Observable<Collection> {
        const chunks = splitEvery(
            75,
            uniqBy((identifier) => identifier.name, identifiers),
        );

        return from(chunks).pipe(
            mergeMap((chunk) =>
                this.store.pipe(
                    select(selectCardsByIdentifier(chunk)),
                    take(1),
                    switchMap(({ cachedCards, cardsNotInCache }) =>
                        this.requestPartialCollection(cachedCards, cardsNotInCache),
                    ),
                ),
            ),
            reduce(mergeCollections, mkEmptyCollection()),
        );
    }

    private delayed() {
        const now = Date.now();
        const delay = Math.max(0, REQUEST_THROTTLE_MS - (now - this.lastRequestTime));

        this.lastRequestTime = now + delay;

        return timer(delay);
    }

    private requestPartialCollection(cachedCards: Card[], cardsNotInCache: CardIdentifierByName[]) {
        if (cardsNotInCache.length === 0) {
            return of({ cards: cachedCards, notFound: [] });
        }

        return this.delayed().pipe(
            switchMap(() =>
                this.http.post<ApiList<ApiCard>>(`${BASE_URL}/cards/collection`, {
                    identifiers: cardsNotInCache,
                }),
            ),
            map((list) => ({ ...list, data: list.data.map(apiCardToDTO) })),
            tap((list) => this.store.dispatch(cardsActions.addCards({ cards: list.data.flatMap(toAllCardFaces) }))),
            map((requested) => ({
                cards: [...cachedCards, ...requested.data.map(apiCardToDTO)],
                notFound: requested.not_found,
            })),
            switchMap((collection) =>
                this.requestPartialCollectionByName(collection.notFound).pipe(
                    map((retry) => mergeCollections(collection, retry)),
                ),
            ),
        );
    }

    private requestPartialCollectionByName(identifiers: CardIdentifier[]): Observable<Collection> {
        if (identifiers.length === 0) {
            return of(mkEmptyCollection());
        }

        const requests = identifiers.map((id) => this.byName(id.name).pipe(catchError((_) => of(id))));

        return forkJoin(requests).pipe(
            map((cards) =>
                cards.reduce((collection, cardOrIdentifier) => {
                    if ('id' in cardOrIdentifier) {
                        collection.cards.push(cardOrIdentifier);
                    } else {
                        collection.notFound.push(cardOrIdentifier);
                    }
                    return collection;
                }, mkEmptyCollection()),
            ),
        );
    }
}
