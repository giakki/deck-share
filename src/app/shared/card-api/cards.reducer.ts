import { createEntityAdapter, Dictionary, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import indexBy from 'ramda/es/indexBy';

import { cardsActions } from './cards.actions';
import { Card } from './cards.model';

export const cardsFeatureKey = 'cards';

export interface State extends EntityState<Card> {
    byName: Dictionary<Card>;
}

export const adapter: EntityAdapter<Card> = createEntityAdapter<Card>();

export const initialState: State = adapter.getInitialState({
    byName: {},
});

export const reducer = createReducer(
    initialState,
    on(cardsActions.addCard, (state, { card }) => {
        const newState = {
            ...adapter.setOne(card, state),
            byName: { ...state.byName, [card.name]: card },
        };

        return newState;
    }),
    on(cardsActions.addCards, cardsActions.initCardsFromDB, (state, { cards }) => {
        const newState = {
            ...adapter.setMany(cards, state),
            byName: {
                ...state.byName,
                ...indexBy((card) => card.name, cards),
            },
        };

        return newState;
    }),
);
