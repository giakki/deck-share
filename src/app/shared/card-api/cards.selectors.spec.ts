import { mkCard } from '@mocks/card.mock';
import { times } from 'ramda';
import indexBy from 'ramda/es/indexBy';
import prop from 'ramda/es/prop';

import { cardsActions } from './cards.actions';
import * as fromCards from './cards.reducer';
import {
    selectAllCards,
    selectAllCardsByName,
    selectCardByName,
    selectCardsByIdentifier,
    selectCardsState,
} from './cards.selectors';

describe('Cards Selectors', () => {
    let state: { [fromCards.cardsFeatureKey]: fromCards.State };
    let featureState: fromCards.State;
    const mockCards = [mkCard(), mkCard(), mkCard()];

    beforeEach(() => {
        featureState = fromCards.reducer(fromCards.initialState, cardsActions.addCards({ cards: mockCards }));
        state = {
            [fromCards.cardsFeatureKey]: featureState,
        };
    });

    it('should select the feature state', () => {
        const result = selectCardsState(state);

        expect(result).toBe(featureState);
    });

    describe('selectAllCards', () => {
        it('should select all the cards', () => {
            const result = selectAllCards(state);

            expect(result).toEqual(mockCards);
        });
    });

    describe('selectAllCardsByName', () => {
        it('should select all cards by name', () => {
            const result = selectAllCardsByName(state);

            expect(result).toEqual(indexBy(prop('name'), mockCards));
        });
    });

    describe('selectCardsByIdentifier', () => {
        it('should select all cards by name', () => {
            const cardsNotInCache = times(() => ({ name: crypto.randomUUID() }), 3);
            const cachedCards = [mockCards[0], mockCards[2]];
            const selector = selectCardsByIdentifier([
                ...cachedCards.map(({ name }) => ({ name })),
                ...cardsNotInCache,
            ]);

            const result = selector(state);

            expect(result).toEqual({
                cachedCards,
                cardsNotInCache,
            });
        });
    });

    describe('selectCardByName', () => {
        it('should select a card by name', () => {
            const result = selectCardByName(mockCards[1].name)(state);

            expect(result).toEqual(mockCards[1]);
        });

        it('should select a card by name and return undefined if not found', () => {
            const result = selectCardByName('')(state);

            expect(result).toBeUndefined();
        });
    });
});
