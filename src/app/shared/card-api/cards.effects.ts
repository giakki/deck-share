import { inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects, provideEffects } from '@ngrx/effects';
import { catchError, concatMap, map, of, switchMap } from 'rxjs';

import { cardsActions } from './cards.actions';
import { CardsDb } from './cards-db.service';

export const addCards$ = createEffect(
    (actions$ = inject(Actions), cardsDb = inject(CardsDb)) => {
        return actions$.pipe(
            ofType(cardsActions.addCard, cardsActions.addCards),
            map((action) => (action.type === '[Cards] Add card' ? [action.card] : action.cards)),
            concatMap((cards) => cardsDb.store(cards).pipe(catchError(() => of()))),
        );
    },
    { dispatch: false, functional: true },
);

@Injectable()
export class CardsEffects implements OnInitEffects {
    constructor(
        private actions$: Actions,
        private cardsDb: CardsDb,
    ) {}

    init$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(cardsActions.init),
            switchMap(() =>
                this.cardsDb.getAll().pipe(
                    map((cards) => cardsActions.initCardsFromDB({ cards })),
                    catchError(() => of(cardsActions.initCardsFromDB({ cards: [] }))),
                ),
            ),
        );
    });

    ngrxOnInitEffects() {
        return cardsActions.init();
    }
}

export const provideCardsEffects = () => provideEffects({ addCards$ }, CardsEffects);
