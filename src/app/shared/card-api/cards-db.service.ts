import { Injectable } from '@angular/core';
import { forkJoin, map, Observable, switchMap, toArray } from 'rxjs';

import * as IDb from '../../utils/indexed-db';
import { ApiCard } from './cards.model';

export const CARDS_DB_NAME = 'cards';
export const CARDS_DB_SCHEMA = { stores: [{ name: 'collection', params: { keyPath: 'name' } }] };

@Injectable({ providedIn: 'root' })
export class CardsDb {
    private db$?: Observable<IDBDatabase>;

    clear() {
        return this.open$().pipe(
            switchMap((db) => {
                db.close();
                this.db$ = undefined;
                return IDb.deleteDatabase$(CARDS_DB_NAME);
            }),
        );
    }

    get(name: string) {
        return this.open$().pipe(
            switchMap((db) => IDb.transaction$(db, 'collection')),
            map((tx) => tx.objectStore('collection')),
            switchMap((store) => IDb.get$<ApiCard>(store, name)),
        );
    }

    getAll() {
        return this.open$().pipe(
            switchMap((db) => IDb.transaction$(db, 'collection')),
            map((tx) => tx.objectStore('collection')),
            switchMap((store) => IDb.getAll$<ApiCard>(store)),
        );
    }

    getMany(names: string[]) {
        return this.open$().pipe(
            switchMap((db) => IDb.transaction$(db, 'collection')),
            switchMap((tx) => {
                const store = tx.objectStore('collection');

                return IDb.getMany$<ApiCard>(tx, store, names);
            }),
            toArray(),
        );
    }

    store(cards: ApiCard[]) {
        return this.open$().pipe(
            switchMap((db) => IDb.transaction$(db, 'collection', 'readwrite')),
            map((tx) => tx.objectStore('collection')),
            switchMap((store) => forkJoin(cards.map((card) => IDb.put$(store, card)))),
        );
    }

    private open$() {
        if (this.db$) {
            return this.db$;
        }

        this.db$ = IDb.open$(CARDS_DB_NAME, CARDS_DB_SCHEMA);
        return this.db$;
    }
}
