import { mkCard } from '@mocks/card.mock';
import { Action } from '@ngrx/store';
import clone from 'ramda/es/clone';

import { cardsActions } from './cards.actions';
import * as fromCards from './cards.reducer';
import { initialState as originalInitialState, reducer } from './cards.reducer';

describe('Cards Reducer', () => {
    let initialState: fromCards.State;
    let initialStateForCheck: fromCards.State;

    beforeEach(() => {
        initialState = clone(originalInitialState);
        initialStateForCheck = clone(originalInitialState);
    });

    afterEach(() => {
        expect(initialState).toEqual(initialStateForCheck);
    });

    describe('an unknown action', () => {
        it('should return the previous state', () => {
            const action = {} as Action;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        });
    });

    describe('addCard action', () => {
        it('should add a card to the state', () => {
            const card = mkCard();
            const action = cardsActions.addCard({ card });

            const result = reducer(initialState, action);

            expect(result).toEqual({
                byName: {
                    [card.name]: card,
                },
                entities: {
                    [card.id]: card,
                },
                ids: [card.id],
            });
        });

        it('should update a card to the state if the id is already present', () => {
            const card1 = mkCard();
            const card2 = mkCard({ id: card1.id, scryfall_uri: crypto.randomUUID() });
            const action1 = cardsActions.addCard({ card: card1 });
            const action2 = cardsActions.addCard({ card: card2 });

            const result = reducer(reducer(initialState, action1), action2);

            expect(result).toEqual({
                byName: {
                    [card1.name]: card1,
                    [card2.name]: card2,
                },
                entities: {
                    [card1.id]: card2,
                },
                ids: [card1.id],
            });
        });
    });

    describe('addCards action', () => {
        it('should add the cards to the state', () => {
            const cards = [mkCard(), mkCard()];
            const action = cardsActions.addCards({ cards });

            const result = reducer(initialState, action);

            expect(result).toEqual({
                byName: {
                    [cards[0].name]: cards[0],
                    [cards[1].name]: cards[1],
                },
                entities: {
                    [cards[0].id]: cards[0],
                    [cards[1].id]: cards[1],
                },
                ids: [cards[0].id, cards[1].id],
            });
        });

        it('should update a card to the state if the id is already present', () => {
            const cards1 = [mkCard(), mkCard()];
            const cards2 = [mkCard(), mkCard({ id: cards1[1].id, scryfall_uri: crypto.randomUUID() })];
            const action1 = cardsActions.addCards({ cards: cards1 });
            const action2 = cardsActions.addCards({ cards: cards2 });

            const result = reducer(reducer(initialState, action1), action2);

            expect(result).toEqual({
                byName: {
                    [cards1[0].name]: cards1[0],
                    [cards1[1].name]: cards1[1],
                    [cards2[0].name]: cards2[0],
                    [cards2[1].name]: cards2[1],
                },
                entities: {
                    [cards1[0].id]: cards1[0],
                    [cards1[1].id]: cards2[1],
                    [cards2[0].id]: cards2[0],
                },
                ids: [cards1[0].id, cards1[1].id, cards2[0].id],
            });
        });
    });
});
