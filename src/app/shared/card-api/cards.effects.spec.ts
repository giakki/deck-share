import { TestBed } from '@angular/core/testing';
import { mkCard } from '@mocks/card.mock';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { Observable, of, throwError } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { cardsActions } from './cards.actions';
import { addCards$, CardsEffects, provideCardsEffects } from './cards.effects';
import { CardsDb } from './cards-db.service';

describe('CardsEffects', () => {
    let actions$: Observable<Action>;
    let effects: CardsEffects;
    let testScheduler: TestScheduler;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CardsEffects, provideMockActions(() => actions$)],
        });

        effects = TestBed.inject(CardsEffects);

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('provideCardsEffects', () => {
        it('should not throw', () => {
            expect(() => provideCardsEffects()).not.toThrow();
        });
    });

    describe('init$', () => {
        it('should call the db service', () => {
            const expected1 = [mkCard()];
            const expected2 = [mkCard()];
            const spy = spyOn(TestBed.inject(CardsDb), 'getAll').and.returnValues(of([]), of(expected1), of(expected2));

            testScheduler.run(({ hot, flush }) => {
                actions$ = hot('-a-aa', {
                    a: cardsActions.init(),
                });

                effects.init$.subscribe();
                flush();

                expect(spy).toHaveBeenCalledTimes(3);
            });
        });

        it('should produce the expected actions', () => {
            const expected1 = [mkCard()];
            const expected2 = [mkCard()];
            spyOn(TestBed.inject(CardsDb), 'getAll').and.returnValues(of([]), of(expected1), of(expected2));

            testScheduler.run(({ hot, expectObservable }) => {
                actions$ = hot('-a-aa', {
                    a: cardsActions.init(),
                });

                expectObservable(effects.init$).toBe('-a-bc', {
                    a: cardsActions.initCardsFromDB({ cards: [] }),
                    b: cardsActions.initCardsFromDB({ cards: expected1 }),
                    c: cardsActions.initCardsFromDB({ cards: expected2 }),
                });
            });
        });

        it('should handle errors', () => {
            const expected = [mkCard()];
            spyOn(TestBed.inject(CardsDb), 'getAll').and.returnValues(
                of([]),
                throwError(() => of(new Error())),
                of(expected),
            );

            testScheduler.run(({ hot, expectObservable }) => {
                actions$ = hot('-a-aa', {
                    a: cardsActions.init(),
                });

                expectObservable(effects.init$).toBe('-a-bc', {
                    a: cardsActions.initCardsFromDB({ cards: [] }),
                    b: cardsActions.initCardsFromDB({ cards: [] }),
                    c: cardsActions.initCardsFromDB({ cards: expected }),
                });
            });
        });
    });

    describe('addCards$', () => {
        it('should call the db service', () => {
            const expected1 = [mkCard()];
            const expected2 = [mkCard()];
            const spy = spyOn(TestBed.inject(CardsDb), 'store').and.returnValue(of([]));

            testScheduler.run(({ hot, flush }) => {
                actions$ = hot('-a-b', {
                    a: cardsActions.addCard({ card: expected1[0] }),
                    b: cardsActions.addCards({ cards: expected2 }),
                });

                TestBed.runInInjectionContext(() => addCards$().subscribe());
                flush();

                expect(spy).toHaveBeenCalledTimes(2);
                expect(spy.calls.all()[0].args).toEqual([expected1]);
                expect(spy.calls.all()[1].args).toEqual([expected2]);
            });
        });

        it('should handle errors', () => {
            const expected1 = [mkCard()];
            const expected2 = [mkCard()];
            const spy = spyOn(TestBed.inject(CardsDb), 'store').and.returnValues(
                throwError(() => new Error()),
                of([]),
            );

            testScheduler.run(({ hot, flush }) => {
                actions$ = hot('-a-b', {
                    a: cardsActions.addCard({ card: expected1[0] }),
                    b: cardsActions.addCards({ cards: expected2 }),
                });

                TestBed.runInInjectionContext(() => addCards$().subscribe());
                flush();

                expect(spy).toHaveBeenCalledTimes(2);
                expect(spy.calls.all()[0].args).toEqual([expected1]);
                expect(spy.calls.all()[1].args).toEqual([expected2]);
            });
        });
    });

    describe('onInitEffects', () => {
        it('should produce the expected action', () => {
            expect(effects.ngrxOnInitEffects()).toEqual(cardsActions.init());
        });
    });
});
