import { isNotUndefined } from '@utils/fn';
import { clone, prop, uniq, uniqBy } from 'ramda';

export interface ApiList<T> {
    data: T[];
    not_found: CardIdentifier[];
}

export interface ApiAutocomplete {
    data: string[];
}

export type CardColorAbbrev = 'W' | 'U' | 'B' | 'R' | 'G';

export const CARD_COLORS = ['white', 'blue', 'black', 'red', 'green'] as const;
export type CardColor = (typeof CARD_COLORS)[number];

export const CARD_TYPES = [
    'land',
    'creature',
    'artifact',
    'enchantment',
    'planeswalker',
    'instant',
    'sorcery',
] as const;
export type CardType = (typeof CARD_TYPES)[number];

export interface ApiCardFace {
    colors?: CardColorAbbrev[];
    name?: string;
    image_uris?: {
        art_crop?: string;
        normal?: string;
    };
    type_line: string;
}

export interface ApiCard {
    id: string;
    scryfall_uri: string;
    name: string;
    image_uris?: {
        art_crop?: string;
        normal?: string;
    };
    colors?: CardColorAbbrev[];
    card_faces?: ApiCardFace[];
    type_line: string;
}

export type Card = ApiCard;

export function apiCardToDTO(apiCard: ApiCard): Card {
    const card = {
        id: apiCard.id,
        scryfall_uri: apiCard.scryfall_uri,
        name: apiCard.name,
        image_uris: {
            art_crop: apiCard.image_uris?.art_crop,
            normal: apiCard.image_uris?.normal,
        },
        colors: apiCard.colors,
        card_faces: apiCard.card_faces?.map((face) => ({
            colors: face.colors,
            name: face.name,
            image_uris: {
                art_crop: face.image_uris?.art_crop,
                normal: face.image_uris?.normal,
            },
            type_line: face.type_line,
        })),
        type_line: apiCard.type_line,
    };

    if (!card.image_uris.art_crop) {
        card.image_uris.art_crop = card.card_faces?.[0].image_uris?.art_crop;
    }
    if (!card.image_uris.normal) {
        card.image_uris.normal = card.card_faces?.[0].image_uris?.normal;
    }

    return card;
}

export function cardColors(card: Card) {
    const baseColors = card.colors ?? [];
    const facesColors = card.card_faces?.flatMap((face) => face.colors).filter(isNotUndefined) ?? [];

    return uniq([...baseColors, ...facesColors]).map(colorAbbrevToColor);
}

function cardTypesFromTypeLine(str: string) {
    const types: CardType[] = [];

    const typeLine = str.toLocaleLowerCase();
    if (typeLine.includes('land')) {
        types.push('land');
    }
    if (typeLine.includes('creature')) {
        types.push('creature');
    }
    if (typeLine.includes('artifact')) {
        types.push('artifact');
    }
    if (typeLine.includes('enchantment')) {
        types.push('enchantment');
    }
    if (typeLine.includes('planeswalker')) {
        types.push('planeswalker');
    }
    if (typeLine.includes('instant')) {
        types.push('instant');
    }
    if (typeLine.includes('sorcery')) {
        types.push('sorcery');
    }

    return types;
}
export function cardTypes(card: Card) {
    const facesTypes =
        card.card_faces?.flatMap((face) => cardTypesFromTypeLine(face.type_line)).filter(isNotUndefined) ?? [];

    return uniq([...cardTypesFromTypeLine(card.type_line), ...facesTypes]);
}

export function colorAbbrevToColor(abbrev: CardColorAbbrev): CardColor {
    switch (abbrev) {
        case 'W':
            return 'white';
        case 'U':
            return 'blue';
        case 'B':
            return 'black';
        case 'R':
            return 'red';
        case 'G':
            return 'green';
    }
}

export function isBasicLand(card: Card) {
    return card.type_line.toLocaleLowerCase().includes('basic land');
}

export function toAllCardFaces(card: Card) {
    const faces = [card];

    for (const face of card.card_faces ?? []) {
        if (face.name) {
            const c = clone(card);
            c.colors = face.colors;
            c.image_uris = face.image_uris;
            c.name = face.name;
            c.type_line = face.type_line;
            faces.push(c);
        }
    }

    return faces;
}

export interface CardIdentifierByName {
    name: string;
}

export type CardIdentifier = CardIdentifierByName;

export class IncompleteCollectionError extends Error {
    constructor(readonly identifiers: CardIdentifier[]) {
        super();
        Object.setPrototypeOf(this, IncompleteCollectionError.prototype);
    }
}

export interface Collection {
    cards: Card[];
    notFound: CardIdentifier[];
}

export function mkEmptyCollection(): Collection {
    return {
        cards: [],
        notFound: [],
    };
}

export function collectionHasCardByName(collection: Collection, name: string) {
    return collection.cards.some((card) => card.name === name);
}

export function mergeCollections(a: Collection, b: Collection): Collection {
    return {
        cards: uniqBy(prop('id'), [...a.cards, ...b.cards]),
        notFound: uniqBy(
            prop('name'),
            [...a.notFound, ...b.notFound].filter(
                (id) => !collectionHasCardByName(a, id.name) && !collectionHasCardByName(b, id.name),
            ),
        ),
    };
}
