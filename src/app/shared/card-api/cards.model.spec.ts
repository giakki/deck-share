import { mkCard } from '@mocks/card.mock';

import {
    apiCardToDTO,
    CARD_COLORS,
    cardColors,
    cardTypes,
    colorAbbrevToColor,
    IncompleteCollectionError,
    isBasicLand,
    mergeCollections,
    mkEmptyCollection,
    toAllCardFaces,
} from './cards.model';

describe('Cards models', () => {
    describe('apiCardToDTO', () => {
        it('should copy all the required fields', () => {
            const card = mkCard();

            expect(apiCardToDTO(card)).toEqual(card);
        });

        it('should copy optional fields', () => {
            const card = mkCard({
                card_faces: [
                    {
                        colors: ['W'],
                        name: crypto.randomUUID(),
                        image_uris: {
                            art_crop: crypto.randomUUID(),
                            normal: crypto.randomUUID(),
                        },
                        type_line: crypto.randomUUID(),
                    },
                ],
                colors: ['U', 'B'],
            });

            expect(apiCardToDTO(card)).toEqual(card);
        });

        it('should not copy extra fields', () => {
            const card = mkCard();
            (card as any).__oops__ = crypto.randomUUID();

            expect('__oops__' in apiCardToDTO(card)).toBeFalse();
        });

        it("should default to using the first face's image for double-face cards (missing images)", () => {
            const card = mkCard({
                card_faces: [
                    {
                        image_uris: {
                            art_crop: crypto.randomUUID(),
                            normal: crypto.randomUUID(),
                        },
                        type_line: crypto.randomUUID(),
                    },
                    {
                        image_uris: {
                            art_crop: crypto.randomUUID(),
                            normal: crypto.randomUUID(),
                        },
                        type_line: crypto.randomUUID(),
                    },
                ],
                image_uris: {
                    art_crop: undefined,
                    normal: undefined,
                },
            });

            const result = apiCardToDTO(card);

            expect(result.image_uris).toEqual(card.card_faces?.[0].image_uris);
        });
    });

    describe('cardColors', () => {
        it('should handle cards without colors', () => {
            expect(cardColors(mkCard({ colors: undefined }))).toEqual([]);
        });

        it('should handle simple cards', () => {
            expect(cardColors(mkCard({ colors: ['R'] }))).toEqual(['red']);
        });

        it('should handle cards with multiple faces', () => {
            expect(cardColors(mkCard({ colors: ['R'], card_faces: [{ colors: ['G'], type_line: '' }] }))).toEqual([
                'red',
                'green',
            ]);
        });

        it('should handle duplicated colors', () => {
            expect(
                cardColors(
                    mkCard({
                        card_faces: [
                            { colors: ['G'], type_line: '' },
                            { colors: ['G'], type_line: '' },
                        ],
                    }),
                ),
            ).toEqual(['green']);
        });
    });

    describe('cardTypes', () => {
        it('should handle cards without type', () => {
            expect(cardTypes(mkCard({ type_line: crypto.randomUUID() }))).toEqual([]);
        });

        it('should handle lands', () => {
            expect(cardTypes(mkCard({ type_line: 'Basic Land' }))).toEqual(['land']);
        });

        it('should handle creatures', () => {
            expect(cardTypes(mkCard({ type_line: 'Creature - Goblin' }))).toEqual(['creature']);
        });

        it('should handle artifacts', () => {
            expect(cardTypes(mkCard({ type_line: 'Artifact - Equipment' }))).toEqual(['artifact']);
        });

        it('should handle enchantements', () => {
            expect(cardTypes(mkCard({ type_line: 'Enchantment - Aura Curse' }))).toEqual(['enchantment']);
        });

        it('should handle planeswalkers', () => {
            expect(cardTypes(mkCard({ type_line: 'Legendary Planeswalker' }))).toEqual(['planeswalker']);
        });

        it('should handle instants', () => {
            expect(cardTypes(mkCard({ type_line: 'Tribal Instant' }))).toEqual(['instant']);
        });

        it('should handle sorceries', () => {
            expect(cardTypes(mkCard({ type_line: 'Legendary Sorcery' }))).toEqual(['sorcery']);
        });

        it('should handle multiple types at once', () => {
            expect(cardTypes(mkCard({ type_line: "Enchantment Land - Urza's Saga" }))).toEqual(['land', 'enchantment']);
        });

        it('should handle cards with multiple faces', () => {
            expect(cardTypes(mkCard({ type_line: 'Land', card_faces: [{ type_line: 'Sorcery' }] }))).toEqual([
                'land',
                'sorcery',
            ]);
        });

        it('should handle duplicated colors', () => {
            expect(cardTypes(mkCard({ type_line: 'Instant', card_faces: [{ type_line: 'Instant' }] }))).toEqual([
                'instant',
            ]);
        });
    });

    describe('colorAbbrevToColor', () => {
        it('should produce the expected results', () => {
            expect((['W', 'U', 'B', 'R', 'G'] as const).map(colorAbbrevToColor)).toEqual(CARD_COLORS);
        });
    });

    describe('isBasicLand', () => {
        it('should produce the expected result', () => {
            const card = mkCard({ type_line: 'Basic Land - Plains' });

            expect(isBasicLand(card)).toEqual(true);
        });

        it('should produce the expected result', () => {
            const card = mkCard({ type_line: 'Land Creature - Forest Dryad' });

            expect(isBasicLand(card)).toEqual(false);
        });
    });

    describe('IncompleteCollectionError', () => {
        it('should create', () => {
            const expected = [{ name: crypto.randomUUID() }];

            const error = new IncompleteCollectionError(expected);

            expect(error.identifiers).toEqual(expected);
        });
    });

    describe('mkEmptyCollection', () => {
        it('should produce an empty collection', () => {
            expect(mkEmptyCollection()).toEqual({
                cards: [],
                notFound: [],
            });
        });
    });

    describe('toAllCardFaces', () => {
        it('should copy card faces if present', () => {
            const card = mkCard({
                card_faces: [
                    {
                        colors: ['B', 'G', 'R'],
                        name: crypto.randomUUID(),
                        image_uris: { normal: crypto.randomUUID() },
                        type_line: crypto.randomUUID(),
                    },
                ],
            });

            expect(toAllCardFaces(card)).toEqual([
                card,
                {
                    ...card,
                    name: card.card_faces?.[0].name ?? '__ERR__',
                    colors: card.card_faces?.[0].colors,
                    image_uris: card.card_faces?.[0].image_uris,
                    type_line: card.card_faces?.[0].type_line ?? '__ERR__',
                },
            ]);
        });

        it('should not copy card faces if not present', () => {
            const card = mkCard();

            expect(toAllCardFaces(card)).toEqual([card]);
        });

        it('should not copy card faces without name', () => {
            const card = mkCard({
                card_faces: [{ colors: [], type_line: '' }],
            });

            expect(toAllCardFaces(card)).toEqual([card]);
        });
    });

    describe('mergeCollections', () => {
        it('should produce the correct result', () => {
            const expected = {
                cards: [mkCard(), mkCard(), mkCard(), mkCard()],
                notFound: [{ name: crypto.randomUUID() }, { name: crypto.randomUUID() }],
            };
            expect(
                mergeCollections(
                    {
                        cards: [expected.cards[0], expected.cards[1], expected.cards[2]],
                        notFound: [expected.notFound[0]],
                    },
                    {
                        cards: [expected.cards[2], expected.cards[3]],
                        notFound: [expected.notFound[0], expected.notFound[1]],
                    },
                ),
            ).toEqual(expected);
        });
    });
});
