import { createFeatureSelector, createSelector } from '@ngrx/store';

import { Card, CardIdentifier } from './cards.model';
import * as fromCards from './cards.reducer';

const cardEntitySelectors = fromCards.adapter.getSelectors();

export const selectCardsState = createFeatureSelector<fromCards.State>(fromCards.cardsFeatureKey);

export const selectAllCards = createSelector(selectCardsState, cardEntitySelectors.selectAll);

export const selectAllCardsByName = createSelector(selectCardsState, (cards) => cards.byName);

export const selectCardsByIdentifier = (identifiers: CardIdentifier[]) =>
    createSelector(selectAllCardsByName, (cardsByName) => {
        const cachedCards: Card[] = [];
        const cardsNotInCache: CardIdentifier[] = [];
        for (const id of identifiers) {
            if (id.name) {
                const cached = cardsByName[id.name];
                if (cached) {
                    cachedCards.push(cached);
                } else {
                    cardsNotInCache.push(id);
                }
            }
        }

        return {
            cachedCards,
            cardsNotInCache,
        };
    });

export const selectCardByName = (name: string) => createSelector(selectCardsState, (cards) => cards.byName[name]);
