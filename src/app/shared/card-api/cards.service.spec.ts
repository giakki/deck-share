import { HttpErrorResponse, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mkCard } from '@mocks/card.mock';
import { mkApiCollection, mkApiCollectionAllFound } from '@mocks/collection.mock';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import indexBy from 'ramda/es/indexBy';
import prop from 'ramda/es/prop';
import splitEvery from 'ramda/es/splitEvery';
import times from 'ramda/es/times';
import { BehaviorSubject, firstValueFrom } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import apiResponseError from '../../../tests/mocks/api/collection-error.json';
import apiResponseOkAllFound from '../../../tests/mocks/api/collection-ok-all-found.json';
import apiResponseOkWithMissing from '../../../tests/mocks/api/collection-ok-with-missing.json';
import { ApiAutocomplete, apiCardToDTO } from './cards.model';
import { selectAllCardsByName } from './cards.selectors';
import { CardsService, REQUEST_THROTTLE_MS } from './cards.service';

function mkMockData(numCards: number, numNotFound?: number) {
    const cards = times(() => mkCard(), numCards);
    const apiResponse = mkApiCollection({
        data: cards.slice(0, numNotFound),
        not_found: [],
    });
    return {
        apiResponse,
        cardIdentifiers: cards.map((card) => ({ name: card.name })),
        cards,
        cardsByName: indexBy(prop('name'), cards.slice(numNotFound)),
    };
}

describe('CardsService', () => {
    let service: CardsService;
    let mockStore: MockStore;
    let http: HttpTestingController;
    let testScheduler: TestScheduler;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [provideMockStore(), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()],
        });

        service = TestBed.inject(CardsService);
        mockStore = TestBed.inject(MockStore);
        http = TestBed.inject(HttpTestingController);
        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    afterEach(() => {
        mockStore.resetSelectors();
        http.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('autocomplete', () => {
        it('should return the list of names', fakeAsync(() => {
            const name = crypto.randomUUID();
            const result = new BehaviorSubject<string[] | undefined>(undefined);
            const apiResponse: ApiAutocomplete = { data: times(() => crypto.randomUUID(), 3) };

            service.autocomplete(name).subscribe(result);
            tick(0);

            const req = http.expectOne(`https://api.scryfall.com/cards/autocomplete?q=${name}`);
            req.flush(apiResponse);

            expect(result.value).toEqual(apiResponse.data);
        }));
    });

    describe('collection', () => {
        it('should try and retrieve the cards from cache', () => {
            const { cardIdentifiers, cards, cardsByName } = mkMockData(3);

            testScheduler.run(({ expectObservable }) => {
                mockStore.overrideSelector(selectAllCardsByName, cardsByName);

                expectObservable(service.collection(cardIdentifiers)).toBe('(a|)', {
                    a: { cards, notFound: [] },
                });
            });
        });

        it('should ask the api for the cards not in cache', fakeAsync(() => {
            const { apiResponse, cardIdentifiers, cards, cardsByName } = mkMockData(3, 3);

            mockStore.overrideSelector(selectAllCardsByName, cardsByName);

            service.collection(cardIdentifiers).subscribe({
                next(collection) {
                    expect(collection).toEqual({ cards: cards, notFound: [] });
                },
                error(err) {
                    fail(err);
                },
            });

            tick(0);

            const req = http.expectOne('https://api.scryfall.com/cards/collection');
            req.flush(apiResponse);
        }));

        it('should call the correct endpoint', fakeAsync(() => {
            const apiResponse = mkApiCollectionAllFound();
            const cards = apiResponse.data;
            const namesToRequest = cards.map((card) => ({ name: card.name }));
            const requestCardNames = apiResponse.data.map((card) => ({ name: card.name }));

            mockStore.overrideSelector(selectAllCardsByName, indexBy(prop('name'), []));

            service.collection(namesToRequest).subscribe({
                next(collection) {
                    expect(collection).withContext('Wrong result').toEqual({ cards: cards, notFound: [] });
                },
                error(err) {
                    fail(err);
                },
            });

            tick(0);

            const req = http.expectOne('https://api.scryfall.com/cards/collection');
            req.flush(apiResponse);

            expect(req.request.method).withContext('Wrong method').toEqual('POST');
            expect(req.request.body).withContext('Wrong body').toEqual({ identifiers: requestCardNames });
        }));

        describe('chunks', () => {
            it('should request at least one card to the api', () => {
                testScheduler.run(({ expectObservable }) => {
                    expectObservable(service.collection([])).toBe('(a|)', { a: { cards: [], notFound: [] } });
                });
            });

            it('should request at most 75 cards at a time', fakeAsync(() => {
                const numCards = 256;
                const expectedNumRequests = Math.ceil(numCards / 75);
                const cards = times(() => mkCard(), numCards);
                const apiResponses = times(
                    () => mkApiCollectionAllFound({ data: cards.slice(0, 75) }),
                    expectedNumRequests,
                );
                const namesToRequest = cards.map((card) => ({ name: card.name }));

                mockStore.overrideSelector(selectAllCardsByName, {});

                service.collection(namesToRequest).subscribe();

                tick(expectedNumRequests * REQUEST_THROTTLE_MS);

                const requests = http.match((req) => req.url == 'https://api.scryfall.com/cards/collection');
                expect(requests.length).toEqual(expectedNumRequests);

                for (const [i, req] of requests.entries()) {
                    expect(req.request.method).withContext(`Wrong method ${i}`).toEqual('POST');
                    expect(req.request.body)
                        .withContext(`Wrong body ${i}`)
                        .toEqual({ identifiers: namesToRequest.slice(i * 75, (i + 1) * 75) });
                    req.flush(apiResponses[i]);
                }
            }));

            it(`should send a request every ${REQUEST_THROTTLE_MS}ms`, fakeAsync(() => {
                const numCards = 256;
                const expectedNumRequests = Math.ceil(numCards / 75);
                const cards = times(() => mkCard(), numCards);
                const apiResponses = splitEvery(75, cards).map((chunk) => mkApiCollectionAllFound({ data: chunk }));
                const namesToRequest = cards.map((card) => ({ name: card.name }));

                mockStore.overrideSelector(selectAllCardsByName, {});

                const collectionPromise = firstValueFrom(service.collection(namesToRequest));

                tick(0);
                for (let i = 0; i < expectedNumRequests; ++i) {
                    const req = http.expectOne('https://api.scryfall.com/cards/collection');
                    req.flush(apiResponses[i]);
                    tick(REQUEST_THROTTLE_MS);
                }

                return collectionPromise.then((collection) => expect(collection.cards).toEqual(cards));
            }));
        });

        describe('integration', () => {
            it('should correctly process an API response where all cards are found.', fakeAsync(async () => {
                const cardIdentifiers = apiResponseOkAllFound.data.map((card) => ({ name: card.name }));

                mockStore.overrideSelector(selectAllCardsByName, {});

                const collectionPromise = firstValueFrom(service.collection(cardIdentifiers));
                tick(0);

                const req = http.expectOne('https://api.scryfall.com/cards/collection');
                req.flush(apiResponseOkAllFound);

                const collection = await collectionPromise;
                expect(collection).toEqual({ cards: apiResponseOkAllFound.data.map(apiCardToDTO), notFound: [] });
            }));

            it('should correctly process an API response where some cards are not found in collection, but are found by name.', fakeAsync(async () => {
                const cardIdentifiers = apiResponseOkAllFound.data.map((card) => ({ name: card.name }));
                const missingCard = mkCard({ name: apiResponseOkWithMissing.not_found[0].name });

                mockStore.overrideSelector(selectAllCardsByName, {});

                const collectionPromise = firstValueFrom(service.collection(cardIdentifiers));
                tick(0);

                let req = http.expectOne('https://api.scryfall.com/cards/collection');
                req.flush(apiResponseOkWithMissing);
                tick(REQUEST_THROTTLE_MS);

                req = http.expectOne(`https://api.scryfall.com/cards/named?exact=${missingCard.name}`);
                req.flush(missingCard);

                const collection = await collectionPromise;
                expect(collection).toEqual({
                    cards: [...apiResponseOkWithMissing.data, missingCard].map(apiCardToDTO),
                    notFound: [],
                });
            }));

            it('should correctly process an API response where some cards are not found neither in collection nor by name.', fakeAsync(async () => {
                const cardIdentifiers = apiResponseOkAllFound.data.map((card) => ({ name: card.name }));
                const expectedError = { status: 404, statusText: 'Not found' };
                const missingCard = mkCard({ name: apiResponseOkWithMissing.not_found[0].name });

                mockStore.overrideSelector(selectAllCardsByName, {});

                const collectionPromise = firstValueFrom(service.collection(cardIdentifiers));
                tick(0);

                let req = http.expectOne('https://api.scryfall.com/cards/collection');
                req.flush(apiResponseOkWithMissing);
                tick(REQUEST_THROTTLE_MS);

                req = http.expectOne(`https://api.scryfall.com/cards/named?exact=${missingCard.name}`);
                req.flush(apiResponseError, expectedError);

                const collection = await collectionPromise;
                expect(collection).toEqual({
                    cards: apiResponseOkWithMissing.data.map(apiCardToDTO),
                    notFound: apiResponseOkWithMissing.not_found,
                });
            }));

            it('should correctly process an API response error.', fakeAsync(() => {
                const expectedError = { status: 500, statusText: 'Internal Server Error' };
                const cardIdentifiers = apiResponseOkAllFound.data.map((card) => ({ name: card.name }));

                mockStore.overrideSelector(selectAllCardsByName, {});

                const collectionPromise = firstValueFrom(service.collection(cardIdentifiers));
                tick(0);

                const req = http.expectOne('https://api.scryfall.com/cards/collection');
                req.flush(apiResponseError, expectedError);

                return collectionPromise.then(fail).catch((error) => {
                    expect(error).toEqual(
                        new HttpErrorResponse({
                            ...expectedError,
                            error: apiResponseError,
                            url: 'https://api.scryfall.com/cards/collection',
                        }),
                    );
                });
            }));
        });
    });
});
