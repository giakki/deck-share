import { createActionGroup, emptyProps, props } from '@ngrx/store';

import { Card } from './cards.model';

export const cardsActions = createActionGroup({
    source: 'Cards',
    events: {
        Init: emptyProps(),
        'Init cards from DB': props<{ cards: Card[] }>(),
        'Add card': props<{ card: Card }>(),
        'Add cards': props<{ cards: Card[] }>(),
    },
});
