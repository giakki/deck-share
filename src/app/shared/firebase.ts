import { authState, GoogleAuthProvider } from '@angular/fire/auth';
import { collection } from '@angular/fire/firestore';
import { traceUntilFirst } from '@angular/fire/performance';
import { signInWithPopup, signOut } from '@firebase/auth';
import { doc, getDocs, setDoc } from '@firebase/firestore';

export default {
    auth: {
        authState,
        signInWithPopup,
        signOut,
        GoogleAuthProvider,
    },
    firestore: {
        collection,
        doc,
        getDocs,
        setDoc,
    },
    performance: {
        traceUntilFirst,
    },
};
