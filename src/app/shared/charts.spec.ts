import { mkPieChartData, toPieChartData } from './charts';

describe('charts', () => {
    describe('mkPieChartData', () => {
        it('should produce empty chart data', () => {
            expect(mkPieChartData()).toEqual({
                data: [],
                labels: [],
            });
        });
    });

    describe('toPieChartData', () => {
        const SORT_MAP = {
            a: 1,
            b: 2,
        };

        it('should produce the expected result', () => {
            const data = { a: 1, b: 2 };
            expect(toPieChartData(data, SORT_MAP)).toEqual({
                data: [1, 2],
                labels: ['a', 'b'],
            });
        });
    });
});
