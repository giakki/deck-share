export interface PieChartData<L, D> {
    labels: L[];
    data: D[];
}

export function mkPieChartData<L, D>(): PieChartData<L, D> {
    return {
        data: [],
        labels: [],
    };
}

export function toPieChartData<Datum, R extends Record<string, Datum>, Label extends keyof R>(
    record: R,
    sortMap: Record<Label, number>,
): PieChartData<Label, Datum> {
    const labels = (Object.keys(record) as Label[]).sort((a, b) => sortMap[a] - sortMap[b]);
    const data = labels.map((label) => record[label]);

    return {
        labels,
        data,
    };
}
