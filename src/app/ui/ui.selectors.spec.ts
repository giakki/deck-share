import * as fromUi from './ui.reducer';
import { selectUi, selectUiIsStandardLayout, selectUiLayout, selectUiWindow } from './ui.selectors';

describe('Ui Selectors', () => {
    describe('selectUi', () => {
        it('should select the feature state', () => {
            const result = selectUi({
                [fromUi.uiFeatureKey]: fromUi.initialState,
            });

            expect(result).toEqual(fromUi.initialState);
        });
    });

    describe('selectUiLayout', () => {
        it('should select the ui layout', () => {
            const result = selectUiLayout({
                [fromUi.uiFeatureKey]: {
                    ...fromUi.initialState,
                    layout: 'empty',
                },
            });

            expect(result).toEqual('empty');
        });
    });

    describe('selectUiIsStandardLayout', () => {
        it("should return false when the layout is not 'standard'", () => {
            const result = selectUiIsStandardLayout({
                [fromUi.uiFeatureKey]: {
                    ...fromUi.initialState,
                    layout: 'empty',
                },
            });

            expect(result).toEqual(false);
        });

        it("should return true when the layout is 'standard'", () => {
            const result = selectUiIsStandardLayout({
                [fromUi.uiFeatureKey]: {
                    ...fromUi.initialState,
                    layout: 'standard',
                },
            });

            expect(result).toEqual(true);
        });
    });

    describe('selectUiWindow', () => {
        it('should select the ui window', () => {
            const expected = {
                height: Math.random(),
                width: Math.random(),
            };

            const result = selectUiWindow({
                [fromUi.uiFeatureKey]: {
                    ...fromUi.initialState,
                    window: {
                        ...fromUi.initialState.window,
                        ...expected,
                    },
                },
            });

            expect(result).toEqual(expected);
        });

        it("should return true when the layout is 'standard'", () => {
            const result = selectUiIsStandardLayout({
                [fromUi.uiFeatureKey]: {
                    ...fromUi.initialState,
                    layout: 'standard',
                },
            });

            expect(result).toEqual(true);
        });
    });
});
