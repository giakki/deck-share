import { Action } from '@ngrx/store';

import { uiActions } from './ui.actions';
import { initialState, reducer } from './ui.reducer';

describe('Ui Reducer', () => {
    describe('unknown action', () => {
        it('should return the previous state', () => {
            const action = {} as Action;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        });
    });

    describe('setWindowSize', () => {
        it('should produce the correct state', () => {
            const action = uiActions.setWindowSize({ height: Math.random(), width: Math.random() });

            const result = reducer(initialState, action);

            expect(result).toEqual({
                ...initialState,
                window: {
                    ...initialState.window,
                    height: action.height,
                    width: action.width,
                },
            });
        });
    });

    describe('setLayout', () => {
        it('should produce the correct state', () => {
            const action = uiActions.setLayout({ layout: 'empty' });

            const result = reducer(initialState, action);

            expect(result).toEqual({
                ...initialState,
                layout: action.layout,
            });
        });
    });
});
