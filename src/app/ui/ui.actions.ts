import { createActionGroup, props } from '@ngrx/store';

import { UiLayout } from './ui.model';

export const uiActions = createActionGroup({
    source: 'Ui',
    events: {
        'Set Window Size': props<{ height: number; width: number }>(),
        'Set Layout': props<{ layout: UiLayout }>(),
    },
});
