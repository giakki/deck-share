import { TestBed } from '@angular/core/testing';
import { NavigationEnd } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { RouterNavigatedAction, routerNavigatedAction } from '@ngrx/router-store';
import { EMPTY, throwError } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { uiActions } from './ui.actions';
import { layout$, provideUiEffects, windowSize$ } from './ui.effects';
import { WindowService } from './window.service';

function mkRouterNavigatedAction(tree: any[]): RouterNavigatedAction {
    const state = { root: {} } as any;
    let iter = state.root;
    for (const data of tree) {
        iter.firstChild = { data };
        iter = iter.firstChild;
    }

    return routerNavigatedAction({
        payload: {
            event: new NavigationEnd(0, '', ''),
            routerState: state,
        },
    });
}

describe('UiEffects', () => {
    let testScheduler: TestScheduler;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [provideMockActions(() => EMPTY)],
        });

        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    describe('provideUiEffects', () => {
        it('should not throw', () => {
            expect(provideUiEffects).not.toThrow();
        });
    });

    describe('layout', () => {
        it('should inject the required providers', () => {
            TestBed.runInInjectionContext(() => {
                testScheduler.run(({ expectObservable }) => {
                    expectObservable(layout$()).toBe('|');
                });
            });
        });

        it('should produce the expected actions', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('-ab', {
                    a: mkRouterNavigatedAction([{ ui: { layout: 'standard' } }, {}, { ui: { layout: 'empty' } }]),
                    b: mkRouterNavigatedAction([{}, {}, { ui: { layout: 'standard' } }]),
                });

                expectObservable(layout$(actions$)).toBe('-ab', {
                    a: uiActions.setLayout({ layout: 'empty' }),
                    b: uiActions.setLayout({ layout: 'standard' }),
                });
            });
        });

        it('should default to the standard layout', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const actions$ = hot('-a', {
                    a: mkRouterNavigatedAction([{}]),
                });

                expectObservable(layout$(actions$)).toBe('-a', {
                    a: uiActions.setLayout({ layout: 'standard' }),
                });
            });
        });
    });

    describe('windowSize', () => {
        it('should inject the required providers', () => {
            TestBed.runInInjectionContext(() => {
                testScheduler.run(({ expectObservable }) => {
                    expectObservable(windowSize$()).toBe('');
                });
            });
        });

        it('should produce the expected actions', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const values = {
                    a: { height: Math.random(), width: Math.random() },
                };
                spyOnProperty(TestBed.inject(WindowService), 'resize$', 'get').and.returnValues(
                    hot('-a', values),
                    throwError(() => new Error()),
                );

                expectObservable(windowSize$(TestBed.inject(WindowService))).toBe('1ms 200ms a', {
                    a: uiActions.setWindowSize(values.a),
                });
            });
        });

        it('should throttle the actions', () => {
            testScheduler.run(({ hot, expectObservable }) => {
                const values = {
                    a: { height: Math.random(), width: Math.random() },
                    b: { height: Math.random(), width: Math.random() },
                    c: { height: Math.random(), width: Math.random() },
                };
                spyOnProperty(TestBed.inject(WindowService), 'resize$', 'get').and.returnValues(
                    hot('a 100ms b 300ms c', values),
                    throwError(() => new Error()),
                );

                expectObservable(windowSize$(TestBed.inject(WindowService))).toBe('- 300ms b 300ms c', {
                    b: uiActions.setWindowSize(values.b),
                    c: uiActions.setWindowSize(values.c),
                });
            });
        });
    });
});
