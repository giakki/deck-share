import { Injectable } from '@angular/core';
import { fromEvent, map, share } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class WindowService {
    private resizeObservable = fromEvent(window, 'resize', { passive: true }).pipe(
        map(() => ({
            height: window.innerHeight,
            width: window.innerWidth,
        })),
        share(),
    );

    get resize$() {
        return this.resizeObservable;
    }
}
