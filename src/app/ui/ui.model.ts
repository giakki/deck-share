export type UiLayout = 'standard' | 'empty';

export function uiLayoutFromString(layout: unknown): UiLayout {
    return layout === 'empty' ? layout : 'standard';
}
