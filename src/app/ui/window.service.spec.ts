import { TestBed } from '@angular/core/testing';

import { WindowService } from './window.service';

describe('WindowService', () => {
    let service: WindowService;

    beforeEach(() => {
        service = TestBed.inject(WindowService);
    });

    describe('resize$', () => {
        it('should react on window resize events', (done) => {
            const subscription = service.resize$.subscribe((actual) => {
                subscription.unsubscribe();

                expect(actual).toEqual({
                    height: window.innerHeight,
                    width: window.innerWidth,
                });

                done();
            });

            window.dispatchEvent(new Event('resize'));
        });
    });
});
