import { inject } from '@angular/core';
import { Actions, createEffect, ofType, provideEffects } from '@ngrx/effects';
import { routerNavigatedAction } from '@ngrx/router-store';
import { debounceTime, map } from 'rxjs';

import { uiActions } from './ui.actions';
import { uiLayoutFromString } from './ui.model';
import { WindowService } from './window.service';

export const layout$ = createEffect(
    (actions$ = inject(Actions)) => {
        return actions$.pipe(
            ofType(routerNavigatedAction),
            map(({ payload }) => {
                let iter = payload.routerState.root;
                while (iter.firstChild) {
                    iter = iter.firstChild;
                }
                return uiActions.setLayout({ layout: uiLayoutFromString(iter.data['ui']?.layout) });
            }),
        );
    },
    { functional: true },
);

export const windowSize$ = createEffect(
    (window = inject(WindowService)) => {
        return window.resize$.pipe(
            map(({ height, width }) =>
                uiActions.setWindowSize({
                    height,
                    width,
                }),
            ),
            debounceTime(200),
        );
    },
    { functional: true },
);

export const provideUiEffects = () => provideEffects({ layout$, windowSize$ });
