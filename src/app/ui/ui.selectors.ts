import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromUi from './ui.reducer';

export const selectUi = createFeatureSelector<fromUi.State>(fromUi.uiFeatureKey);

export const selectUiLayout = createSelector(selectUi, (state) => state.layout);

export const selectUiIsStandardLayout = createSelector(selectUiLayout, (layout) => layout === 'standard');

export const selectUiWindow = createSelector(selectUi, (state) => state.window);
