import { createFeature, createReducer, on } from '@ngrx/store';

import { uiActions } from './ui.actions';
import { UiLayout } from './ui.model';

export const uiFeatureKey = 'ui';

export interface State {
    layout: UiLayout;
    window: {
        height: number;
        width: number;
    };
}

export const initialState: State = {
    layout: 'standard',
    window: {
        height: window.innerHeight,
        width: window.innerWidth,
    },
};

export const reducer = createReducer(
    initialState,
    on(
        uiActions.setWindowSize,
        (state, { height, width }): State => ({ ...state, window: { ...state.window, height, width } }),
    ),
    on(uiActions.setLayout, (state, { layout }): State => ({ ...state, layout })),
);

export const uiFeature = createFeature({
    name: uiFeatureKey,
    reducer,
});
