import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { provideRouter } from '@angular/router';
import { PushPipe } from '@ngrx/component';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import { AppComponent } from './app.component';
import { authActions } from './auth/auth.actions';

describe('AppComponent', () => {
    beforeEach(() => {
        return TestBed.configureTestingModule({
            imports: [FormsModule, PushPipe, AppComponent],
            providers: [provideMockStore(), provideHttpClient(withInterceptorsFromDi()), provideRouter([])],
        }).compileComponents();
    });

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    describe('login', () => {
        it('should dispatch a login action', () => {
            const fixture = TestBed.createComponent(AppComponent);
            const spy = spyOn(TestBed.inject(Store), 'dispatch').and.returnValue();

            fixture.componentInstance.login();

            expect(spy).toHaveBeenCalledOnceWith(authActions.login());
        });
    });

    describe('logout', () => {
        it('should dispatch a login action', () => {
            const fixture = TestBed.createComponent(AppComponent);
            const spy = spyOn(TestBed.inject(Store), 'dispatch').and.returnValue();

            fixture.componentInstance.logout();

            expect(spy).toHaveBeenCalledOnceWith(authActions.logout());
        });
    });
});
