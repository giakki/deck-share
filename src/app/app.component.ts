import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterLink, RouterOutlet } from '@angular/router';
import { LetDirective, PushPipe } from '@ngrx/component';
import { select, Store } from '@ngrx/store';

import { authActions } from './auth/auth.actions';
import { selectAuthUser } from './auth/auth.selectors';
import { FeatureSelectorComponent } from './feature-selector/feature-selector.component';
import { selectUiIsStandardLayout } from './ui/ui.selectors';

@Component({
    selector: 'app-root',
    styleUrl: './app.component.scss',
    templateUrl: './app.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [
        MatToolbarModule,
        MatButtonModule,
        RouterLink,
        LetDirective,
        RouterOutlet,
        PushPipe,
        FeatureSelectorComponent,
    ],
})
export class AppComponent {
    readonly isStandardLayout$ = this.store.pipe(select(selectUiIsStandardLayout));

    readonly user$ = this.store.pipe(select(selectAuthUser));

    constructor(private store: Store) {}

    login() {
        this.store.dispatch(authActions.login());
    }

    logout() {
        this.store.dispatch(authActions.logout());
    }
}
