import { TestBed } from '@angular/core/testing';
import { provideRouter, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { testDiForCoverage } from '@tests/util';
import { TestScheduler } from 'rxjs/testing';

import {
    APP_ROUTES,
    navigateByUrlEffect,
    navigateEffect,
    provideAppRouter,
    provideAppRouterEffects,
    routerActions,
} from './app-routing.module';

describe('provideAppRouterEffects', () => {
    let actions: Actions;
    let router: Router;
    let testScheduler: TestScheduler;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [StoreModule.forRoot({})],
            providers: [provideRouter([])],
        });

        actions = TestBed.inject(Actions);
        router = TestBed.inject(Router);
        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    it('should be created', () => {
        expect(provideAppRouterEffects).toBeTruthy();
    });

    describe('coverage', () => {
        it('should cover APP_ROUTES', () => {
            for (const route of APP_ROUTES) {
                if (route.loadChildren) {
                    expect(route.loadChildren()).toBeInstanceOf(Promise);
                }
            }
        });

        it('should cover provideAppRouter', () => {
            expect(() => provideAppRouter()).not.toThrow();
        });

        it('should cover provideAppRouterEffects', () => {
            expect(() => provideAppRouterEffects()).not.toThrow();
        });
    });

    describe('navigateEffect', () => {
        it('should auto inject the serives', () => {
            testDiForCoverage(navigateEffect, actions, router);
        });

        it('should call the router', () => {
            const spy = spyOn(router, 'navigate');
            const commands = [crypto.randomUUID()];
            const extras = {};

            testScheduler.run(({ hot, flush }) => {
                const actions$ = new Actions(hot('-a', { a: routerActions.navigate({ commands, extras }) }));

                navigateEffect(actions$, router).subscribe();
                flush();

                expect(spy).toHaveBeenCalledWith(commands, extras);
            });
        });
    });

    describe('navigateByUrlEffect', () => {
        it('should auto inject the serives', () => {
            testDiForCoverage(navigateByUrlEffect, actions, router);
        });

        it('should call the router', () => {
            const spy = spyOn(router, 'navigateByUrl');
            const url = crypto.randomUUID();
            const extras = {};

            testScheduler.run(({ hot, flush }) => {
                const actions$ = new Actions(hot('-a', { a: routerActions.navigateByUrl({ url, extras }) }));

                navigateByUrlEffect(actions$, router).subscribe();
                flush();

                expect(spy).toHaveBeenCalledWith(url, extras);
            });
        });
    });
});
