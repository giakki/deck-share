import { provideHttpClient } from '@angular/common/http';
import { enableProdMode } from '@angular/core';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getPerformance, providePerformance } from '@angular/fire/performance';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouterStore } from '@ngrx/router-store';
import { provideStore } from '@ngrx/store';

import { AppComponent } from './app/app.component';
import { initialState, metaReducers, reducers } from './app/app.reducers';
import { provideAppRouter, provideAppRouterEffects } from './app/app-routing.module';
import { provideAuthEffects } from './app/auth/auth.effects';
import { extModules } from './app/build-specifics';
import { provideUiEffects } from './app/ui/ui.effects';
import { environment } from './environments/environment';

if (environment.production) {
    enableProdMode();
}

bootstrapApplication(AppComponent, {
    providers: [
        provideAnimations(),
        provideHttpClient(),
        provideAppRouter(),
        provideAppRouterEffects(),
        provideStore(reducers, { initialState, metaReducers }),
        provideRouterStore(),
        provideAuthEffects(),
        provideUiEffects(),
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        providePerformance(() => getPerformance()),
        ...extModules,
    ],
}).catch((error) => {
    console.error(error);
});
