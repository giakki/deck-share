FROM node:lts-alpine
LABEL maintainer "Giacomo Martino <docker@giakki.eu>"

ENV CHROME_BIN="/usr/bin/chromium-browser"

RUN echo @edge https://dl-cdn.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories \
    && echo @edge https://dl-cdn.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories \
    && apk -U upgrade \
    && apk add --no-cache \
    g++ \
    make \
    python3 \
    chromium@edge \
    nss@edge \
    freetype@edge \
    harfbuzz@edge \
    ttf-freefont@edge \
    libstdc++@edge \
    wayland-libs-client@edge \
    wayland-libs-server@edge \
    wayland-libs-cursor@edge \
    wayland-libs-egl@edge \
    wayland@edge \
    && ln -sf python3 /usr/bin/python

USER node
